---
layout: draft
title: 草稿
date: 2021-08-03 17:22:15
updated: 2021-08-03 17:22:30
---

### Web 开发简介

在上文中，我们已经成功使用 Go 语言的 `net/http` 包实现路由转发。

Go的 `net/http` 包提供了基础的路由函数组合与丰富的功能函数。但在复杂场景下，官方的http库还是有些力有不逮。因此，在很多情况下，我们需要使用到 GO 相关的 WEB 框架，可以分为两类：

1. Router 框架
2. MVC 框架

开源界有这么几种框架，第一种是对 httpRouter 进行简单的封装，然后提供定制的中间件和一些简单的小工具集成比如 `gin` ，主打轻量，易学，高性能。第二种是借鉴其它语言的编程风格的一些MVC类框架，例如 `beego` ，方便从其它语言迁移过来的程序员快速上手，快速开发。还有一些框架功能更为强大，除了数据库 schema 设计，大部分代码直接生成，例如 `goa` 。

### Beego 快速入门

1. 安装

   ```go
   // 安装Beego框架
   $ go get github.com/astaxie/beego
   // 安装bee工具（命令），执行此命令，会下载bee.exe到$GOPATH/bin目录中。
   $ go get github.com/beego/bee
   ```

2. 创建应用

   ```go
   // 创建一个web应用
   $ bee new webDemo
   // 或者创建一个api应用
   $ bee api webDemo
   ```

   注： `bee` 命令的使用，需要确保 `$GOPATH/bin` 目录已在环境变量 `Path` 中进行配置。

3. 目录结构

   创建一个 beego web 程序，会生成如下目录结构：

   ```
   webDemo
   |-- conf
   |   `-- app.conf
   |-- controllers
   |   `-- default.go
   |-- main.go
   |-- models
   |-- routers
   |   `-- router.go
   |-- static
   |   |-- css
   |   |-- img
   |   `-- js
   |-- tests
   |   `-- default_test.go
   `-- views
       `-- index.tpl
   ```

   呈现出一个典型的 MVC 框架特征。

4. 编译运行

   进入项目目录（即此处示例的 webDemo 目录）下，并执行 `bee run` 命令，编译并启动项目，示例：

   ```
   E:\Go_workspace\src>cd webDemo
   E:\Go_workspace\src\webDemo>bee run
   ```

5. 访问

   启动成功后，浏览器访问：http://localhost:8080 ，将会看到 Beego 默认配置的首页视图：

   ![beego_web_index](https://www.xfc-exclave.com/upload/2020/12/beego_web_index-26c8b736ca394aedafb97af277594f7d.png)

### Beego MVC

beego 是一个典型的 MVC 框架，它的整个执行逻辑如下图所示：

![beego_mvc](https://www.xfc-exclave.com/upload/2020/12/beego_mvc-683b89a815254a16a78ea1051f2471fc.png)

#### 模型（Models）

beego ORM 是一个强大的 Go 语言 ORM 框架。她的灵感主要来自 Django ORM 和 SQLAlchemy。

支持的数据：

* MySQL：https://github.com/go-sql-driver/mysql 
* PostgreSQL：https://github.com/lib/pq 
* Sqlite3：https://github.com/mattn/go-sqlite3 

ORM 特性：

* 支持 Go 的所有类型存储
* 轻松上手，采用简单的 CRUD 风格
* 自动 Join 关联表
* 跨数据库兼容查询
* 允许直接使用 SQL 查询／映射
* 严格完整的测试保证 ORM 的稳定与健壮

安装 ORM 及驱动：

```go
go get github.com/astaxie/beego/orm
go get github.com/go-sql-driver/mysql
```

使用 ORM 注册数据库表：

```go
package main

import (
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql" // import your used driver
)

// Model Struct
type User struct {
	Id       int
UserName string `orm:"size(100)"`
}

func init() {
	// set default database
	orm.RegisterDataBase("default", "mysql", "root:root@/db_for_go?charset=utf8", 30)
	// register model
	orm.RegisterModel(new(User))
	// create table
	orm.RunSyncdb("default", false, true)
}
func main() {
}
```

运行代码，将执行如下命令：

```sql
[Running] go run "e:\Go_workspace\src\beegoDemo\views\orm-test.go"
create table `user` 
    -- --------------------------------------------------
    --  Table Structure for `main.User`
    -- --------------------------------------------------
    CREATE TABLE IF NOT EXISTS `user` (
        `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
        `user_name` varchar(100) NOT NULL DEFAULT '' 
    ) ENGINE=InnoDB;

[Done] exited with code=0 in 1.028 seconds
```

这时，我们看到，主键为 int 类型时，数据库默认采用 `AUTO_INCREMENT` 策略。

##### CURD 基础操作

基于上述代码，注释掉代码 `orm.RunSyncdb("default", false, true)` 后，我们可以对数据库表进行增删改查等基础操作：

1. Insert：插入数据

   ```go
   func main() {
   	o := orm.NewOrm()
   	user := User{UserName: "李白"}
   	// insert
   	id, err := o.Insert(&user)
   	fmt.Printf("ID: %d, ERR: %v\n", id, err)// ID: 1, ERR: <nil>
   }
   ```

2. InsertMulti：同时插入多个对象

   ```go
   func main() {
   	o := orm.NewOrm()
   	users := []User{User{UserName: "秦始皇"}, User{UserName: "江流儿"}, User{UserName: "查尔斯·狄更斯"}}
   	num, err := o.InsertMulti(100, users)
   	fmt.Printf("NUM: %d, ERR: %v\n", num, err) // NUM: 3, ERR: <nil>
   }
   ```

   其执行效果相当于：

   ```sql
   -- 插入多条数据
   insert into table (name, age) values("秦始皇", 28),("江流儿", 30),("查尔斯·狄更斯", 20)
   ```

3. Update：修改单条数据

   ```go
   func main() {
   	o := orm.NewOrm()
   	user := User{Id = 1, UserName: "李太白"}
   	num, err := o.Update(&user)
       // num, err := o.Update(&user, "UserName", "Field2")// 或指定多个更新列
   	fmt.Printf("NUM: %d, ERR: %v\n", num, err)// NUM: 1, ERR: <nil>
   }
   ```

4. Read：查询单条数据（默认通过主键查询）

   ```go
   func main() {
   	o := orm.NewOrm()
   	user := User{Id: 1, UserName: "杜甫"}
   	err := o.Read(&user)// 默认通过主键查询
       // err := o.Read(&user, "UserName")// 也可以通过制定字段进行匹配查询
   	if err == orm.ErrNoRows {
   		fmt.Println("查询不到")
   	} else if err == orm.ErrMissPK {
   		fmt.Println("找不到主键")
   	} else {
   		fmt.Println(user.Id, user.UserName)// 1 李太白
   	}
   }
   ```

5. ReadOrCreate：查询或创建

   当查询不到，则创建一个数据：默认必须传入一个参数作为条件字段，同时也支持多个参数多个条件字段。

   ```go
   func main() {
   	o := orm.NewOrm()
   	user := User{UserName: "辉夜姬"}
   	// 三个返回参数依次为：是否新创建的，对象 Id 值，错误
   	if created, id, err := o.ReadOrCreate(&user, "UserName"); err == nil {
   		if created {
   			fmt.Println("New Insert an object. Id:", id)// 数据库查询不到时，将会自动插入数据
   		} else {
   			fmt.Println("Get an object. Id:", id)// 数据库查询到数据时，将会读取该数据
   		}
   	}
   }
   ```

7. Delete：删除单条数据

   ```go
   func main() {
   	o := orm.NewOrm()
   	// delete
   	u := User{Id: 2}
   	num, err := o.Delete(&u)// 可以缩写成：num, err := o.Delete(&User{Id: 1})
   	fmt.Printf("NUM: %d, ERR: %v\n", num, err)// NUM: 1, ERR: <nil>
   }
   ```

8. 事务处理

   ```go
   func main() {
   	o := orm.NewOrm()
   	// isnert
   	user := User{UserName: "slene"}
   	id, err := o.Insert(&user)
   	fmt.Printf("ID: %d, ERR: %v\n", id, err)
   	if err == nil {
   		o.Commit()
   	} else {
   		o.Rollback()
   	}
   }
   ```

9. 调试查询日志

   ```go
   func init() {
   	// set default database
   	orm.RegisterDataBase("default", "mysql", "root:root@/db_for_go?charset=utf8", 30)
       // open debug log
   	orm.Debug = true
   	// register model
   	orm.RegisterModel(new(User))
   }
   ```

##### 高级查询

ORM 以 **QuerySeter** 来组织查询，每个返回 **QuerySeter** 的方法都会获得一个新的 **QuerySeter** 对象。

基本使用方法：

```go
o := orm.NewOrm()

// 获取 QuerySeter 对象，user 为表名，有点类似于java mybatis中的resultType
qs := o.QueryTable("user")

// 也可以直接使用对象作为表名
user := new(User)
qs = o.QueryTable(user) // 返回 QuerySeter
```

**一、expr** 

QuerySeter 中用于描述字段和 sql 操作符，使用简单的 expr 查询方法。

字段组合的前后顺序依照表的关系，比如 User 表拥有 Profile 的外键，那么对 User 表查询对应的 Profile.Age 为条件，则使用 `Profile__Age` 注意，字段的分隔符号使用双下划线 `__`，除了描述字段， expr 的尾部可以增加操作符以执行对应的 sql 操作。比如 `Profile__Age__gt` 代表 Profile.Age > 18 的条件查询。

注释后面将描述对应的 sql 语句，仅仅是描述 expr 的类似结果，并不代表实际生成的语句。

```go
qs.Filter("id", 1) // WHERE id = 1
qs.Filter("profile__age", 18) // WHERE profile.age = 18
qs.Filter("Profile__Age", 18) // 使用字段名和 Field 名都是允许的
qs.Filter("profile__age", 18) // WHERE profile.age = 18
qs.Filter("profile__age__gt", 18) // WHERE profile.age > 18
qs.Filter("profile__age__gte", 18) // WHERE profile.age >= 18
qs.Filter("profile__age__in", 18, 20) // WHERE profile.age IN (18, 20)
qs.Filter("profile__age__in", 18, 20).Exclude("profile__lt", 1000)
// WHERE profile.age IN (18, 20) AND NOT profile_id < 1000
```

**二、Operators** 

注：此处类似示例不进行重复书写。

1. exact：等于

   Filter / Exclude / Condition expr 的默认值为 exact

   ```go
   qs.Filter("name", "slene") // WHERE name = 'slene'
   qs.Filter("name__exact", "slene") // WHERE name = 'slene'
   // 使用 = 匹配，大小写是否敏感取决于数据表使用的 collation
   qs.Filter("profile_id", nil) // WHERE profile_id IS NULL
   ```

2. iexact：等于，不区分大小写

   ```go
   qs.Filter("name__iexact", "slene")// WHERE name LIKE 'slene'
   // 大小写不敏感，匹配任意 'Slene' 'sLENE'
   ```

3. contains：包含

4. icontains：包含，不区分大小写

5. in：被包含

   ```go
   qs.Filter("profile__age__in", 17, 18, 19, 20)
   // WHERE profile.age IN (17, 18, 19, 20)
   
   ids:=[]int{17,18,19,20}
   qs.Filter("profile__age__in", ids)
   // WHERE profile.age IN (17, 18, 19, 20)
   
   // 同上效果
   ```

6. gt / gte：大于（等于）

7. lt / lte：小于（等于）

8. startswith：开始于

9. istartswith：开始于，不区分大小写

10. endswith：结束于

11. iendswith：结束于，不区分大小写

12. isnull：为空

    ```go
    qs.Filter("profile__isnull", true)
    qs.Filter("profile_id__isnull", true)// WHERE profile_id IS NULL
    
    qs.Filter("profile__isnull", false)// WHERE profile_id IS NOT NULL
    ```

**三、高级查询接口使用** 

QuerySeter 是高级查询使用的接口。

每个返回 QuerySeter 的 api 调用时都会新建一个 QuerySeter，不影响之前创建的。

高级查询使用 Filter 和 Exclude 来做常用的条件查询。囊括两种清晰的过滤规则：包含， 排除

1. Filter

   用来过滤查询结果，起到 **包含条件** 的作用，多个 Filter 之间使用 `AND` 连接。

   ```go
   qs.Filter("profile__isnull", true).Filter("name", "slene")
   // WHERE profile_id IS NULL AND name = 'slene'
   ```

2. Exclude

3. SetCond

4. Limit

5. Offset

6. GroupBy

7. OrderBy

8. Distinct

9. RelatedSel

10. Count

11. Exist

12. Update

13. Delete

14. PrepareInsert

15. All

16. One

17. Values

18. ValuesList

19. ValuesFlat

**四、关系查询** 

1. 一对一关系
2. 一对多关系
3. 多对多关系

### 参考

* https://www.bookstack.cn/read/beego/mvc-model-query.md