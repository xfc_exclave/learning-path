---
layout: draft
title: 草稿
date: 2021-08-03 17:22:15
updated: 2021-08-03 17:22:30
---

### 快速开始

1. 下载 vue脚手架： `npm install vue-cli -g` 
2. 初始化项目： `vue init mpvue/mpvue-quickstart my-project` 
3. 进入项目根目录： `cd my-project` 
4. 安装项目依赖包： `npm install` 
5. 启动初始化项目： `npm start`  或 `npm run dev` 

![798ef172a67e45b495bdc78abc420e571618747258498](.\img\798ef172a67e45b495bdc78abc420e571618747258498.jpg)