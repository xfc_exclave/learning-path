---
layout: draft
title: 草稿
date: 2021-08-03 17:22:15
updated: 2021-08-03 17:22:30
---

## Swagger使用详例

### 一、引入maven依赖

```xml
<properties>
	<swagger.version>2.9.2</swagger.version>
</properties>

<!-- swagger2-->
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger2</artifactId>
    <version>${swagger.version}</version>
    <exclusions>
        <exclusion>
            <groupId>io.swagger</groupId>
            <artifactId>swagger-annotations</artifactId>
        </exclusion>
        <exclusion>
            <groupId>io.swagger</groupId>
            <artifactId>swagger-models</artifactId>
    	</exclusion>
    </exclusions>
</dependency>

<!-- swagger2-UI-->
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger-ui</artifactId>
    <version>${swagger.version}</version>
</dependency>
```



### 二、启动类添加注解（可选）

```java
@EnableSwagger2
@SpringBootApplication
public class MuYiApplication {
    public static void main(String[] args) {
        SpringApplication.run(MuYiApplication.class, args);
        System.out.println("启动成功！！！");
	}
}
```



### 三、创建Swagger2配置类

```java
package com.demo.web.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.demo.common.config.Global;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger2的接口配置
 *
 * @Auther: ErDong
 * @Email: xfc_exclave@163.com
 * @Date: 2019/11/17 11:07
 * @Description:
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    /**
     * 创建API
     */
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
            // 用来创建该API的基本信息，展示在文档的页面中（自定义展示的信息）                				.apiInfo(apiInfo())
            // 设置哪些接口暴露给Swagger展示
            .select()
            // 扫描所有有注解的api，用这种方式更灵活
            .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
            // 扫描指定包中的swagger注解
            //.apis(RequestHandlerSelectors.basePackage("com.demo.project.tool.swagger"))
            // 扫描所有 .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.any())
            .build();
    }
    /**
     * 添加摘要信息
     */
    private ApiInfo apiInfo() {
        // 用ApiInfoBuilder进行定制
        return new ApiInfoBuilder()
            // 设置标题
            .title("标题：**管理系统_接口文档")
            // 描述
            .description("描述：**管理系统_接口文档……")
            // 作者信息
            .contact(new Contact(Global.getName(), null, null))
            // 版本
            .version("版本号:" + Global.getVersion())
            .build();
    }
}
```



### 四、配置swagger-ui访问路径

```java
/**
 * swagger 接口
 *
 * @Auther: ErDong
 * @Email: xfc_exclave@163.com
 * @Date: 2019/11/30 11:55
 * @Description:
 */
@Controller
@RequestMapping("/tool/swagger")
public class SwaggerController extends BaseController {
    
    @GetMapping()
    public String index() {
        return redirect("/swagger-ui.html");
    }
}
```



### 五、@Api与@ApiOperation

#### 1. 标记类资源

```java
// 标记类，可以标记当前controller为swagger文档资源
@Api(value = "/user", description = "用户相关接口")
```

#### 2. 标记接口方法

```java
// 标记方法，说明方法的作用
@ApiOperation(value = "根据分页查询用户列表",
			notes = "根据分页条件查询用户列表[说明信息]",
			response = Order,
			tags = {"用户查询"})
```



### 六、访问swagger-ui.html

浏览器访问：http://localhost:8080/tool/swagger