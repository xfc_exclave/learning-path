---
​---
title: Typora + PicGo 实现自己的图床写作
date: 2021-08-05 10:42:33
updated: 2021-08-05 10:43:09
categories: [工具安装、使用及躺坑, 高效工具推荐]
tags: [markdown, 图床, 存储, 工具]
top_img: false
​---
---

### 前言

`Typora` 是目前最好用的 `markdown` 书写工具，不解释，也不接收反驳。

`PicGo` 是一款免费高效的图床工具，在所有图床工具中， `PicGo` 可以说是你的最佳选择（或之一）了。

通过这篇文章，我们将彻底解决这样一个问题：

* 在 markdown 写作过程中，我的图片应该放在哪儿？

### 软件安装

1. Typora

   下载地址：https://www.typora.io

2. PicGo

   下载地址：https://github.com/Molunerfinn/PicGo/releases

### 配置图床

