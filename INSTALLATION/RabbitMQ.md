---
layout: draft
title: 草稿
date: 2021-08-03 17:22:15
updated: 2021-08-03 17:22:30
---

## RabbitMQ安装步骤

> **说明：** 演示环境为 Windows

### 一、前置环境（Erlang）

#### Erlang 下载

* https://www.erlang.org/downloads（慢）
* https://packages.erlang-solutions.com/erlang/（快）

#### Erlang 安装

1. 双击执行安装文件。
2. 选择安装位置（基本上都是下一步）。
3. 安装完成。

### 二、RabbitMQ 安装

#### RabbitMQ 下载：

* https://www.rabbitmq.com/download.html（下载选择页）
* https://www.rabbitmq.com/changelog.html（快捷下载页）

#### RabbitMQ 安装：

1. 双击执行安装文件。
2. 选择安装位置（基本上都是下一步）。
3. 安装完成。

> 安装完成后， `开始` 中可以看到 `RabbitMQ Service -start` 、 `RabbitMQ Service -stop` 、 `RabbitMQ Service -remove` 三个快捷启动方式。

#### RabbitMQ 配置：

1. 启动插件

   cd 到 `[rabbitmq]\rabbitmq_server-3.8.1\sbin>` 。

   执行： `rabbitmq-plugins enable rabbitmq_management` （执行一次即可）。

2. 启动服务

   执行 `RabbitMQ Service -start` （需要启动服务时执行）。

3. 网页端访问

   浏览器访问：http://localhost:15672/

   账号：guest（默认）

   密码：guest（默认）

> 消息队列必须端口：5672（RabbitMQ的主端口）