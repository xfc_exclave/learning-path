---
layout: draft
title: 草稿
date: 2021-08-03 17:22:15
updated: 2021-08-03 17:22:30
---

本文最终目的：完成node环境及npm工具的安装。

### 1. 下载

> https://nodejs.org/en/download/
>
> 选择安装方式，这里示例选择 `.msi` `64-bit` 安装。

![download_index](https://www.xfc-exclave.com/upload/2020/11/download_index-be516064dc3b42d09f9980221d71b824.png)

### 2. 安装

执行安装文件 `node-v12.18.2-x64.msi` 

一路 **next** ，默认安装位置是 `C:\Program Files\nodejs\` ，当然，你也可以指定自己的安装位置。

安装时会默认安装 `npm` 及添加环境变量。

![anzhuangsetup](https://www.xfc-exclave.com/upload/2020/11/anzhuangsetup-14678eee3546473cb6d5d79ebe0e515c.png)

下一步，直到点击 `install` 、 `Finish` 。

### 3. 测试

命令行执行：

>  `node -v` 
>
>  `npm -v` 

也许你会看到以下错误：

> Error: EPERM: operation not permitted, mkdir 'C:\Program Files\nodejs\node_global'

那么，使用管理员执行就好了。此时的 `npm` 是在安装 `node` 时默认安装的，因此版本可能相对较低。

### 4. 配置

1. **目录** 

   > 推荐移除 `C:\Users\longwin\AppData\Roaming` 目录下的 `npm` 及 `npm-cache` 文件夹。当然，你也可能没有 `npm-cache` 文件夹，那就算了，因为你还没有使用过。

   在安装目录下查看 `node_cache` 和 `node_global` 文件夹是否存在，如果不存在，则手动创建。

   ![nodemulu](https://www.xfc-exclave.com/upload/2020/11/nodemulu-984960c353064b8cb4af78bc55c715ac.png)

   配置 `node_global` 和 `node_cache` 

   > `npm config set prefix "C:\Program Files\nodejs\node_global"` 
   >
   > `npm config set cache "C:\Program Files\nodejs\node_cache"` 

2. 配置镜像（提升速度）

   >  `npm config set registry=http://registry.npm.taobao.org` 

3. 查看配置

   输入命令查看 `.npmrc` 文件位置。

   ![checknpmrc](https://www.xfc-exclave.com/upload/2020/11/checknpmrc-e6393278a47a4ef298ede0df5207d047.png)

   找到该配置文件并打开，可以查看到我们配置的 `node_global` 和 `node_cache` 

   ![npmrc_content](https://www.xfc-exclave.com/upload/2020/11/npmrc_content-745d8b52819a4eaba8be145621742859.png)

4. 更新 `npm` 版本 

   > 在安装 node 时，默认安装最新版本的 npm ，但我们上述重新配置了 `node_global` 和 `node_cache` ，需要执行 `npm install -g npm` 命令更新 npm 版本。

5. 设置环境变量

   > 在node安装时， `node` 及 `npm` 默认添加了环境变量。这里配置 `node_global` 及`node_modules` 的环境变量。
   >
   > * 添加系统变量 `NODE_PATH` 值为 `C:\Program Files\nodejs\node_global\node_modules` 。
   >
   > * 编辑 `PATH` ，添加 `C:\Program Files\nodejs\node_global` 。

### 5. 模块安装

> 全局安装 `vue` 
>
> `npm install vue -g` 
>
> 全局安装 `vue-router` 
>
> `npm install vue-router -g` 
>
> 全局安装 `vue-cli` 
>
> `npm install vue-cli -g` 
>
> 全局安装 `webpack` 与 `webpack-cli` 
>
> `npm install webpack -g` `npm install webpack-cli -g` 