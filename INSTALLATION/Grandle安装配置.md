---
layout: draft
title: 草稿
date: 2021-08-03 17:22:15
updated: 2021-08-03 17:22:30
---

## Grandle 安装配置

### 下载

下载地址：https://gradle.org/install/#manually

> 如果没有阅读源码等需求，仅仅选择 `Binary-only` 即可。

---

### 安装

解压到自定义的目录即可。

---

### 配置

#### 环境变量

