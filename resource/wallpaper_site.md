---
title: 高清壁纸网站推荐
date: 2021-08-03 21:38:41
updated: 2021-08-03 21:39:12
categories: [资源收集]
tags: [资源, 推荐]
top_img: https://img.xfc-exclave.com/2021/08/03/9335428d33fe4191b7f08dbcbd858a991627998032.png
---

### 前言

因为在写博客和文章的过程中，经常使用图片作为封面，所以就专门收集整理了一些比较优质的图片资源网站。

### 高清壁纸网站

#### 极简壁纸（推荐）

网址：https://bz.zzzmh.cn/index

特点：站点很简单，多是二次元壁纸，不需要登录即可下载，收录壁纸约三万五千张，分辨率基本是2K往上，而且网站和图片都很干净。

缺点：种类相对较少。

![网站截图：极简壁纸](https://img.xfc-exclave.com/2021/08/03/bc464392aaba04fa9a34bab85577a6f81627991797.png)

#### WallpaperCave

网址：https://wallpapercave.com

特点：图片种类相当多，各种各样的分辨率都有。

缺点：-

![网站截图：WallpaperCave](https://img.xfc-exclave.com/2021/08/03/a18ac86c6fc14d5cd706f948818f49721627992962.png)

#### CGWALLPAPERS

网址：https://www.cgwallpapers.com

特点：游戏壁纸居多。

缺点：网站内多是带水印的缩略图，获取原图需要跳转来源网站（基本上是游戏网站），探索成本相对较大。

![网站截图：CGWALLPAPERS](https://img.xfc-exclave.com/2021/08/03/f92590a441d032c296353a471f26e1181627993984.png)

#### H128壁纸

网址：https://www.h128.com

特点：瀑布流显示。

缺点：部分图片趣味有点低（国内现状，但重在筛选），另外，需要注册账号才能下载。

![网站截图：H128壁纸](https://img.xfc-exclave.com/2021/08/03/3790f0f9475081426a95212b4ede478f1627997715.png)

#### Magdeleine

网址：https://magdeleine.co/browse

特点：多是文艺偏向的图片，即滤镜的感觉比较重。

缺点：多是文艺偏向的图片，即滤镜的感觉比较重。

![网站截图：Magdeleine](https://img.xfc-exclave.com/2021/08/03/f5f84bdec85fbcaf4b53b19e3a52023b1627997727.png)

#### Unsplash

网址：https://unsplash.com

特点：多欧美风的图片。

缺点：-

![网站截图：Unsplash](https://img.xfc-exclave.com/2021/08/03/a3fffaf39d9df99f8b7274ee7c9f3d6d1627997732.png)

#### Wallroom

网址：https://wallroom.io

特点：-

缺点：-

![网站截图：Wallroom](https://img.xfc-exclave.com/2021/08/03/98dd0b40044bee252c0a66d2f9a6903d1627997756.png)

#### wallheaven

特点：-

缺点：-

![网站截图：Wallheaven](https://img.xfc-exclave.com/2021/08/03/91adcc416791b1234d9adbcc879f62181627997773.png)

#### 彼岸图网

特点：

缺点：需要登录，每天仅供一张壁纸下载（网站公告已说明）。

![网站截图：彼岸图网](https://img.xfc-exclave.com/2021/08/03/9a19ee6b7a2018b94583da1185583b011627997792.png)

#### Netnr

网址：https://ss.netnr.com/wallpaper

特点：其实是一个工具集网站，页面干净。

缺点：索引稍有点不方便。

![网站截图：Netnr](https://img.xfc-exclave.com/2021/08/03/53efa1817a6b15ed4ab47013b90497ef1627997820.png)

#### 千叶网

网址：http://qianye88.com

特点：-

缺点：需要登录，非https协议。

![网站截图：千叶网](https://img.xfc-exclave.com/2021/08/03/aa0723ff48c533f858765cbdeaa462c31627997835.png)

