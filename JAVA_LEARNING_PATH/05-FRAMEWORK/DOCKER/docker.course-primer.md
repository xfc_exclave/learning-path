---
layout: false
title: Docker 详细教程
date: 2021-08-10 21:40:36
updated: 2021-08-10 21:52:17
categories: [JAVA学习路线, 架构篇, Docker]
tags: [Linux, Docker]
top_img: https://img.xfc-exclave.com/2021/08/9a06207ceb0ebb00acdda04836b963de1628336179.png
top_img_height: 80
---

### Docker 安装

> 警告：切勿在没有配置 Docker YUM 源的情况下直接使用 yum 命令安装 Docker 。

#### 系统要求

Docker 支持 64 位版本 CentOS 7/8，并且要求内核版本不低于 3.10。 CentOS 7 满足最低内核的要求，但由于内核版本比较低，部分功能（如 `overlay2` 存储层驱动）无法使用，并且部分功能可能不太稳定。

> 查看 CentOS 版本： `cat /etc/redhat-release` 或 `cat /etc/os-release` 

#### 卸载旧版本

旧版本的 Docker 称为 `docker` 或者 `docker-engine`，如果安装了这些，请卸载它们以及相关的依赖项：

```shell
$ sudo yum remove docker \
            docker-client \
            docker-client-latest \
            docker-common \
            docker-latest \
            docker-latest-logrotate \
            docker-logrotate \
            docker-engine
```

#### 使用 yum 安装

执行以下命令安装依赖包：

```shell
$ sudo yum install -y yum-utils
```

执行以下命令设置 `yum` 软件源：

```shell
# 阿里镜像源（国内推荐）
$ sudo yum-config-manager --add-repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

# Docker 官方镜像源
# $ sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

# 测试版本 Docker
# $ sudo yum-config-manager --enable docker-ce-test
```

#### 安装 Docker

更新 `yum` 软件源缓存，并安装 `docker-ce` 。

```shell
# 更新 yum 软件包索引
$ yum mackcache fast
# docker-ce 指的是社区版
$ sudo yum install docker-ce docker-ce-cli containerd.io
```

> 注：使用阿里云服务器的在这里可能会有些坑。

#### 启动 Docker

```shell
# 设置 docker 开机启动
$ sudo systemctl enable docker
$ sudo systemctl enable containerd
# 启动 docker
$ sudo systemctl start docker
```

如需禁用开机启动

```shell
$ sudo systemctl disable docker
$ sudo systemctl disable containerd
```

#### 建立 Docker 用户组

默认情况下， `docker` 命令会使用 `Unix` 套接字与 Docker 引擎通讯。而只有 `root` 用户和 `docker` 组的用户才可以访问 Docker 引擎的 Unix socket 。出于安全考虑，一般 Linux 系统上不会直接使用 `root` 用户。因此，更好地做法是将需要使用 `docker` 的用户加入 `docker` 用户组。

建立 `docker` 组：

```shell
$ sudo groupadd docker
```

将当前用户加入 `docker` 组：

```shell
$ sudo usermod -aG docker $USER
```

#### 测试 Docker 安装

1. 启动服务

   ```shell
   $ service docker start
   ```

2. 查看 docker 版本

   ```shell
   $ docker verison
   ```

3. 拉取测试镜像

   ```shell
   $ docker run --rm hello-world # --rm 启动成功后就删掉
   
   Unable to find image 'hello-world:latest' locally
   latest: Pulling from library/hello-world
   b8dfde127a29: Pull complete 
   Digest: sha256:df5f5184104426b65967e016ff2ac0bfcd44ad7899ca3bbcf8e44e4461491a9e
   Status: Downloaded newer image for hello-world:latest
   
   Hello from Docker!
   This message shows that your installation appears to be working correctly.
   
   To generate this message, Docker took the following steps:
    1. The Docker client contacted the Docker daemon.
    2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
       (amd64)
    3. The Docker daemon created a new container from that image which runs the
       executable that produces the output you are currently reading.
    4. The Docker daemon streamed that output to the Docker client, which sent it
       to your terminal.
   
   To try something more ambitious, you can run an Ubuntu container with:
    $ docker run -it ubuntu bash
   
   Share images, automate workflows, and more with a free Docker ID:
    https://hub.docker.com/
   
   For more examples and ideas, visit:
    https://docs.docker.com/get-started/
   ```

   如果正常输出如上信息，则表示 docker 已安装成功。

   > 使用 docker 命令获取镜像时，默认会先从本地进行查找，如果本地不存在，则会从配置的镜像源中下载。

4. 查看本地镜像

   ```shell
   $ docker images
   
   REPOSITORY    TAG       IMAGE ID       CREATED        SIZE
   hello-world   latest    d1165f221234   5 months ago   13.3kB
   ```

#### 配置镜像加速

```shell
# 创建并打开 daemon.json 文件
$ vi /etc/docker/daemon.json
# 写入内容
{
  "registry-mirrors": ["您的镜像地址"]
}
# 保存退出
# 重启
$ sudo systemctl daemon-reload
# 重启 docker
$ sudo systemctl restart docker
```

如何获取自己的镜像地址？

1. 阿里云
   * 登录阿里云控制台。
   * 搜索并进入 `容器镜像服务` 。
   * 找到 `镜像工具` / `镜像加速器` 即可获取到唯一的镜像地址。
2. 华为云
   * 登录华为云控制台。
   * 搜索并进入 `容器镜像服务` 。
   * 找到 `镜像资源` / `镜像中心` 并添加 `镜像加速器` 即可获取到唯一的镜像地址。

> 获取其余服务器商提供的镜像加速器方式均相似。

### 卸载 Docker

```shell
# 卸载依赖
$ sudo yum remove docker-ce docker-ce-cli containerd.io
# 删除目录
$ sudo rm -rf /var/lib/docker
$ sudo rm -rf /var/lib/containerd
```

### Docker 启动流程及原理

#### Docker 启动流程

![Docker启动流程图](https://img.xfc-exclave.com/2021/08/8a14ba52293bd7cdbf600968ea481e551628602757.png)

#### Docker VS 虚拟机

Docker 有着比虚拟机更少的抽象层，且 Docker 容器在 Linux 本机上运行，它与其他容器共享主机的内核，因此在新建一个容器时， Docker 并不需要像虚拟机一样重新加载一个操作系统的内核，避免了很多引导操作。

而虚拟机（VM）则是运行一个完整的“Guest操作系统”，通过虚拟机监控程序对主机资源进行虚拟访问。一般来说，虚拟机除了应用程序逻辑所消耗的开销之外，还会产生很多开销。

![Docker VS 虚拟机](https://img.xfc-exclave.com/2021/08/e781c900e152e279625ea9056b299f661628512294.png)

### Docker 常用命令

#### 帮助命令

```shell
# 显示 docker 版本信息
$ docker version
# 显示 docker 详细信息
$ docker info
# 查看 docker 状态
$ docker stats
# 帮助命令
$ docker xxx --help
```

![Docker命令图表](https://img.xfc-exclave.com/2021/08/681651c6f99d3f25184a775dc8ee56bb1628602735.jpeg)

> Docker 命令行参考文档：https://docs.docker.com/reference 

#### 镜像命令

1. docker images

   ```shell
   $ docker images # 查看镜像
   REPOSITORY    TAG       IMAGE ID       CREATED        SIZE
   hello-world   latest    d1165f221234   5 months ago   13.3kB
   
   # REPOSITORY: 镜像的仓库源
   # TAG: 镜像的标签
   # IMAGE ID: 镜像ID
   # CREATED: 创建时间
   # SIZE: 镜像大小
   
   $ docker images -a # -all 列出所有镜像
   $ docker images -q # -quiet 仅列出镜像的ID
   $ docker images -aq # 组合使用
   ```

2. docker search

   ```shell
   $ docker search mysql # 搜索 mysql 镜像
   $ docker search mysql --filter=STARS=3000 # 过滤：过滤STARS大于3000的mysql镜像
   ```

3. docker pull

   > docker 镜像既可使用命令拉取，也可以前往 [Docker Hub](https://hub.docker.com/) 进行下载。

   ```shell
   $ docker pull mysql # 拉取 mysql 镜像
   
   Using default tag: latest # 不指定tag时默认下载最新版本
   latest: Pulling from library/mysql
   45b42c59be33: Pull complete # 分层下载（分层下载会复用已下载的layer，即已存在的layer不会重复下载）
   b4f790bd91da: Pull complete 
   325ae51788e9: Pull complete 
   adcb9439d751: Pull complete 
   174c7fe16c78: Pull complete 
   698058ef136c: Pull complete 
   4690143a669e: Pull complete 
   f7599a246fd6: Pull complete 
   35a55bf0c196: Pull complete 
   790ac54f4c47: Pull complete 
   b0ddd5d1b543: Pull complete 
   1aefd67cb33d: Pull complete 
   Digest: sha256:03306a1f248727ec979f61424c5fb5150e2c5fd2436f2561c5259b1258d6063c # 签名，用于防伪
   Status: Downloaded newer image for mysql:latest
   docker.io/library/mysql:latest # 真实地址
   # docker pull mysql
   # 等价于
   # docker pull docker.io/library/mysql:latest
   
   $ docker pull mysql:5.7 # 指定版本拉取镜像
   ```

4. docker rmi

   ```shell
   $ docker rmi -f d1165f221234 # docker remove image: 删除指定的镜像
   $ docker rmi -f d1165f221234 8457e9155715 # 删除多个镜像
   $ docker rmi -f $(docker images -aq) # 删除所有（递归删除）
   ```

#### 容器命令

拉取一个测试镜像。

```shell
$ docker pull centos
```

1. 创建并启动容器

   ```shell
   $ docker run [可选参数] [image] # 新建并启动容器
   $ docker run -it centos /bin/bash # 启动并以交互的方式进入容器
   # 后台以端口80启动centos，并命名为centos01，并映射到外部端口3344
   $ docker run -d --name centos01 -p 3344:80
   # 进入容器
   [root@xxxxxxxx ~]# docker run -it centos /bin/bash
   [root@01939a1ac7cd /]# ls
   bin  dev  etc  home  lib  lib64  lost+found  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var
   ```

2. 退出容器

   ```shell
   $ exit # 退出容器（停止并退出）
   
   # ctrl + P + Q 退出容器（退出但容器不停止）
   [root@xxxxxxxx /]# docker run -it centos /bin/bash
   [root@286b1fb8e21d /]# [root@xxxxxxxx /]# docker ps
   CONTAINER ID   IMAGE     COMMAND       CREATED              STATUS
   286b1fb8e21d   centos    "/bin/bash"   About a minute ago   Up About a minute
   ```

3. 查看运行的容器

   ```shell
   $ docker ps # 查看正在运行的容器
   $ docker ps -a # 查看所有运行的容器（包括曾经运行过的）
   $ docker ps -n=2 # 显示运行过的2个容器
   $ docker ps -aq # 显示当前正在运行的容器ID
   ```

4. 删除容器

   ```shell
   $ docker rm [容器ID] # 移除指定的容器（不能移除正在运行的容器）
   $ docker rm -f $(docker ps -aq) # 移除所有的容器（-f 可删除正在运行的容器）
   $ docker ps -a -q|xargs docker rm # 移除所有的容器
   ```

5. 启动和停止容器

   ```shell
   $ docker start [容器ID] # 启动容器
   $ docker restart [容器ID] # 重启容器
   $ docker stop [容器ID] # 停止容器
   $ docker kill [容器ID] # 强制停止容器
   ```

6. 后台启动

   ```shell
   $ docker run -d centos # 后台启动centos
   # 注：docker必须保证至少一个运行的进程，否则会自动停止
   ```

7. 查看日志

   ```shell
   $ docker logs -tf --tail 10 8b1c31f83ff8 # 查看10条日志
   # -tf 显示时间戳（格式化显示）
   ```

8. 查看容器内部的进程

   ```shell
   $ docker top 8b1c31f83ff8 # 查看指定容器内部运行的进程
   ```

9. 查看镜像的元数据

   ```shell
   $ docker inspect 8b1c31f83ff8 # 查看指定容器的所有信息
   ```

10. 进入当前正在运行的容器

    ```shell
    $ docker exec -it 8b1c31f83ff8 /bin/bash # 进入指定的容器并启动命令行（进入后开启一个新的终端）
    $ docker attach 8b1c31f83ff8 # 进入指定容器（不会启动新的进程）
    ```

11. 从容器内拷贝文件到主机

    ```shell
    $ docker cp [容器ID]:[路径] [路径] # 将容器内指定路径的文件拷贝到主机
    $ docker cp 8b1c31f83ff8:/home/test.java /home # 将容器内的/home/test.java文件拷贝到主机/home目录下
    ```


#### 可视化工具 Portainer

> Portainer 官方网站：https://www.portainer.io 

```shell
$ docker run -d -p 8088:9000 -v /root/portainer:/data -v /var/run/docker.sock:/var/run/docker.sock --name dev-portainer portainer/portainer
# 查看是否启动成功
$ curl localhost:8088
```

此时从外部浏览器访问该主机的对应端口，可以看到如下界面：

![Portainer初始页面](https://img.xfc-exclave.com/2021/08/a8ad2462956f5a99deeb70cfca98e0ff1628602584.png)

完善信息后创建用户后即可登录。

> 注：如果是云服务器，需要确认在安全组规则中是否开放对应的端口。这里不推荐使用可视化工具，如有兴趣，登录成功后自行探索。

### Docker 镜像详解

#### 镜像和容器

镜像由多个层组成，每层叠加之后，从外部看来就如一个独立的对象。镜像内部是一个精简的操作系统（OS），同时还包含应用运行所必须的文件和依赖包。因为容器的设计初衷就是快速和小巧，所以镜像通常都比较小。

镜像就像停止运行的容器（类），但实际上，它可以停止某个容器的运行，并从中创建新的镜像。在该前提下，镜像可以理解为一种构建时（build-time）结构，而容器可以理解为一种运行时（run-time）结构，如下图所示。

![镜像和容器间的关系](https://img.xfc-exclave.com/2021/08/ae6291c5590378207dc3680076fc89f61628601022.gif)

我们通常使用 `docker container run` 和 `docker service create` 命令从某个镜像启动一个或多个容器，一旦容器从镜像启动后，二者之间就变成了互相依赖的关系，并且在镜像上启动的容器全部停止之前，镜像是无法被删除的。尝试删除镜像而不停止或销毁使用它的容器，会导致出错。

#### Docker 镜像加载原理

##### UnionFS（联合文件系统）

UnionFS（联合文件系统）： UnionFS 是一种分层、轻量级并且高性能的文件系统，它支持对文件系统的修改作为一次提交来一层层的叠加，同时可以将不同目录挂载到同一个虚拟文件系统下（unite several directories into a single virtual filesystem）。 UnionFS 是 Docker 镜像的基础。镜像可以通过分层来进行继承, 基于基础镜像（没有父镜像），可以制作各种具体的应用镜像。
**特性：** 一次同时加载多个文件系统，但从外面看起来，只能看到一个文件系统，联合加载会把各层文件系统叠加起来，这样最终的文件系统会包含所有底层的文件和目录。

##### Docker 镜像加载原理

Docker 的镜像实际上是由一层一层的文件系统组成的，这种层级的文件系统（UFS）主要包含 `bootleader` 和 `kernel` ， `bootleader` 主要是引导加载 `kernel` ， Linux 刚启动会加载 bootfs 文件系统，在 Docker 镜像的最底层是 bootfs 。这一层与我们典型的 Linux/Unix 系统是一样的，包含 boot 加载器和内核。当 boot 加载完成之后，整个内核就都在内存中了，此时内存的使用权已由 bootfs 转交给内核，此时系统也会卸载 bootfs 。

roofts（root file system）就是各种不同的操作系统发行版，比如 `Ubuntu` 、 `Centos` 等。

对于个精简的 OS ， rootfs 可以很小，只需要包合最基本的命令，工具和程序库就可以了，因为底层直接用宿主机的内核，自己只需要提供 rootfs 就可以了。

由此可见对于不同的Linux发行版， boots 基本是一致的， rootfs 会有差別，因此不同的发行版可以公用 bootfs 。

##### 镜像的分层结构

Docker 支持通过扩展现有镜像，创建新的镜像。

![层级文件系统图示](https://img.xfc-exclave.com/2021/08/c4c69be4310118b7eda353256d3c5a221628601033.jpeg)

事实上，所有的 Docker 镜像都起始于一个基础的镜像层，当进行修改或增加新的内容时，就会在当前镜像层之上创建新的镜像层。

Docker 镜像采用这种分层结构的好处在于共享资源，多个镜像都从相同的基础层镜像创建而来， Docker 主机则只需要在磁盘上保存一份基础层镜像，同时内存中也只加载一份基础层镜像，就可以为所有容器提供服务了。

> Docker 通过存储引擎（新版本采用快照机制）的方式来实现镜像层堆栈，并保证多镜像层对外展示为统一的文件系统。

Docker 的镜像都是只读的，而容器层则是可写的。当容器启动时，一个新的可写层被加载到镜像的顶部，它被称为容器层，在容器层之下，都是叫镜像层。

当我们对容器进行添加、删除或修改等操作时，其实所作用的是通过 `run` 命令启动后生成的一个新的容器层。

##### 提交镜像

```shell
# 将一个容器提交为新的镜像
$ docker commit -m '镜像描述' -a '作者' [容器ID] [镜像名]
```

> docker 原理与 git 相似，也可以简单理解为：原镜像 + 自己的文件 = 新的镜像

### 容器数据卷

`数据卷` 是一个可供一个或多个容器使用的特殊目录，它绕过 UFS，可以提供很多有用的特性：

* 数据卷可以在容器之间共享和重用。
* 对数据卷的修改会立马生效。
* 对数据卷的更新，不会影响镜像。
* 数据卷默认会一直存在，即使容器被删除。

容器目录的挂载：

```shell
$ docker run -it -v [宿主机目录]:[容器目录] # 使用 -v 参数实现挂载
$ docker run -it -v /home/ceshi:/home centos /bin/bash # 将主机的/home/ceshi目录与容器内的目录/home进行挂载，挂载成功后，两个目录之间会自动进行双向同步。
# 指定多个 -v [宿主机目录]:[容器目录]，即可挂载多个目录。
```

> 数据卷的使用，类似于 Linux 下对目录或文件进行 mount ，镜像中的被指定为挂载点的目录中的文件会复制到数据卷中（仅数据卷为空时会复制）。

#### 具名挂载与匿名挂载

```shell
# 匿名挂载，即没有指定容器内挂载目录对应的外部路径和名字
$ docker run -d -P --name nginx01 -v /etc/nginx nginx # 使用随机端口启动nginx，并将目录/etc/nginx匿名挂载。

$ docker volume ls # 查看所有数据卷信息

# 具名挂载，即没有指定容器内挂载目录对应的外部路径，但指定了数据卷名字
$ docker run -d -P --name nginx02 -v nginx-test:/etc/nginx nginx

# 使用 docker inspect 查看容器详情，可以看到卷挂载路径信息。
```

具名和匿名挂载会将数据卷挂载到 `/var/lib/docker/volumes/xxx/_data` 目录中。

> 匿名挂载不易识别，不建议使用。

```shell
$ docker run -d -P --name nginx01 -v /etc/nginx:ro nginx # ro -> 只读，只能通过书主机操作
$ docker run -d -P --name nginx01 -v /etc/nginx:rw nginx # rw -> 读写，默认值。
```



#### 数据卷容器

多个容器之间也可以通过 `--volunes-from` 挂载实现容器间数据共享。

```shell
$ docker run -it --name docker02 --volunes-from docker01 exclave/centos
# 启动镜像 exclave/centos 并命名为docker02，且挂载到 docker01
```

> 容器之间配置信息的传递，数据卷容器的生命周期一直持续到没有容器使用位置。
>
> 但如果容器挂载到了本地文件系统，移除所有容器，本地信息也不会丢失。

### DockerFile

#### DockerFile 构建示例

添加数据卷除了直接通过命令进行挂载外，还可以通过 `dockerFile` 进行添加，操作方式如下：

1. 创建一个 dockerfile 文件（名字可以随意），并在该文件中写入脚本。

   ```shell
   FROM centos
   VOLUME ["volume01", "volume02"]
   CMD echo "--------------success--------------"
   CMD /bin/bash
   ```

   > FROM: 定制的镜像都是基于 FROM 的镜像。

2. 构建镜像

   ```shell
   # docker build -f [脚本路径]:[镜像标签] .
   $ docker build -f dockerfile:v3 .
   ```

   > 注意：命令结尾有一个 `.` ，代表着本次执行的上下文路径。

3. 启动构建成功后的镜像

   ```shell
   $ docker run -it [镜像ID] /bin/bash
   ```

   启动成功后，根目录下会生成脚本中指定的挂载目录（匿名挂载）。

#### DockerFile 构建说明

* 每个保留关键字都必须是大写。
* 执行顺序从上至下。
* `#` 表示注解。
* 每一个指令都会创建提交一个新的镜像层。

#### DockerFile 指令

```shell
FROM      # 基础镜像
MAINTAINER  # 镜像作者：姓名+邮箱
RUN       # 镜像构建时需要运行的命令
ADD       # 步骤
WORKDIR    # 镜像的工作目录
VOLUME     # 挂载目录
EXPOSE     # 指定暴露的端口
CMD       # 指定容器启动时要运行的命令（只有最后一个会生效，可被替代）
ENTRYPOINT  # 与CMD相似，但可以追加命令
ONBUILD    # （触发指令）
COPY      # 类似ADD，将文件拷贝到镜像中
ENV       # 构建时设置环境变量
```

> docker history [镜像ID] 可以查看镜像的构建过程。
>
> DockerFile 的官方命名文件为 `Dockerfile` ，编译时不指定 `-f` 参数则会自动读取该文件。

#### DockerFile 步骤总结

1. 编写一个 dockerfile 文件。
2. docker build 构建称为一个镜像。
3. docker run 运行镜像。
4. docker push 发布镜像（DockerHub、阿里云镜像仓库）

#### 制作 Tomcat 镜像

TODO

#### 镜像发布

**发布到 Docker Hub：** 

1. Docker Hub 注册账号。

2. 登录。

   ```shell
   $ docker login -u [用户名]
   Password: ******
   ```

3. 提交。

   ```shell
   $ docker push [仓库名]/[镜像名]:[版本标签]
   ```

**发布到阿里云：** 

TODO

### Docker 流程总结

![Docker流程图](https://img.xfc-exclave.com/2021/08/fe3d92358148e397a84020f11c15d5011628612250.png)

### Docker 网络

#### Docker0

宿主机每启动一个 Docker 容器，就会给容器分配一个 IP 。宿主机只要安装了 docker ，就会安装一个 docker0网卡进行桥接。

`evth-pair` 

#### 容器互联：--link



#### 容器互联：自定义网络



#### 网络连通



#### Redis 集群部署



#### Springboot 微服务打包 Docker

### 参考

* https://yeasy.gitbook.io/docker_practice/install/centos
* https://docs.docker.com
* https://www.bilibili.com/video/BV1og4y1q7M4