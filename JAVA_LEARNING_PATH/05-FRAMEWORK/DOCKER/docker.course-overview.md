---
title: Docker 概述
date: 2021-08-07 18:37:51
updated: 2021-08-07 19:18:44
categories: [JAVA学习路线, 架构篇, Docker]
tags: [Linux, Docker]
top_img: https://img.xfc-exclave.com/2021/08/9a06207ceb0ebb00acdda04836b963de1628336179.png
top_img_height: 80
---

`Docker` 是一个用于开发、发布和运行应用程序的开放平台，它能够使您将引用程序与基础设施分离开来，以实现快速交付。使用 Docker ，您可以使用与管理您的应用程序相同的方式管理您的基础设施。通过利用 Docker 快速发布、测试及部署代码的方法，您可以显著地减少编写代码和在生产环境中运行代码之间的延迟。

### Docker 平台

Docker 提供了在一个被称之为 `容器` 的松散隔离的环境中进行打包和运行程序的能力。这种隔离性和安全性使您能够在同一台给定的主机上同时运行多个容器。所有的容器都是轻量级的，并且包含了应用程序运行所需要的一切，因此您不需要再依赖于当前主机所安装的内容。

### Docker 可以做什么

1. 快速、一致的交付应用程序。

   Docker 适合持续集成和持续交付（CI/CD）工作流。

   ![DevOps能力环](https://img.xfc-exclave.com/2021/08/34056f83461b2667fefe304d915170211628335468.png)

   > 关于 `CI/CD` ：https://www.redhat.com/zh/topics/devops/what-is-ci-cd
   >
   > 关于 `DevOps` ：https://baike.baidu.com/item/devops/2613029

2. 响应式部署和扩展。

   Docker 基于容器的平台允许高度可移植的工作负载。Docker 容器可以在开发人员的本地笔记本电脑、数据中心的物理或虚拟机、云提供商或混合环境中运行。

   Docker 的可移植性和轻量级特性还使动态管理工作负载、根据业务需求几乎实时地扩展或拆除应用程序和服务变得容易。

3. 在相同的硬件环境上运行更多工作负载。

   Docker 非常适合高密度环境以及需要以更少资源完成更多任务的中小型部署。

> Docker 理念是将应用及依赖包打包到一个可移植的容器中，可发布到任意 Linux 发行版 Docker 引擎上。使用沙箱机制运行程序，程序之间相互隔离。

### Docker 体系结构

Docker 使用 `客户端 - 服务器` 的架构模式， Docker 客户端与 Docker 守护进程进行对话，后者负责构建、运行和分发 Docker 容器的繁重工作。它们既可以在同一系统上运行，也可以远程建立连接。客户端和守护进程之间使用 `REST API` 、 UNIX 套接字或网络接口进行通信。 Docker 体系结构如下图所示：

![Docker体系结构示意图](https://img.xfc-exclave.com/2021/08/b595b80b1dc39db2af4ffd6f0e8e8f551628335478.svg+xml)

#### Docker 守护进程

Docker 守护进程（ `dockerd` ）侦听 Docker API 的请求并管理 Docker 对象，如镜像、容器、网络和卷。守护进程还可以与其他守护进程进行通信以管理 Docker 服务。

#### Docker 客户端

Docker 客户端 ( `docker` ) 是许多 Docker 用户与 Docker 进行交互的主要方式。当用户发起 `docker run` 等命令时，客户端会使用 `Docker API` 将这些命令发送到 `dockerd` 并执行。一个 Docker 客户端可以与多个 `dockerd` 进行通信。

#### Docker 注册表

Docker Hub 是 Docker 镜像的公共注册中心， Docker 默认配置是从 Docker Hub 上查找镜像，您也可以根据需要配置自己的私有注册表。

当使用 `docker pull` 或 `docker run` 命令时，您所需要的镜像将从配置的注册表中提取。当使用 `dcoker push` 命令时，您的镜像会被推送到您配置的注册表中。

> Docker 注册表工作原理与 Git 相似。

#### Docker 对象

1. 镜像（ `Image` ）

   镜像是用于创建 Docker 容器指令的 **只读模板** 。通常，一个镜像会基于另一个镜像，例如，您可以构建一个基于 ubuntu 镜像的镜像，并安装 Apache Web 服务器和您的引用程序，以及您的应用程序所需要的相关环境和配置详细信息。

   在构建自己的镜像时，您需要使用简单的语法创建一个 `Dockerfile` ，用于定义创建和运行镜像所需要的步骤。 `Dockerfile` 中的每条指令都会在镜像中创建一个层（ `layer` ）。当对镜像进行重建时，只会重建被更改的层，而非所有。这种虚拟化技术及其模式，使得镜像轻量而小巧且易于部署。

2. 容器（ `Container` ）

   容器是镜像的可运行实例。您可以使用 `Docker API` 或 `CLI` 进行创建、启动、停止、移动或删除容器。也可以将容器连接到一个或多个网络，以为其附加存储，甚至可以根据当前的状态创建新的镜像。

   默认情况下，一个容器与其他容器，及其自身所在的主机是相对隔离的。

   命令示例： `docker run` 

   ```shell
   # 使用docker运行一个ubuntu容器，以交互的方式添加到本地命令行会话，并运行 /bin/bash
   $ docker run -i -t ubuntu /bin/bash
   ```

   运行该命令时，将发生以下情况：

   1. 如果当前主机本地没有该镜像， Docker 会从配置的注册表中进行提取，这一步骤等同于 `docker pull ubuntu` 。

   2. Docker 会创建一个新的容器，这一步骤等同于 `docker container create` 。

   3. Docker 为容器分配一个读写文件系统，作为它的最后一层。这允许正在运行的容器在其本地文件系统中创建或修改文件和目录。

   4. Docker 创建一个网络接口来将容器连接到默认网络，其中包括为容器分配 IP 地址。默认情况下，容器可以使用主机的网络连接连接到外部网络。

   5. Docker 启动容器并执行 `/bin/bash` 。由于容器以交互方式运行并附加到您的终端（由于 `-i` 和 `-t` 标志），您可以在输出记录到终端时使用键盘提供输入。

      > docker 中必须保持至少一个进程运行，否则容器会自动退出。这里在启动时指定启动了 `/bin/bash` 进程。

   6. 当输入 `exit` 以终止 `/bin/bash` 命令时，容器会停止，但不会被移除。您可以重新启动或删除它。

3. 仓库（ `Repository` ）

   Docker 仓库与代码仓库类似，所不同的是它用于集中存放镜像。Docker 仓库存放于 Docker 注册表中，通常情况下，注册表中会有多个仓库，而仓库中则存放一类镜像，每个镜像利用 tag 进行区分，比如 Ubuntu 仓库中存放有多个版本的 Ubuntu 镜像。

### 底层技术支持

Docker 是使用 `Golang` 编写的，并利用 Liunx 内核的几个特征来提供其功能， Docker 使用一种被称为 `namespaces` 的技术来提供隔离的工作空间，这种工作空间即被称为容器。当运行一个容器时， Docker 会为该容器创建一组命名空间。

这些命名空间提供了一层隔离。容器的每个方面都在单独的命名空间中运行，并且其访问权限仅限于该命名空间。

### 参考

* https://docs.docker.com/get-started/overview
* https://www.bilibili.com/video/BV1og4y1q7M4

> 注：本文大部分内容参考自docker官方文档，并有少部分内容根据个人理解进行整理。