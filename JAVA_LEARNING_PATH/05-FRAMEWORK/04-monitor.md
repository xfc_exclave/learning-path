---
layout: draft
title: 草稿
date: 2021-08-03 17:22:15
updated: 2021-08-03 17:22:30
---

## 监控

### 监控什么

#### CPU

#### 内存

#### 磁盘I/O

#### 网络I/O等

---

### 监控手段

#### 进程监控

#### 语义监控

#### 极其资源监控

#### 数据波动

---

### 监控数据采集

#### 日志

#### 埋点

---

### Dapper

