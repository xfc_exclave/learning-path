---
title: JAVA基础知识
categories: [JAVA学习路线, 基础篇]
tags: [java]
top_img: false
---

参考：http://hollischuang.gitee.io/tobetopjavaer/#/

### 基本数据类型

变量就是申请内存来存储值。也就是说，当创建变量的时候，需要在内存中申请空间。内存管理系统根据变量的类型为变量分配存储空间，分配的空间只能用来储存该类型数据。因此，通过定义不同类型的变量，可以在内存中储存整数、小数或者字符。Java语言提供了八种基本类型。六种数字类型（四个整数型，两个浮点型），一种字符类型，还有一种布尔型。

#### 8种基本数据类型

##### 整数型

* byte
* int
* short
* long

##### 浮点型

通常的浮点型数据在不声明的情况下都是double型的，如果要表示一个数据时float 型的，可以在数据后面加上 "F" 。浮点型的数据是不能完全精确的，有时候在计算时可能出现小数点最后几位出现浮动，这时正常的。

* float

  float：单精度，在计算机中存储占用4字节，也32位，有效位数为7位。

* double

  double：双精度，在计算机中存储占用8字节，64位，有效位数为16位。

##### 字符型

* char

  char 有以下的初始化方式：

  * char ch = 'a'; // 可以是汉字，因为是Unicode编码
  
  * char ch = 1010; // 可以是十进制数、八进制数、十六进制数等等。
  
  * char ch = '\0'; // 可以用字符编码来初始化，如：'\0' 表示结束符，它的ascll码是0，这句话的意思和 ch = 0 是一个意思。

##### 布尔型

* boolean

  例：boolean flag = false;

##### 基本数据类型对比

| 数据类型名称 | 占用字节 | 位数 | 默认值   | 取值范围                | 封装器类  |
| ------------ | -------- | ---- | -------- | ----------------------- | --------- |
| byte         | 1        | 8    | 0        | -2^7 - 2^7-1[-128～127] | Byte      |
| short        | 2        | 16   | 0        | -2^15 - 2^15-1          | Short     |
| int          | 4        | 32   | 0        | -2^31 - 2^31-1          | Integer   |
| long         | 8        | 64   | 0L       | -2^63 - 2^63-1          | Long      |
| float        | 4        | 32   | 0.0f     | -2^31 - 2^31-1          | Float     |
| double       | 8        | 64   | 0.0d     | -2^63 - 2^63-1          | Double    |
| char         | 2        | 16   | '\u0000' | 0 - 2^16-1              | Character |
| boolean      | 1        | 8    | false    | true、false             | Boolean   |

#### 引用数据类型

基本数据类型和引用数据类型的区别主要在存储方式上，基本数据类型在被创建时，在栈上给其划分一块内存，将数值直接存储在栈上；引用数据类型在被创建时，首先要在栈上给其引用（句柄）分配一块内存，而对象的具体信息都存储在堆内存上，然后由栈上面的引用指向堆中对象的地址。

##### 类（对象）

##### 接口

##### 数组

### 自动拆装箱

自动类型转换时，转换前的数据类型的位数低于转换后的数据类型。

> 低  ------------------------------------------------------------->  高
>
> byte,short,char—> int —> long—> float —> double

数据类型转换必须满足如下规则：

- 不能对boolean类型进行类型转换。

- 不能把对象类型转换成不相关类的对象。

- 由大到小会丢失精度：在把容量大的类型转换为容量小的类型时必须使用强制类型转换。

  强制类型转换

  - 条件是转换的数据类型必须是兼容的。
  - 格式：(type)value type是要强制类型转换后的数据类型

- 转换过程中可能导致溢出或损失精度，例如：

  ```java
  int i =128;
  byte b = (byte)i;// -128
  ```

- 浮点数到整数的转换是通过舍弃小数得到，而不是四舍五入。

#### 包装类型

包装类型使得基本数据类型中的变量具有了类中对象的特征。基本数据类型，使用起来非常方便，但是没有对应的方法来操作这些基本类型的数据，可以使用一个类，把基本数据类型的数据装起来，这个类叫做包装类（wrapper）。这样我们可以调用类中的方法。

#### 基本类型

> 见基本数据类型部分

#### 自动拆装箱

自动装箱时编译器调用 valueOf 将原始类型值转换成对象，同时自动拆箱时，编译器通过调用类似intValue(),doubleValue()这类的方法将对象转换成原始类型值。

* 装箱（基本类型->包装类）

  通过包装类的构造器实现装箱（JDK1.5之前）

  ```java
  Integer t = new Integer(10);
  ```

  通过字符串参数构造包装类对象实现装箱

  ```java
  Float f = new Float("4.56");
  Long l = new Long("wer");// NumberFormatException
  ```

  自动装箱（JDK1.5之后）

  ```java
  Integer i = 100;// 自动装箱
  // 相当于编译器自动作以下的语法编译：Integer i = Integer.valueOf(100);
  ```

* 拆箱（包装类->基本类型）

  调用包装类中的.xxxValue()方法

  ```java
  Integer t = 128; // 此时t就是一个包装类
  System.out.println(t.intValue());//128
  ```

  自动拆箱

  ```java
  Integer i = 10; // 自动装箱 
  int t = i; // 自动拆箱，实际上执行了 int t = i.intValue();
  ```

#### Integer的缓存机制

Integer与Integer比较的时候，由于直接赋值的时候会进行自动装箱。那么这里就需要注意两个问题

1. -128<= x<=127的整数，将会直接缓存在IntegerCache中，那么当赋值在这个区间的时候，不会创建新的Integer对象，而是从缓存中获取已经创建好的Integer对象。
2. 大于这个范围的时候，直接new Integer来创建Integer对象。

此部分内容来自：https://www.cnblogs.com/benjieqiang/p/11305777.html

### String

#### 字符串的不可变性

字符串的底层使用的是数组存储，数组的长度是不可变的。且使用final，private修饰，不能直接修改，String也没有提供直接修改的方法。String一旦被创建，则不能被修改，所谓的修改也只是创建了新的对象。字符串常量对象存储在常量池中，常量池中的字符串是不会重复的。

#### JDK6和JDK7中substring的原理及区别

* JDK6中的substring

  在JDK6中，String类包含三个成员变量： `char value[]` ， `int offset` ， `int count` 。他们分别用来存储真正的字符数组，数组的第一个位置索引以及字符串中包含的字符个数。当调用substring方法的时候，会创建一个新的string对象，但是这个string的值仍然指向堆中的同一个字符数组。这两个对象中只有count和offset 的值是不同的。

  ```java
  // JDK 6
  String(int offset, int count, char value[]) {
      this.value = value;
      this.offset = offset;
      this.count = count;
  }
  
  public String substring(int beginIndex, int endIndex) {
      //check boundary
      return  new String(offset + beginIndex, endIndex - beginIndex, value);
  }
  ```

  **JDK 6中的substring导致的问题** 

  如果你有一个很长很长的字符串，但是当你使用substring进行切割的时候你只需要很短的一段。这可能导致性能问题，因为你需要的只是一小段字符序列，但是你却引用了整个字符串（因为这个非常长的字符数组一直在被引用，所以无法被回收，就可能导致内存泄露）。在JDK 6中，一般用以下方式来解决该问题，原理其实就是生成一个新的字符串并引用他。

  ```java
  x = x.substring(x, y) + "";
  ```

* JDK7中的substring

  在JDK7中，substring方法会在堆内存中创建一个新的数组，substring 使用`new String`创建了一个新字符串，避免对老字符串的引用。从而解决了内存泄露问题。

  ```java
  // JDK 7
  public String(char value[], int offset, int count) {
      //check boundary
      this.value = Arrays.copyOfRange(value, offset, offset + count);
  }
  
  public String substring(int beginIndex, int endIndex) {
      //check boundary
      int subLen = endIndex - beginIndex;
      return new String(value, beginIndex, subLen);
  }
  ```

此部分内容来自：https://www.hollischuang.com/archives/1232

#### replaceFirst、replaceAll、replace区别

```java
public class Replace {
    public static void main(String[] args) {
        String s = "my.test.txt";
        System.out.println(s.replace(".", "#"));      // replace将字符串中的. 都替换为 #
        System.out.println(s.replaceAll(".", "#"));   // replaceAll 用到了正则表达式，其中. 是任意字符的意思，所以结果是字符串全部替换为#
        System.out.println(s.replaceFirst(".", "#")); // replaceFirst 用到了正则表达式， 其中. 是任意字符的意思，所以第一个字符被#号代替
        System.out.println(s.replaceFirst("\\.", "#"));  // 正则表达式中双杠是原生字符串的意思，所以结果是字符串中第一个. 被#代替得到           
    }
}
```

#### String对“+”的重载

java中实际没有运算符的重载，但是对String对象而言，它是可以直接`+`将两个String对象的字符串值相加。乍看起来这是对 `+` 的重载，但我们可以通过class文件看出，这只是JVM做的语法糖。

#### 字符串拼接的几种方式和区别

* +

  Java中的 `+` 实际上是先构建一个 `StringBuilder` 对象，然后使用 `append()` 方法拼接字符串，最后调用 `toString()` 方法生成字符串，简单来说，其实现原理是使用`StringBuilder.append` 。

* concat

  ```java
  public String concat(String str) {
      int otherLen = str.length();
      if (otherLen == 0) {
          return this;
      }
      int len = value.length;
      char buf[] = Arrays.copyOf(value, len + otherLen);
      str.getChars(buf, len);
      return new String(buf, true);
  }
  ```

  `concat` 实现字符串拼接，首先是创建了一个字符数组，长度是已有字符串和待拼接字符串的长度之和，再把两个字符串的值复制到新的字符数组中，并使用这个字符数组创建一个新的String对象并返回。

* StringBuilder.append

  和`String`类类似，`StringBuilder`类也封装了一个字符数组，与`String`不同的是，它并不是`final`的，所以他是可以修改的。另外，与`String`不同，字符数组中不一定所有位置都已经被使用，它有一个实例变量，表示数组中已经使用的字符个数。`StringBuilder` 的 `append` 方法会直接拷贝字符到内部的字符数组中，如果字符数组长度不够，会进行扩展。

* StringBuffer.append

  `StringBuffer` 和 `StringBuilder` 类似，最大的区别就是 `StringBuffer` 是线程安全的，其 `append` 方法使用`synchronized`进行声明，说明是一个线程安全的方法。而`StringBuilder`则不是线程安全的。

* StringUtils.join

  其实也是通过 `StringBuilder` 来实现的。

**总结** 

1. 如果不是在循环体中进行字符串拼接的话，直接使用`+`就好了。
2. 如果在并发场景中进行字符串拼接的话，要使用`StringBuffer`来代替`StringBuilder`。

此部分内容来自：https://www.hollischuang.com/archives/3186

#### String.valueOf和Integer.toString的区别

`String.valueOf` 和 `Integer.toString` 没有区别，因为前者内部是通过后者实现的。

```java
public static String valueOf(int i) {
    return Integer.toString(i);
}
```

#### switch对String的支持

**其实swich只支持一种数据类型，那就是整型，其他数据类型都是转换成整型之后再使用switch的。** 

* switch对int的判断是直接比较整数的值。

* switch对char类型进行比较的时候，实际上比较的是ascii码，编译器会把char型变量转换成对应的int型变量。
* switch对字符串的switch是通过 `equals()` 和 `hashCode()` 方法来实现的。

#### 字符串池（String Pool）

字符串池的优点就是避免了相同内容的字符串的创建，节省了内存，省去了创建相同字符串的时间，同时提升了性能；另一方面，字符串池的缺点就是牺牲了JVM在常量池中遍历对象所需要的时间，不过其时间成本相比而言比较低。

#### 常量池

常量池在java用于保存在编译期已确定的，已编译的class文件中的一份数据。它包括了关于类，方法，接口等中的常量，也包括字符串，执行器产生的常量也会放入常量池，故认为常量池是 JVM 的一块特殊的内存空间。

#### intern

intern() 方法返回字符串对象的规范化表示形式。它遵循以下规则：对于任意两个字符串 s 和 t，当且仅当 s.equals(t) 为 true 时，s.intern() == t.intern() 才为 true。

当字符串调用 `intern()` 方法时，这个方法会首先检查字符串池中是否存在该字符串，如果存在，则返回该字符串的引用；如果不存在，则将这个字符串添加到字符串池中，并返回这个字符串的引用。

```java
String str1 = "a";
String str2 = "b";
String str3 = "ab";
String str4 = str1 + str2;
String str5 = new String("ab");

System.out.println(str5.equals(str3));// true 比较字符串的值
System.out.println(str5 == str3);// false 比较内存地址，str5使用new String方式创建了新的字符串对象
System.out.println(str5.intern() == str3);// true intern先行检查字符串是否能存在
System.out.println(str5.intern() == str4);// false str4使用+号，相当于新创建了字符串对象
```

---

### 关键字

* transient
* instanceof
* volatile
* synchronized
* final
* static
* const

---

### 集合类

#### java集合类图

![jihelei](https://pic002.cnblogs.com/images/2012/80896/2012053020261738.gif)

注：图中 `LinkIterator` 应为 `ListIterator` 

#### 常用集合类的使用



#### ArrayList和LinkList和Vector的区别

`ArrayList` 和 `Vector` 都是基于数组实现的，而 `LinkedList` 则是基于双向链表实现的。（查询块，增删慢）

* `Arraylist` 的实现原理是采用一个动态对象数组实现的，默认构造方法创建一个空数组，它在第一次添加元素时候，扩展容量为10，之后的扩充算法：原来数组大小+原来数组的一半（也就是1.5倍）。
* Vector的实现原理也是采用一个动态对象数组实现的，只不过它的默认构造方法创建一个大小为10的对象数组，与 `Arraylist` 不同的是， 在缺省的情况下，增长原数组长度的一倍（也就是2倍）。
* 而对 `ArrayList` 和 `Vector` 要进行增删操作的时候，需要移动修改元素后面的所有元素，所以增删的开销比较大，对增删操作的执行效率低。
* 为了防止数组动态扩充过多，建议创建 `ArrayList` 或者 `Vector` 时，给定初始容量。
* `Arraylist` 多线程中使用不安全，适合在单线程访问时使用，效率较高，而 `Vector` 线程安全，适合在多线程访问时使用，效率较低。

而对于 `LinkedList` 来说，增加和删除元素方便，增加或删除一个元素，仅需处理结点间的引用即可。但是查询不方便，需要一个个对比，无法根据下标直接查找。（增删块，查询慢），同时，`LinkedList` 也是非线程安全的。

> 扩展搜索：`ArrayList和Vector的扩容机制` 、`单向链表、双向链表` 。

#### SynchronizedList和Vector的区别

SynchronizedList和Vector最主要的区别：

1. SynchronizedList有很好的扩展和兼容功能。他可以将所有的List的子类转成线程安全的类。
2. 使用SynchronizedList的时候，进行遍历时要手动进行同步处理。
3. SynchronizedList可以指定锁定的对象。

#### HashMap、HashTable、ConcurrentHashMap区别

* HashTable 中的方法是同步的，而HashMap中的方法在默认情况下是非同步的。在多线程并发的环境下，可以直接使用HashTable，但是要使用HashMap的话就要自己增加同步处理了。
* 在继承关系上，HashTable是基于陈旧的Dictionary类继承来的。 HashMap继承的抽象类AbstractMap实现了Map接口。
* HashTable中，key和value都不允许出现null值，否则会抛出NullPointerException异常。 HashMap中，null可以作为键，这样的键只有一个；可以有一个或多个键所对应的值为null。
* 在扩容机制上，HashTable中的hash数组初始大小是11，增加的方式是 old*2+1。HashMap中hash数组的默认大小是16，而且一定是2的指数。
* HashTable直接使用对象的hashCode。 HashMap重新计算hash值。
* Hashtable、HashMap都使用了 Iterator。而由于历史原因，Hashtable还使用了Enumeration的方式 。 HashMap 实现 Iterator，支持fast-fail，Hashtable的 Iterator 遍历支持fast-fail，用 Enumeration 不支持 fast-fail。

**HashMap 和 ConcurrentHashMap 的区别？** 

ConcurrentHashMap和HashMap的实现方式不一样，虽然都是使用桶数组实现的，但是还是有区别，ConcurrentHashMap对桶数组进行了分段，而HashMap并没有。

ConcurrentHashMap在每一个分段上都用锁进行了保护。HashMap没有锁机制。所以，前者线程安全的，后者不是线程安全的。

#### Set和List的区别

List,Set都是继承自Collection接口。都是用来存储一组相同类型的元素的。

List特点：元素有放入顺序，元素可重复 。

Set特点：元素无放入顺序，元素不可重复。

#### Set如何保证元素不重复

在Java的Set体系中，根据实现方式不同主要分为两大类。HashSet和TreeSet。

1. TreeSet 是二叉树实现的,Treeset中的数据是自动排好序的，不允许放入null值
2. HashSet 是哈希表实现的,HashSet中的数据是无序的，可以放入null，但只能放入一个null，两者中的值都不能重复，就如数据库中唯一约束。

在HashSet中，基本的操作都是有HashMap底层实现的，因为HashSet底层是用HashMap存储数据的。当向HashSet中添加元素的时候，首先计算元素的hashcode值，然后通过扰动计算和按位与的方式计算出这个元素的存储位置，如果这个位置位空，就将元素添加进去；如果不为空，则用equals方法比较元素是否相等，相等就不添加，否则找一个空位添加。

TreeSet的底层是TreeMap的keySet()，而TreeMap是基于红黑树实现的，红黑树是一种平衡二叉查找树，它能保证任何一个节点的左右子树的高度差不会超过较矮的那棵的一倍。

TreeMap是按key排序的，元素在插入TreeSet时compareTo()方法要被调用，所以TreeSet中的元素要实现Comparable接口。TreeSet作为一种Set，它不允许出现重复元素。TreeSet是用compareTo()来判断重复元素的。

#### Java8中stream相关用法

Stream 使用一种类似用 SQL 语句从数据库查询数据的直观方式来提供一种对 Java 集合运算和表达的高阶抽象。

Stream API可以极大提高Java程序员的生产力，让程序员写出高效率、干净、简洁的代码。

这种风格将要处理的元素集合看作一种流，流在管道中传输，并且可以在管道的节点上进行处理，比如筛选，排序，聚合等。

Stream有以下特性及优点：

- 无存储。Stream不是一种数据结构，它只是某种数据源的一个视图，数据源可以是一个数组，Java容器或I/O channel等。
- 为函数式编程而生。对Stream的任何修改都不会修改背后的数据源，比如对Stream执行过滤操作并不会删除被过滤的元素，而是会产生一个不包含被过滤元素的新Stream。
- 惰式执行。Stream上的操作并不会立即执行，只有等到用户真正需要结果的时候才会执行。
- 可消费性。Stream只能被“消费”一次，一旦遍历过就会失效，就像容器的迭代器那样，想要再次遍历必须重新生成。

和以前的Collection操作不同， Stream操作还有两个基础的特征：

- **Pipelining**: 中间操作都会返回流对象本身。 这样多个操作可以串联成一个管道， 如同流式风格（fluent style）。 这样做可以对操作进行优化， 比如延迟执行(laziness)和短路( short-circuiting)。
- **内部迭代**： 以前对集合遍历都是通过Iterator或者For-Each的方式, 显式的在集合外部进行迭代， 这叫做外部迭代。 Stream提供了内部迭代的方式， 通过访问者模式(Visitor)实现。

**实例** 

```java
import java.util.ArrayList;
import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.Map;
 
public class Java8Tester {
   public static void main(String args[]){
      System.out.println("使用 Java 7: ");
        
      // 计算空字符串
      List<String> strings = Arrays.asList("abc", "", "bc", "efg", "abcd","", "jkl");
      System.out.println("列表: " +strings);
      long count = getCountEmptyStringUsingJava7(strings);
        
      System.out.println("空字符数量为: " + count);
      count = getCountLength3UsingJava7(strings);
        
      System.out.println("字符串长度为 3 的数量为: " + count);
        
      // 删除空字符串
      List<String> filtered = deleteEmptyStringsUsingJava7(strings);
      System.out.println("筛选后的列表: " + filtered);
        
      // 删除空字符串，并使用逗号把它们合并起来
      String mergedString = getMergedStringUsingJava7(strings,", ");
      System.out.println("合并字符串: " + mergedString);
      List<Integer> numbers = Arrays.asList(3, 2, 2, 3, 7, 3, 5);
        
      // 获取列表元素平方数
      List<Integer> squaresList = getSquares(numbers);
      System.out.println("平方数列表: " + squaresList);
      List<Integer> integers = Arrays.asList(1,2,13,4,15,6,17,8,19);
        
      System.out.println("列表: " +integers);
      System.out.println("列表中最大的数 : " + getMax(integers));
      System.out.println("列表中最小的数 : " + getMin(integers));
      System.out.println("所有数之和 : " + getSum(integers));
      System.out.println("平均数 : " + getAverage(integers));
      System.out.println("随机数: ");
        
      // 输出10个随机数
      Random random = new Random();
        
      for(int i=0; i < 10; i++){
         System.out.println(random.nextInt());
      }
        
      System.out.println("使用 Java 8: ");
      System.out.println("列表: " +strings);
        
      count = strings.stream().filter(string->string.isEmpty()).count();
      System.out.println("空字符串数量为: " + count);
        
      count = strings.stream().filter(string -> string.length() == 3).count();
      System.out.println("字符串长度为 3 的数量为: " + count);
        
      filtered = strings.stream().filter(string ->!string.isEmpty()).collect(Collectors.toList());
      System.out.println("筛选后的列表: " + filtered);
        
      mergedString = strings.stream().filter(string ->!string.isEmpty()).collect(Collectors.joining(", "));
      System.out.println("合并字符串: " + mergedString);
        
      squaresList = numbers.stream().map( i ->i*i).distinct().collect(Collectors.toList());
      System.out.println("Squares List: " + squaresList);
      System.out.println("列表: " +integers);
        
      IntSummaryStatistics stats = integers.stream().mapToInt((x) ->x).summaryStatistics();
        
      System.out.println("列表中最大的数 : " + stats.getMax());
      System.out.println("列表中最小的数 : " + stats.getMin());
      System.out.println("所有数之和 : " + stats.getSum());
      System.out.println("平均数 : " + stats.getAverage());
      System.out.println("随机数: ");
        
      random.ints().limit(10).sorted().forEach(System.out::println);
        
      // 并行处理
      count = strings.parallelStream().filter(string -> string.isEmpty()).count();
      System.out.println("空字符串的数量为: " + count);
   }
    
   private static int getCountEmptyStringUsingJava7(List<String> strings){
      int count = 0;
        
      for(String string: strings){
        
         if(string.isEmpty()){
            count++;
         }
      }
      return count;
   }
    
   private static int getCountLength3UsingJava7(List<String> strings){
      int count = 0;
        
      for(String string: strings){
        
         if(string.length() == 3){
            count++;
         }
      }
      return count;
   }
    
   private static List<String> deleteEmptyStringsUsingJava7(List<String> strings){
      List<String> filteredList = new ArrayList<String>();
        
      for(String string: strings){
        
         if(!string.isEmpty()){
             filteredList.add(string);
         }
      }
      return filteredList;
   }
    
   private static String getMergedStringUsingJava7(List<String> strings, String separator){
      StringBuilder stringBuilder = new StringBuilder();
        
      for(String string: strings){
        
         if(!string.isEmpty()){
            stringBuilder.append(string);
            stringBuilder.append(separator);
         }
      }
      String mergedString = stringBuilder.toString();
      return mergedString.substring(0, mergedString.length()-2);
   }
    
   private static List<Integer> getSquares(List<Integer> numbers){
      List<Integer> squaresList = new ArrayList<Integer>();
        
      for(Integer number: numbers){
         Integer square = new Integer(number.intValue() * number.intValue());
            
         if(!squaresList.contains(square)){
            squaresList.add(square);
         }
      }
      return squaresList;
   }
    
   private static int getMax(List<Integer> numbers){
      int max = numbers.get(0);
        
      for(int i=1;i < numbers.size();i++){
        
         Integer number = numbers.get(i);
            
         if(number.intValue() > max){
            max = number.intValue();
         }
      }
      return max;
   }
    
   private static int getMin(List<Integer> numbers){
      int min = numbers.get(0);
        
      for(int i=1;i < numbers.size();i++){
         Integer number = numbers.get(i);
        
         if(number.intValue() < min){
            min = number.intValue();
         }
      }
      return min;
   }
    
   private static int getSum(List numbers){
      int sum = (int)(numbers.get(0));
        
      for(int i=1;i < numbers.size();i++){
         sum += (int)numbers.get(i);
      }
      return sum;
   }
    
   private static int getAverage(List<Integer> numbers){
      return getSum(numbers) / numbers.size();
   }
}
```

#### apache集合处理工具类的使用

#### 不同版本的JDK中HashMap的实现的区别以及原因

#### Collection和Collections的区别

* `java.util.Collection` 是一个集合接口。它提供了对集合对象进行基本操作的通用接口方法。Collection接口的意义是为各种具体的集合提供了最大化的统一操作方式。
* `java.util.Collections` 是一个包装类。它包含有各种有关集合操作的静态多态方法。此类不能实例化，就像一个工具类，服务于Java的Collection框架。

#### Arrays.asList获得的List使用时需注意什么

* asList 得到的只是一个 Arrays 的内部类，一个原来数组的视图 List，因此如果对它进行增删操作会报错。

  这是因为Arrays.asList()方法返回的ArrayList不是java.util包下的，而是java.util.Arrays.ArrayList。这个内部类没有实现add()、remove()方法，而是直接使用它的父类AbstractList的相应方法。而这个方法中相关的set，add和remove方法均返回 `throw new UnsupportedOperationException();` 。

* 用 ArrayList 的构造器可以将其转变成真正的 ArrayList。

#### Enumeration和Iterator的区别

* 函数接口不同

  Enumeration只有2个函数接口。通过Enumeration，我们只能读取集合的数据，而不能对数据进行修改。

  Iterator只有3个函数接口。Iterator除了能读取集合的数据之外，也能数据进行删除操作。

* Iterator支持fail-fast机制，而Enumeration不支持。

  Enumeration 是JDK 1.0添加的接口。使用到它的函数包括Vector、Hashtable等类，这些类都是JDK 1.0中加入的，Enumeration存在的目的就是为它们提供遍历接口。Enumeration本身并没有支持同步，而在Vector、Hashtable实现Enumeration时，添加了同步。

  而Iterator 是JDK 1.2才添加的接口，它也是为了HashMap、ArrayList等集合提供遍历接口。Iterator是支持fail-fast机制的：当多个线程对同一个集合的内容进行操作时，就可能会产生fail-fast事件。

注意：Enumeration迭代器只能遍历Vector、Hashtable这种古老的集合，因此通常不要使用它，除非在某些极端情况下，不得不使用Enumeration，否则都应该选择Iterator迭代器。

#### fail-fast和fail-safe

**fail-fast** 

在系统设计中，快速失效系统是一种可以立即报告任何可能表明故障情况的系统。快速失效系统通常设计用于停止正常操作，而不是试图继续可能存在缺陷的过程。这种设计通常会在操作中的多个点检查系统的状态，因此可以及早检测到任何故障。快速失效模块的职责是检测错误，然后让系统的下一个最高级别处理错误。

fail-fast机制可以预先识别出一些错误情况，一方面可以避免执行复杂的其他代码，另外一方面，这种异常情况被识别之后也可以针对性的做一些单独处理。

但值得注意的是，Java的集合类中运用了fail-fast机制进行设计，一旦使用不当，触发fail-fast机制设计的代码，就会发生非预期情况。

我们通常说的Java中的fail-fast机制，默认指的是Java集合的一种错误检测机制。当多个线程对部分集合进行结构上的改变的操作时，有可能会产生fail-fast机制，这个时候就会抛出ConcurrentModificationException。如在 foreach 循环中对某些集合元素进行元素进行 remove/add 操作。

在 foreach 进行 add/remove 操作出现异常的原因：

> foreach使用了增强for循环，而在增强for循环中，集合遍历是通过iterator进行的，但是元素的add/remove却是直接使用的集合类自己的方法。这就导致iterator在遍历的时候，会发现有一个元素在自己不知不觉的情况下就被删除/添加了，就会抛出一个ConcurrentModificationException异常，用来提示用户，可能发生了并发修改！但实际上这里并没有真的发生并发，只是Iterator使用了fail-fast的保护机制，只要他发现有某一次修改是未经过自己进行的，那么就会抛出异常。

**fail-safe** 

为了避免触发fail-fast机制，导致异常，我们可以使用Java中提供的一些采用了fail-safe机制的集合类。这样的集合容器在遍历时不是直接在集合内容上访问的，而是先复制原有集合内容，在拷贝的集合上进行遍历。

`java.util.concurrent` 包下的容器都是fail-safe的，可以在多线程下并发使用，并发修改。同时也可以在foreach中进行add/remove 。fail-safe集合的所有对集合的修改都是先拷贝一份副本，然后在副本集合上进行的，并不是直接对原集合进行修改。并且这些修改方法，如add/remove都是通过加锁来控制并发的。

但是，虽然基于拷贝内容的优点是避免了ConcurrentModificationException，但同样地，迭代器并不能访问到修改后的内容。

CopyOnWriteArrayList中add/remove等写方法是需要加锁的，目的是为了避免Copy出N个副本出来，导致并发写。但是，CopyOnWriteArrayList中的读方法是没有加锁的。这样做的好处是我们可以对CopyOnWrite容器进行并发的读，当然，这里读到的数据可能不是最新的。因为写时复制的思想是通过延时更新的策略来实现数据的最终一致性的，并非强一致性。

但对于在循环中进行 add/remove 操作时，我们可以使用普通的for循环，因为普通for循环并没有用到Iterator的遍历，所以压根就没有进行fail-fast的检验。

#### Copy-On-Write

Copy-On-Write简称COW，是一种用于程序设计中的优化策略。其基本思路是，从一开始大家都在共享同一个内容，当某个人想要修改这个内容的时候，才会真正把内容Copy出去形成一个新的内容然后再改，这是一种延时懒惰策略。

CopyOnWrite容器即写时复制的容器。通俗的理解是当我们往一个容器添加元素的时候，不直接往当前容器添加，而是先将当前容器进行Copy，复制出一个新的容器，然后新的容器里添加元素，添加完元素之后，再将原容器的引用指向新的容器。

所以CopyOnWrite容器是一种读写分离的思想，读和写不同的容器。而Vector在读写的时候使用同一个容器，读写互斥，同时只能做一件事儿。

#### CopyOnWriteArrayList、ConcurrentSkipListMap

从JDK1.5开始Java并发包里提供了两个使用CopyOnWrite机制实现的并发容器,它们是CopyOnWriteArrayList和CopyOnWriteArraySet。CopyOnWrite容器非常有用，可以在非常多的并发场景中使用到。

**CopyOnWriteArrayList** 

CopyOnWriteArrayList的整个add操作都是在锁的保护下进行的。也就是说add方法是线程安全的。CopyOnWrite并发容器常用于读多写少的并发场景。

和ArrayList不同的是，CopyOnWriteArrayList具有以下特性：

* 支持高效率并发且是线程安全的
* 因为通常需要复制整个基础数组，所以可变操作（add()、set() 和 remove() 等等）的开销很大
* 迭代器支持hasNext(), next()等不可变操作，但不支持可变 remove()等操作
* 使用迭代器进行遍历的速度很快，并且不会与其他线程发生冲突。在构造迭代器时，迭代器依赖于不变的数组快照。

**ConcurrentSkipListMap** 

ConcurrentSkipListMap是一个内部使用跳表，并且支持排序和并发的一个Map，是线程安全的。（一般很少会被用到，也是一个比较偏门的数据结构）

ConcurrentSkipListMap 和 ConcurrentHashMap 的主要区别：

* 底层实现方式不同。ConcurrentSkipListMap底层基于跳表。ConcurrentHashMap底层基于Hash桶和红黑树。
* ConcurrentHashMap不支持排序。ConcurrentSkipListMap支持排序。

### 枚举

#### 枚举的用法



#### 枚举的实现

枚举类本质上是一个继承自Enum的类，并且使用关键字 final 进行修饰。当我们使用enmu来定义一个枚举类型的时候，编译器会自动帮我们创建一个final类型的类继承Enum类，所以枚举类型不能被继承。

#### 枚举与单例



#### Enum类



#### Java枚举如何比较

java 枚举值比较用 == 和 equals 方法没啥区别，两个随便用都是一样的效果。因为枚举 Enum 类的 equals 方法默认实现就是通过 == 来比较的；

类似的 Enum 的 compareTo 方法比较的是 Enum 的 ordinal 顺序大小；

类似的还有 Enum 的 name 方法和 toString 方法一样都返回的是 Enum 的 name 值。

#### switch对枚举的支持

Java 1.7 之前 switch 参数可用类型为 short、byte、int、char，枚举类型之所以能使用其实是编译器层面实现的

编译器会将枚举 switch 转换为类似下面的形式：

```java
switch(s.ordinal()) {
    case Status.START.ordinal();
}
```

所以其实质还是 int 参数类型。

#### 枚举的序列化如何实现



#### 枚举的线程安全性问题

编译器会将我们创建的枚举类中的属性及方法声明为static修饰，而当一个Java类第一次被真正使用到的时候静态资源被初始化、Java类的加载和初始化过程都是线程安全的。所以，创建一个enum类型是线程安全的。

### IO

Bit最小的二进制单位 ，是计算机的操作部分。取值0或者1

Byte（字节）是计算机操作数据的最小单位由8位bit组成 取值（-128-127）

Char（字符）是用户的可读写的最小单位，在Java里面由16位bit组成 取值（0-65535）

#### 字符流、字节流

字节流，用于操作byte（字节）类型数据，主要操作类是OutputStream、InputStream的子类；不用缓冲区，直接对文件本身操作。

字符流，用于操作char（字符）字符类型数据，主要操作类是Reader、Writer的子类；使用缓冲区缓冲字符，不关闭流就不会输出任何内容。

整个IO包实际上分为字节流和字符流，但是除了这两个流之外，还存在一组字节流-字符流的转换类。

OutputStreamWriter：是Writer的子类，将输出的字符流变为字节流，即将一个字符流的输出对象变为字节流输出对象。

InputStreamReader：是Reader的子类，将输入的字节流变为字符流，即将一个字节流的输入对象变为字符流的输入对象。

#### 输入流、输出流

输入、输出以存储数据的介质作为参照物，如果是把对象读入到介质中，这就是输入。从介质中向外读数据，这就是输出。所以，输入流是把数据写入存储介质的。输出流是从存储介质中把数据读取出来。

#### 同步、异步、阻塞、非阻塞

同步与异步用于描述 **被调用者** ，如果是同步，在被调用时会立即执行要做的事。如果是异步，在被调用时不保证会立即执行要做的事，但是保证会去做。

阻塞与非阻塞用于描述 **调用者** ，如果是阻塞，发出调用后，要一直等待返回结果。如果是非阻塞，在发出调用后不需要等待，可以去做自己的事情。

#### Linux 5种IO模型

#### BIO、NIO和AIO的区别

#### 三种IO的用法和原理

#### netty

### 反射

反射机制指的是程序在运行时能够获取自身的信息。在java中，只要给定类的名字，那么就可以通过反射机制来获得类的所有属性和方法。

#### 反射与工厂模式

#### 反射有什么用

* 在运行时判断任意一个对象所属的类。
* 在运行时判断任意一个类所具有的成员变量和方法。
* 在运行时任意调用一个对象的方法。
* 在运行时构造任意一个类的对象。

#### Class类

Java的Class类是java反射机制的基础，通过Class类我们可以获得关于一个类的相关信息。

Java.lang.Class是一个比较特殊的类，它用于封装被装入到JVM中的类（包括类和接口）的信息。当一个类或接口被装入的JVM时便会产生一个与之关联的java.lang.Class对象，可以通过这个Class对象对被装入类的详细信息进行访问。

虚拟机为每种类型管理一个独一无二的Class对象。也就是说，每个类（型）都有一个Class对象。运行程序时，Java虚拟机(JVM)首先检查是否所要加载的类对应的Class对象是否已经加载。如果没有加载，JVM就会根据类名查找.class文件，并将其Class对象载入。

#### java.long.reflect.*

### 动态代理

#### 静态代理、动态代理

#### 动态代理和反射的关系

#### 动态代理的集中实现方式

#### AOP

### 序列化

#### 什么是序列化与反序列化

#### 为什么序列化

#### 序列化底层原理

#### 序列化与单例模式

#### protobuf

#### 为什么说序列化并不安全

### 注解

#### 元注解

元注解，即定义其他注解的注解。元注解有四个：

* @Target（表示该注解可以用于什么地方）
* @Retention（表示再什么级别保存该注解信息）
* @Documented（将此注解包含再javadoc中）
* @Inherited（允许子类继承父类中的注解）

#### 自定义注解

除了元注解，都是自定义注解。通过元注解定义出来的注解。 如我们常用的Override 、Autowire等。 日常开发中也可以自定义一个注解，这些都是自定义注解。

#### Java中常用注解使用

* @Override 表示当前方法覆盖了父类的方法
* @Deprecation 表示方法已经过时,方法上有横线，使用时会有警告。
* @SuppressWarnings 表示关闭一些警告信息(通知java编译器忽略特定的编译警告)
* SafeVarargs (jdk1.7更新) 表示：专门为抑制“堆污染”警告提供的。
* @FunctionalInterface (jdk1.8更新) 表示：用来指定某个接口必须是函数式接口，否则就会编译出错。

#### 注解与反射的结合

#### Spring常用注解

* @Configuration 把一个类作为一个IoC容器，它的某个方法头上如果注册了@Bean，就会作为这个Spring容器中的Bean。
* @Scope注解 作用域
* @Lazy(true) 表示延迟初始化
* @Service用于标注业务层组件
* @Controller用于标注控制层组件@Repository用于标注数据访问组件，即DAO组件。
* @Component泛指组件，当组件不好归类的时候，我们可以使用这个注解进行标注。
* @Scope用于指定scope作用域的（用在类上）
* @PostConstruct用于指定初始化方法（用在方法上）
* @PreDestory用于指定销毁方法（用在方法上）
* @DependsOn：定义Bean初始化及销毁时的顺序
* @Primary：自动装配时当出现多个Bean候选者时，被注解为@Primary的Bean将作为首选者，否则将抛出异常
* @Autowired 默认按类型装配，如果我们想使用按名称装配，可以结合@Qualifier注解一起使用。如下：
* @Autowired @Qualifier("personDaoBean") 存在多个实例配合使用
* @Resource默认按名称装配，当找不到与名称匹配的bean才会按类型装配。
* @PostConstruct 初始化注解
* @PreDestroy 摧毁注解 默认 单例 启动就加载

### JMS

#### 什么是Java消息服务

#### JMS消息传送模型

### JMX

#### java.lang.management.*

#### javax.management.*

### 泛型

Java泛型（ generics） 是JDK 5中引入的⼀个新特性， 允许在定义类和接口的时候使用类型参数 。

声明的类型参数在使用时用具体的类型来替换。 泛型最主要的应⽤是在JDK 5中的新集合类框架中。

泛型最⼤的好处是可以提⾼代码的复用性。 以List接口为例，我们可以将String、 Integer等类型放⼊List中， 如不用泛型， 存放String类型要写⼀个List接口， 存放Integer要写另外⼀个List接口， 泛型可以很好的解决这个问题。

#### 泛型与继承

#### 类型擦除

#### 泛型中KTVE？object等的含义

#### 泛型各种用法

#### 限定通配符和非限定通配符

#### 上下界限定符extends和super

#### List\<Object\>和原始类型List之间的区别

原始类型List和带参数类型 `List<Object>` 之间的主要区别是，在编译时编译器不会对原始类型进行类型安全检查，却会对带参数的类型进行检查。它们之间的第二点区别是，你可以把任何带参数的类型传递给原始类型List，但却不能把 `List` 传递给接受 `List<Object>` 的方法，因为会产生编译错误。

#### List<?>和List\<Object\>之间的区别是什么？

`List<?>` 是一个未知类型的List，而 `List<Object>` 其实是任意类型的List。你可以把`List<String>` ， `List<Integer>` 赋值给 `List<?>` ，却不能把 `List<String>` 赋值给 `List<Object>` 。

### 单元测试

#### junit

#### mock

#### mockito

#### 内存数据库（h2）

### 正则表达式

#### java.lang.util.regex.*

### 常用的Java工具库

#### commons.lang

#### commons.*...

#### guava-libraries

#### netty

### API&SPI

#### API

#### SPI

##### 如何定义SPI

##### SPI的实现原理

#### API与SPI的关系和区别

### 异常

#### 异常类型

#### 正确处理异常

#### 自定义异常

#### Error和Exception

#### 异常链

#### try-with-resources

#### finally和return的执行顺序

### 时间处理

#### 时区

#### 冬时令和夏时令

#### 时间戳

#### Java中时间API

#### 格林威治时间

格林尼治平时（英语：Greenwich Mean Time，GMT）是指位于英国伦敦郊区的皇家格林尼治天文台当地的平太阳时，因为本初子午线被定义为通过那里的经线。

格林尼治平时基于天文观测本身的缺陷，已经被原子钟报时的协调世界时（UTC）所取代。

一般使用GMT+8表示中国的时间，是因为中国位于东八区，时间上比格林威治时间快8个小时。

#### CET，UTC，GMT，CST几种常见时间的含义和关系

**CET** 欧洲中部时间（英語：Central European Time，CET）是比世界标准时间（UTC）早一个小时的时区名称之一。它被大部分欧洲国家和部分北非国家采用。冬季时间为UTC+1，夏季欧洲夏令时为UTC+2。

**UTC** 协调世界时，又称世界标准时间或世界协调时间，简称UTC，从英文“Coordinated Universal Time”／法文“Temps Universel Cordonné”而来。台湾采用CNS 7648的《资料元及交换格式–资讯交换–日期及时间的表示法》（与ISO 8601类似）称之为世界统一时间。中国大陆采用ISO 8601-1988的国标《数据元和交换格式信息交换日期和时间表示法》（GB/T 7408）中称之为国际协调时间。协调世界时是以原子时秒长为基础，在时刻上尽量接近于世界时的一种时间计量系统。

**GMT** 格林尼治标准时间（旧译格林尼治平均时间或格林威治标准时间；英语：Greenwich Mean Time，GMT）是指位于英国伦敦郊区的皇家格林尼治天文台的标准时间，因为本初子午线被定义在通过那里的经线。

**CST** 北京时间，China Standard Time，又名中国标准时间，是中国的标准时间。在时区划分上，属东八区，比协调世界时早8小时，记为UTC+8，与中华民国国家标准时间（旧称“中原标准时间”）、香港时间和澳门时间和相同。

**关系** 

CET=UTC/GMT + 1小时 CST=UTC/GMT +8 小时 CST=CET+9

#### SimpleDateFormat的线程安全问题

#### Java 8中的时间处理

#### 如何在东八区的计算机上获取美国时间

### 编码方式

#### Unicode

#### 有了Unicode为啥还需要UTF-8

#### GBK、GB2312、GB18030之间的区别

#### UTF8、UTF16、UTF32区别

#### URL编解码、BIG Endian和Little Endian

#### 如何解决乱码问题

### 语法糖

> 语法糖（Syntactic Sugar），也称糖衣语法，是由英国计算机学家 Peter.J.Landin 发明的一个术语，指在计算机语言中添加的某种语法，这种语法对语言的功能并没有影响，但是更方便程序员使用。简而言之，语法糖让程序更加简洁，有更高的可读性。

#### Java中语法糖原理

#### 解语法糖

语法糖的存在主要是方便开发人员使用，但其实Java虚拟机并不支持这些语法糖。这些语法糖在编译阶段就会被还原成简单的基础语法结构，这个过程就是解语法糖。

#### 语法糖

* switch支持String与枚举

  字符串的switch是通过 `equals()` 和 `hashCode()` 方法来实现的。而枚举则是通过其 `ordinal()` 方法实现的。

* 泛型

* 自动装箱与拆箱

  自动装箱就是Java自动将原始类型值转换成对应的对象，反之，则是拆箱。装箱过程是通过调用包装器的valueOf方法实现的，而拆箱过程是通过调用包装器的 xxxValue方法实现的。

* 方法变长参数

  可变参数是在Java 1.5中引入的一个特性。它允许一个方法把任意数量的值作为参数。

  ```java
  run(String... strs)
  ```

  可变参数在被使用的时候，他首先会创建一个数组，数组的长度就是调用该方法是传递的实参的个数，然后再把参数值全部放到这个数组当中，然后再把这个数组作为参数传递到被调用的方法中。

* 枚举

  当我们使用 `enmu` 来定义一个枚举类型的时候，编译器会自动帮我们创建一个 `final` 类型的类继承 `Enum` 类，所以枚举类型不能被继承。

* 内部类

* 条件编译

  —般情况下，程序中的每一行代码都要参加编译。但有时候出于对程序代码优化的考虑，希望只对其中一部分内容进行编译，此时就需要在程序中加上条件，让编译器只对满足条件的代码进行编译，将不满足条件的代码舍弃，这就是条件编译。
  
  Java语法的条件编译，是通过判断条件为常量的if语句实现的。其原理也是Java语言的语法糖。根据if判断条件的真假，编译器直接把分支为false的代码块消除。通过该方式实现的条件编译，必须在方法体内实现，而无法在整个Java类的结构或者类的属性上进行条件编译。

* 断言

  Java在执行的时候默认是不启动断言检查的，如果要开启断言检查，则需要用开关 `-enableassertions` 或 `-ea` 来开启。其实断言的底层实现就是if语言，如果断言结果为true，则什么都不做，程序继续执行，如果断言结果为false，则程序抛出AssertError来打断程序的执行。

* 数值字面量

  在java 7中，数值字面量，不管是整数还是浮点数，都允许在数字之间插入任意多个下划线。这些下划线不会对字面量的数值产生影响，目的就是方便阅读。在编译时，编译器会自动去掉下划线。

* for-each

  for-each的实现原理其实就是使用了普通的for循环和迭代器。

* try-with-resources

* Lambda表达式

> 扩展搜索：`语法盐` 、 `语法糖精` 