---
title: Java并发编程
categories: [JAVA学习路线, 基础篇]
tags: [java]
top_img: false
---

## Java并发编程

### 并发与并行

#### 什么是并发

在操作系统中，是指一个时间段中有几个程序都处于已启动运行到运行完毕之间，且这几个程序都是在同一个处理机上运行。

#### 什么是并行

并行（Parallel），当系统有一个以上CPU时，当一个CPU执行一个进程时，另一个CPU可以执行另一个进程，两个进程互不抢占CPU资源，可以同时进行，这种方式我们称之为并行(Parallel)。只有在多CPU的情况中，才会发生并行。否则，看似同时发生的事情，其实都是并发执行的。

#### 并发与并行的区别

并发是指在一段时间内宏观上多个程序同时运行。并行指的是同一个时刻，多个任务确实真的在同时运行。

---

### 线程

#### 线程的实现

实现线程主要有三种方式：

1. 继承Thread类，并重写其run()方法。
2. 实现Runnable接口，并实现其run()方法。
3. 实现Callable接口，并实现其call()方法。

#### 线程的状态

![threadstatus](https://www.xfc-exclave.com/upload/2020/11/threadstatus-2fc806bbbfb44d5abc02d7dd32180d36.png)

1. 新建状态：
   使用 new 关键字和 Thread 类或其子类建立一个线程对象后，该线程对象就处于新建状态。它保持这个状态直到程序 start() 这个线程。

2. 就绪状态：
   当线程对象调用了start()方法之后，该线程就进入就绪状态。就绪状态的线程处于就绪队列中，要等待JVM里线程调度器的调度。

3. 运行状态：
   如果就绪状态的线程获取 CPU 资源，就可以执行 run()，此时线程便处于运行状态。处于运行状态的线程最为复杂，它可以变为阻塞状态、就绪状态和死亡状态。

4. 阻塞状态：

   如果一个线程执行了sleep（睡眠）、suspend（挂起）等方法，失去所占用资源之后，该线程就从运行状态进入阻塞状态。在睡眠时间已到或获得设备资源后可以重新进入就绪状态。可以分为三种：

   - 等待阻塞：运行状态中的线程执行 wait() 方法，使线程进入到等待阻塞状态。
   - 同步阻塞：线程在获取 synchronized同步锁失败(因为同步锁被其他线程占用)。
   - 其他阻塞：通过调用线程的 sleep() 或 join() 发出了 I/O请求时，线程就会进入到阻塞状态。当sleep() 状态超时，join() 等待线程终止或超时，或者 I/O 处理完毕，线程重新转入就绪状态。

5. 死亡状态:
   一个运行状态的线程完成任务或者其他终止条件发生时，该线程就切换到终止状态。

#### 优先级

Java线程可以有优先级的设定，高优先级的线程比低优先级的线程有更高的几率得到执行。

1. 记住当线程的优先级没有指定时，所有线程都携带普通优先级。
2. 优先级可以用从1到10的范围指定。10表示最高优先级，1表示最低优先级，5是普通优先级。
3. 记住优先级最高的线程在执行时被给予优先。但是不能保证线程在启动时就进入运行状态。
4. 与在线程池中等待运行机会的线程相比，当前正在运行的线程可能总是拥有更高的优先级。
5. 由调度程序决定哪一个线程被执行。
6. setPriority() 用来设定线程的优先级。
7. 记住在线程开始方法被调用之前，线程的优先级应该被设定。
8. 你可以使用常量，如MIN_PRIORITY,MAX_PRIORITY，NORM_PRIORITY来设定优先级。

线程优先级具有继承性。a线程启动b线程，b线程的优先级和a线程的优先级是一样的。

高优先级的线程总是大部分先执行完，并不是高优先级的完全先执行完。线程的优先级和执行顺序无关。

优先级高的线程执行快。

#### 线程调度

Java提供一个线程调度器来监视和控制Runnable状态的线程。线程的调度策略采用 **抢占式** ，优先级高的线程比优先级低的线程优先执行。在优先级相同的情况下，按照”先到先服务“的原则。每个Java程序都有一个默认的主线程，就是通过JVM启动的第一个线程。对于应用程序，主线程执行的是main()方法。对于Applet主线程是指浏览器加载并执行小应用程序的那一个线程。

子线程是由应用程序创建的线程。

还有一种线程称为守护现成（Daemon），这是一种用于监视其他线程工作的服务线程，优先级为最低。

#### 创建线程的多种方式

在jdk1.5之前，创建线程就只有两种方式，即继承java.lang.Thread类和实现java.lang.Runnable接口；而在JDK1.5以后，增加了两个创建线程的方式，即实现java.util.concurrent.Callable接口和线程池。

**创建线程示例：** 

* 继承 Thread 类

  ```java
  public class ThreadTest {
   
      public static void main(String[] args) {
          // 设置线程名字
          Thread.currentThread().setName("main thread");
          MyThread myThread = new MyThread();
          myThread.setName("子线程:");
          // 开启线程
          myThread.start();
          for(int i = 0;i<5;i++){
              System.out.println(Thread.currentThread().getName() + i);
          }
      }
  }
   
  class MyThread extends Thread{
      
      public void run(){
          for(int i = 0;i < 10; i++){
              System.out.println(Thread.currentThread().getName() + i);
          }
      }
  }
  ```

* 实现 Runnable 接口

  ```java
  public class RunnableTest {
    
      public static void main(String[] args) {
          //设置线程名字
          Thread.currentThread().setName("main thread:");
          Thread thread = new Thread(new MyRunnable());
          thread.setName("子线程:");
          //开启线程
          thread.start();
          for(int i = 0; i <5;i++){
              System.out.println(Thread.currentThread().getName() + i);
          }
      }
  }
    
  class MyRunnable implements Runnable {
    
      @Override
      public void run() {
          for (int i = 0; i < 10; i++) {
              System.out.println(Thread.currentThread().getName() + i);
          }
      }
  }
  ```

* 实现 Callable 接口

  ```java
  public class CallableTest {
   
      public static void main(String[] args) {
          //执行Callable 方式，需要FutureTask 实现实现，用于接收运算结果
          FutureTask<Integer> futureTask = new FutureTask<Integer>(new MyCallable());
          new Thread(futureTask).start();
          //接收线程运算后的结果
          try {
              Integer sum = futureTask.get();
              System.out.println(sum);
          } catch (InterruptedException e) {
              e.printStackTrace();
          } catch (ExecutionException e) {
              e.printStackTrace();
          }
      }
  }
   
  class MyCallable implements Callable<Integer> {
   
      @Override
      public Integer call() throws Exception {
          int sum = 0;
          for (int i = 0; i < 100; i++) {
              sum += i;
          }
          return sum;
      }
  }
  ```

* 自定义线程池

  线程池提供了一个线程队列，队列中保存着所有等待状态的线程。避免了创建与销毁额外开销，提交了响应速度。

  ```java
  public class ThreadPoolExecutorTest {
   
      public static void main(String[] args) {
          //创建线程池
          ExecutorService executorService = Executors.newFixedThreadPool(10);
          ThreadPool threadPool = new ThreadPool();
          for(int i =0;i<5;i++){
              //为线程池分配任务
              executorService.submit(threadPool);
          }
          //关闭线程池
          executorService.shutdown();
      }
  }
   
  class ThreadPool implements Runnable {
   
      @Override
      public void run() {
          for(int i = 0 ;i<10;i++) {
              System.out.println(Thread.currentThread().getName() + ":" + i);
          }
      }
  
  }
  ```

#### 守护线程

#### 线程与进程的区别和关系

---

### 线程池

#### 自己设计线程池

#### submit()和execute()

#### 线程池原理

#### 为什么不允许使用Executors创建线程池

---

### 线程安全

#### 死锁

#### 死锁如何排查

#### 线程安全和内存模型的关系

---

### 锁

#### CAS

#### 乐观锁与悲观锁

#### 数据库相关锁机制

#### 分布式锁

#### 偏向锁

#### 轻量级

#### 重量级锁

#### monitor

#### 锁优化

#### 阻塞锁

---

### 死锁

#### 死锁的原因

#### 死锁的解决办法

---

### synchornized

#### synchornized是如何实现的？

#### synchornized和lock之间关系

#### 不使用synchornized如何实现一个线程安全的单例

#### synchornized和原子性、可见性和有序性之间的关系

---

### volatile

#### happens-before

#### 内存屏障

#### 编译器指令重排和CPU指令重

#### volatile的实现原理

#### volatile和原子性、可见性和有序性之间的关系

#### 有了synchornized为什么还需要volatile

---

### sleep和wait

---

### wait和notify

---

### notify和notifyAll

---

### ThreadLocal

---

### 写一个死锁的程序

---

### 写代码来解决生产者消费者问题

---

### 并发包

#### Thread

#### Runnable

#### Callable

#### ReentrantLock

#### ReentrantReadWriteLock

#### Atomic*

#### Semaphore

#### CountDownLatch

#### ConcurrentHashMap

#### Executors