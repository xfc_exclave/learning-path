---
layout: draft
title: 草稿
date: 2021-08-03 17:22:15
updated: 2021-08-03 17:22:30
---

## SpringBoot教程

> 参考：https://www.yiibai.com/spring-boot

开发 **SpringBoot** 的主要动机是简化配置和部署spring应用程序的过程。

**SpringBoot** 的主要特点

- 创建独立的 **Spring** 应用程序
- 直接嵌入Tomcat，Jetty或Undertow（无需部署WAR文件）
- 提供“初始”的POM文件内容，以简化Maven配置
- 尽可能时自动配置Spring
- 提供生产就绪的功能，如指标，健康检查和外部化配置
- 绝对无代码生成，也不需要XML配置

**SpringBoot核心和限制** 

SpringBoot不是编写应用程序的框架，它可以帮助我们以最少的配置或零配置开发和构建，打包和部署应用程序。

它不是应用程序服务器。但是它是提供应用程序服务器功能的嵌入式servlet容器，而不是SpringBoot本身。
类似地，SpringBoot不实现任何企业Java规范，例如JPA或JMS。 例如，SpringBoot不实现JPA，但它通过为JPA实现(例如Hibernate)自动配置适当的bean来支持JPA。
最后，SpringBoot不使用任何形式的代码生成来完成它的功能。它是利用Spring 4的条件配置功能，以及Maven和Gradle提供的传递依赖关系解析，以在Spring应用程序上下文中自动配置bean。简而言之，SpringBoot的核心就是Spring。

**SpringBoot优点** 

* 易于理解和开发Spring应用
* 提高生产力
* 缩短开发时间

**SpringBoot的限制** 

将现有或传统的Spring Framework项目转换为SpringBoot应用程序是一个非常困难和耗时的过程。

---

### SpringBoot 简介

SpringBoot是一个基于Java的开源框架，用于创建微服务。它由Pivotal Team开发，用于构建独立的生产就绪Spring应用。 Spring Boot为Java开发人员提供了一个很好的平台，可以开发一个可以运行的独立和生产级Spring应用程序。可以开始使用最少的配置，而无需进行整个Spring配置设置。

**SpringBoot是如何工作的？** 

Spring Boot会根据使用 `@EnableAutoConfiguration` 批注添加到项目中的依赖项自动配置应用程序。 例如，如果MySQL数据库在类路径上，但尚未配置任何数据库连接，则Spring Boot会自动配置内存数据库。

spring boot应用程序的入口点是包含 `@SpringBootApplication` 注释和 `main` 方法的类。

Spring Boot使用 `@ComponentScan` 注释自动扫描项目中包含的所有组件。

* SpringBoot Starters

* 自动配置

  > 将 `@EnableAutoConfiguration` 或 `@SpringBootConfiguration` 注解添加到主类文件中，即可实现SpringBoot程序的自动配置。

* SpringBoot 应用程序

  > SpringBoot 应用程序的入口是包含 `@SpringBootConfiguration` 注解的类，该类应具有运行Spring Boot应用程序的主要方法。  `@SpringBootConfiguration` 注解包括自动配置，组件扫描和 SpringBoot 配置。

  SpringBoot 应用程序入口示例：

  ```java
  import org.springframework.boot.SpringApplication;
  import org.springframework.boot.autoconfigure.SpringBootApplication;
  
  @SpringBootApplication
  public class DemoApplication {
     public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
     }
  }
  ```

* 组件扫描

---

### SpringBoot 快速入门

#### SpringBoot CLI

SpringBoot CLI 是一个命令行工具，它用于运行Groovy脚本。它是使用Spring Boot命令行界面创建Spring Boot应用程序的最简单方法。