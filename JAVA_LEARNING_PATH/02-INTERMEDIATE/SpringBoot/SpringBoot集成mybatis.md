---
layout: draft
title: 草稿
date: 2021-08-03 17:22:15
updated: 2021-08-03 17:22:30
---

## SpringBoot集成开发

### 三、Springboot集成freemarker

1. 依赖

   ```xml
   <dependency>
       <groupId>org.springframework.boot</groupId>
       <artifactId>spring-boot-starter-thymeleaf</artifactId>
   </dependency>
   ```

2. 配置

   ```properties
   
   ```

3. 使用

   > 在 `resources/templates` 目录下创建 `freemarker` 文件。在 `controller` 中指定返回视图，将ftl模板文件返回到浏览器。

---



### 四、Swagger2

1. 依赖

   ```xml
   <dependency>
       <groupId>io.springfox</groupId>
       <artifactId>springfox-swagger2</artifactId>
       <version>2.7.0</version>
   </dependency>
   <dependency>
       <groupId>io.springfox</groupId>
       <artifactId>springfox-swagger-ui</artifactId>
       <version>2.7.0</version>
   </dependency>
   ```

2. 配置

   ```properties
   
   ```

3. 使用

   > 创建基于 `@Configuration` 和 `@EnableSwagger2` 的配置类，集中配置api信息。然后再 `controller` 类中使用 `@ApiOperation` 、 `@ApiImplicitParam` 等注解。最后，访问 `localhost:8080/swagger-ui.html` 。

