---
layout: draft
title: 草稿
date: 2021-08-03 17:22:15
updated: 2021-08-03 17:22:30
---

### SpringBoot简介

> SpringBoot是由Pivotal团队在2013年开始研发、2014年4月发布第一个版本的全新开源的轻量级框架。它基于Spring4.0设计，不仅继承了Spring框架原有的优秀特性，而且还通过简化配置来进一步简化了Spring应用的整个搭建和开发过程。另外SpringBoot通过集成大量的框架使得依赖包的版本冲突，以及引用的不稳定性等问题得到了很好的解决。

### SpringBoot 创建 HelloWorld

1. 创建项目

   File -> New -> Project -> Spring Initializr

   ![SpringBoot-001](https://www.xfc-exclave.com/upload/2020/09/SpringBoot-001-1699f379370145d69eaf530ed036f649.png)

2. 配置项目基本信息

   以下为示例选择：

   ![SpringBoot-002](https://www.xfc-exclave.com/upload/2020/09/SpringBoot-002-dc2bcee3bb1740c2b351053563a10958.png)

3. 选择依赖

   根据项目需要选择依赖文件（也可以先不选择，后续再进行配置）。

   ![SpringBoot-003](https://www.xfc-exclave.com/upload/2020/09/SpringBoot-003-456842d6b7ec412a91e1a3e8964bf189.png)

4. 配置基础依赖

   示例 pom.xml

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
       <modelVersion>4.0.0</modelVersion>
   
       <groupId>com.xfc</groupId>
       <artifactId>springboot-demo</artifactId>
       <version>0.0.1-SNAPSHOT</version>
   
       <name>springboot-demo</name>
       <description>Demo project for Spring Boot</description>
   
       <properties>
           <java.version>1.8</java.version>
           <springboot.version>2.1.1.RELEASE</springboot.version>
       </properties>
   
       <dependencies>
           <!--SpringBoot基础依赖-->
           <dependency>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-starter</artifactId>
               <version>${springboot.version}</version>
           </dependency>
   
           <!--SpringBoot web支持-->
           <dependency>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-starter-web</artifactId>
               <version>${springboot.version}</version>
           </dependency>
   
           <!--SpringBoot 测试支持-->
           <dependency>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-starter-test</artifactId>
               <version>${springboot.version}</version>
               <scope>test</scope>
           </dependency>
       </dependencies>
   
       <build>
           <plugins>
               <plugin>
                   <groupId>org.springframework.boot</groupId>
                   <artifactId>spring-boot-maven-plugin</artifactId>
                   <version>${springboot.version}</version>
               </plugin>
           </plugins>
       </build>
   
   </project>
   ```

5. 注意事项

   * 创建项目后， Maven 加载时间长或不成功，或 pom 文件报错。
     1.  `File -> Settings -> Build,Exception,Deployment -> Build Tools -> Maven` 检查 Maven 地址。
     2. `mvn clean` -> `mvn install` 。
   * 其他错误
     1. 检查项目配置的 JDK 版本。
     2. ……

6. 创建测试文件

   `TestController.java` 

   ```java
   package com.xfc.springboot.controller;
   
   import org.springframework.web.bind.annotation.GetMapping;
   import org.springframework.web.bind.annotation.RestController;
   
   /**
    * Description:
    * date: 2020-09-15 9:30
    *
    * @author Jason Chen
    */
   @RestController
   public class TestController {
   
       @GetMapping("/hello")
       public String helloWorld() {
           return "Hello World";
       }
   
   }
   ```

7. 启动及访问

   运行 `SpringbootDemoApplication` 启动项目，并在浏览器访问 `http://localhost:8080/hello` 。

### SpringBoot 单元测试

#### 依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-test</artifactId>
    <version>${springboot.version}</version>
    <scope>test</scope>
</dependency>
```

#### 示例

```java
package com.xfc.springboot;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Description:
 * date: 2020-09-15 11:00
 *
 * @author Jason Chen
 */
@RunWith(SpringRunner.class)//设置启动器
@SpringBootTest(classes={SpringbootDemoApplication.class})//指定启动类
public class ApplicationTests {

    @Test
    public void test1() {
        System.out.println(" -------- 测试代码部分 -------- ");
    }

    @Before
    public void testBefore(){
        //测试前执行
        System.out.println(" <--- 测试执行开始 ---> ");
    }

    @After
    public void testAfter(){
        //测试结束后
        System.out.println(" <--- 测试执行结束 ---> ");
    }
}
```

### SpringBoot 配置 mybatis-generator

#### 依赖

```xml
<!--mybatis-generator代码生成-->
<dependency>
    <groupId>org.mybatis.generator</groupId>
    <artifactId>mybatis-generator-core</artifactId>
    <version>1.3.2</version>
</dependency>

...

<!-- mybatis代码生成插件 -->
<plugin>
    <groupId>org.mybatis.generator</groupId>
    <artifactId>mybatis-generator-maven-plugin</artifactId>
    <version>1.3.2</version>
    <configuration>
        <!--配置文件的位置-->
        <configurationFile>src/main/resources/generatorConfig.xml</configurationFile>
        <verbose>true</verbose>
        <overwrite>true</overwrite>
    </configuration>
    <executions>
        <execution>
            <id>Generate MyBatis Artifacts</id>
            <goals>
                <goal>generate</goal>
            </goals>
        </execution>
    </executions>
    <!--加入mybatis-generator单独使用的驱动包-->
    <dependencies>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.21</version>
        </dependency>
    </dependencies>
</plugin>
```

#### 配置

* generatorConfig.xml

  在resources 目录下创建文件 `generatorConfig.xml` （以下为示例）。

  ```xml
  <?xml version="1.0" encoding="UTF-8"?>
  <!DOCTYPE generatorConfiguration
          PUBLIC "-//mybatis.org//DTD MyBatis Generator Configuration 1.0//EN"
          "http://mybatis.org/dtd/mybatis-generator-config_1_0.dtd">
  
  <generatorConfiguration>
      <properties resource="jdbc.properties"/>
      <context id="Tables" targetRuntime="MyBatis3">
          <!--自动生成代码去掉注释-->
          <commentGenerator>
              <property name="suppressDate" value="true"/>
              <property name="suppressAllComments" value="true"/>
          </commentGenerator>
          <jdbcConnection
                  driverClass="${spring.datasource.driver-class-name}"
                  connectionURL="${spring.datasource.url}"
                  userId="${spring.datasource.username}"
                  password="${spring.datasource.password}">
              <property name="nullCatalogMeansCurrent" value="true" />
          </jdbcConnection>
          <!--生成实体类地址-->
          <javaModelGenerator targetPackage="com.xfc.springboot.entity" targetProject="src/main/java">
              <!-- 从数据库返回的值被清理前后的空格 -->
              <property name="trimStrings" value="true"/>
              <!-- enableSubPackages:是否让schema作为包的后缀 -->
              <property name="enableSubPackages" value="false"/>
          </javaModelGenerator>
          <!-- 生成映射文件的包名和位置-->
          <sqlMapGenerator targetPackage="mapping" targetProject="src/main/resources">
              <property name="enableSubPackages" value="true"/>
          </sqlMapGenerator>
          <!-- 生成DAO的包名和位置-->
          <javaClientGenerator type="XMLMAPPER" targetPackage="com.xfc.springboot.mapper" targetProject="src/main/java">
              <property name="enableSubPackages" value="true"/>
          </javaClientGenerator>
          <!-- 要生成的表 tableName是数据库中的表名或视图名 domainObjectName是实体类名-->
          <table tableName="user" domainObjectName="User" enableCountByExample="false" enableUpdateByExample="false"
                 enableDeleteByExample="false" enableSelectByExample="false" selectByExampleQueryId="false"></table>
      </context>
  </generatorConfiguration>
  ```

* jdbc.properties

  ```properties
  spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
  spring.datasource.url=jdbc:mysql://localhost:3306/springboot-demo?serverTimezone=GMT%2B8
  spring.datasource.username=root
  spring.datasource.password=root
  ```

#### 运行

在终端执行命令 `mybatis-generator:generate -e` 。

或者：

![SpringBoot-005](https://www.xfc-exclave.com/upload/2020/09/SpringBoot-005-d0058bcd28be4e7abd25877ba4582bd5.png)

### SpringBoot 集成 MyBatis

#### 依赖

在基本依赖的基础上，添加如下依赖：

```xml
<!-- 数据库驱动包（Mysql） -->
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>8.0.21</version>
</dependency>

<!--Mybatis支持-->
<dependency>
    <groupId>org.mybatis.spring.boot</groupId>
    <artifactId>mybatis-spring-boot-starter</artifactId>
    <version>2.1.1</version>
</dependency>
```

> 注意保持 mybatis-spring-boot-starter 版本与项目 springboot 版本的匹配。

#### 数据库

以 MySQL 数据库为例。

* 创建数据库表

  ![SpringBoot-004](https://www.xfc-exclave.com/upload/2020/09/SpringBoot-004-28e8f7bfca4346f0b24c55c221f18b14.png)

* SQL

  ```sql
  SET NAMES utf8mb4;
  SET FOREIGN_KEY_CHECKS = 0;
  
  -- ----------------------------
  -- Table structure for user
  -- ----------------------------
  DROP TABLE IF EXISTS `user`;
  CREATE TABLE `user`  (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
    `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
    `age` int(11) NULL DEFAULT NULL,
    `job` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
    `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
  ) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;
  
  -- ----------------------------
  -- Records of user
  -- ----------------------------
  INSERT INTO `user` VALUES (1, 'zhangsan', 'zhang3', 23, '市场专员', NULL);
  INSERT INTO `user` VALUES (2, 'lisi', 'li4', 27, '行政助理', NULL);
  INSERT INTO `user` VALUES (3, 'wangwu', 'wang5', 32, '项目总监', NULL);
  INSERT INTO `user` VALUES (4, 'you', 'ni', 26, '码农', NULL);
  
  SET FOREIGN_KEY_CHECKS = 1;
  ```

#### 代码生成

参考 **SpringBoot 配置 mybatis-generator** 部分。

如配置自动生成代码失败，则需手动创建相关文件，包括：`User.java` 、 `UserMapper.java` 、 `UserMapper.xml` 文件。

#### 配置文件

* application.yml

  ```yaml
  server:
    port: 8080
  
  spring:
    datasource:
      username: root
      password: root
      url: jdbc:mysql://localhost:3306/springboot-demo?useUnicode=true&characterEncoding=utf-8&useSSL=true&serverTimezone=GMT%2B8
      driver-class-name: com.mysql.cj.jdbc.Driver
  
  mybatis:
    mapper-locations: classpath:mapping/*Mapper.xml
    type-aliases-package: com.xfc.springboot.entity
  ```

  > 配置 srping.datasource.url 时，需要注意 serverTimezone 。

* 启动类 `SpringbootDemoApplication.java` 

  在启动类配置注解 `@MapperScan` 自动进行 Mapper 扫描。

  ```java
  package com.xfc.springboot;
  
  import org.mybatis.spring.annotation.MapperScan;
  import org.springframework.boot.SpringApplication;
  import org.springframework.boot.autoconfigure.SpringBootApplication;
  
  @MapperScan("com.xfc.springboot.mapper")
  @SpringBootApplication
  public class SpringbootDemoApplication {
  
      public static void main(String[] args) {
          SpringApplication.run(SpringbootDemoApplication.class, args);
      }
  
  }
  ```

#### 测试示例

1. 创建 `service/UserService.java` 。

   ```java
   package com.xfc.springboot.service;
   
   import com.xfc.springboot.entity.User;
   
   /**
    * Description:
    * date: 2020-09-15 13:41
    *
    * @author Jason Chen
    */
   public interface UserService {
   
       int deleteByPrimaryKey(Integer id);
   
       int insert(User record);
   
       int insertSelective(User record);
   
       User selectByPrimaryKey(Integer id);
   
       int updateByPrimaryKeySelective(User record);
   
       int updateByPrimaryKey(User record);
   }
   ```

2. 创建 `service/impl/UserServiceImpl.java` 。

   ```java
   package com.xfc.springboot.service.impl;
   
   import com.xfc.springboot.entity.User;
   import com.xfc.springboot.mapper.UserMapper;
   import com.xfc.springboot.service.UserService;
   import org.springframework.beans.factory.annotation.Autowired;
   import org.springframework.stereotype.Service;
   
   /**
    * Description:
    * date: 2020-09-15 13:42
    *
    * @author Jason Chen
    */
   @Service
   public class UserServiceImpl implements UserService {
   
       @Autowired
       private UserMapper userMapper;
   
       public int deleteByPrimaryKey(Integer id) {
           return userMapper.deleteByPrimaryKey(id);
       }
   
       public int insert(User record) {
           return userMapper.insert(record);
       }
   
       public int insertSelective(User record) {
           return userMapper.insertSelective(record);
       }
   
       public User selectByPrimaryKey(Integer id) {
           return userMapper.selectByPrimaryKey(id);
       }
   
       public int updateByPrimaryKeySelective(User record) {
           return userMapper.updateByPrimaryKeySelective(record);
       }
   
       public int updateByPrimaryKey(User record) {
           return userMapper.updateByPrimaryKey(record);
       }
   }
   ```

3. 创建 `controller/UserController.java` 

   ```java
   package com.xfc.springboot.controller;
   
   import com.xfc.springboot.entity.User;
   import com.xfc.springboot.service.UserService;
   import org.springframework.beans.factory.annotation.Autowired;
   import org.springframework.web.bind.annotation.GetMapping;
   import org.springframework.web.bind.annotation.PathVariable;
   import org.springframework.web.bind.annotation.RequestMapping;
   import org.springframework.web.bind.annotation.RestController;
   
   /**
    * Description:
    * date: 2020-09-15 14:23
    *
    * @author Jason Chen
    */
   @RestController
   @RequestMapping("/user")
   public class UserController {
   
       @Autowired
       private UserService userService;
   
       @GetMapping("/getUser/{id}")
       public User helloWorld(@PathVariable int id) {
           User user = userService.selectByPrimaryKey(id);
           return user;
       }
   }
   ```

4. 运行项目并在浏览器访问 `http://localhost:8080/user/getUser/1` 。

### SpringBoot 集成日志

SpringBoot默认支持logback日志，因此，不需要在 `pom` 文件中导入依赖，但如果不需要使用默认的 `logback` ，可以通过以下方式引入依赖（下面以log4j2为例）。

1. 依赖

   ```xml
   <dependency>
       <groupId>org.springframework.boot</groupId>
       <artifactId>spring-boot-starter-web</artifactId>
       <exclusions>
           <exclusion>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-starter-logging</artifactId>
           </exclusion>
       </exclusions>
   </dependency>
   <dependency>
       <groupId>org.springframework.boot</groupId>
       <artifactId>spring-boot-starter-log4j2</artifactId>
   </dependency>
   <!-- 也可以使用aop集中配置日志 -->
   <dependency>
       <groupId>org.springframework.boot</groupId>
       <artifactId>spring-boot-starter-aop</artifactId>
   </dependency>
   ```

2. 配置

   ```properties
   logging.level.root=INFO
   logging.level.cn.enjoy.controller=DEBUG
   logging.file =D:\\log\\enjoy.log
   ```

3. 使用

   在需要监控的地方添加日志打印，并测试不同级别的日志。例：

   ```java
   private final Logger logger = LoggerFactory.getLogger(UserController.class);
   ......
   logger.debug("这是个hello的日志");
   ```

   如果需要集中配置日志，可以使用 `@Aspect` 修饰的类配置切点。

   ```java
   package cn.enjoy.utils;
   
   import org.aspectj.lang.JoinPoint;
   import org.aspectj.lang.annotation.AfterReturning;
   import org.aspectj.lang.annotation.Aspect;
   import org.aspectj.lang.annotation.Before;
   import org.aspectj.lang.annotation.Pointcut;
   import org.slf4j.Logger;
   import org.slf4j.LoggerFactory;
   import org.springframework.stereotype.Component;
   import org.springframework.web.context.request.RequestContextHolder;
   import org.springframework.web.context.request.ServletRequestAttributes;
   
   import javax.servlet.http.HttpServletRequest;
   import java.util.Enumeration;
   
   @Aspect
   @Component
   public class WebLogAspect {
   
       private static final Logger logger = LoggerFactory.getLogger(WebLogAspect.class);
   
       @Pointcut("execution(public * cn.enjoy.controller.*.*(..))")
       public void webLog() {
       }
   
       @Before("webLog()")
       public void doBefore(JoinPoint joinPoint) throws Throwable {
           // 接收到请求，记录请求内容
           ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
           HttpServletRequest request = attributes.getRequest();
           // 记录下请求内容
           logger.info("URL : " + request.getRequestURL().toString());
           logger.info("HTTP_METHOD : " + request.getMethod());
           logger.info("IP : " + request.getRemoteAddr());
           Enumeration<String> enu = request.getParameterNames();
           while (enu.hasMoreElements()) {
               String name = (String) enu.nextElement();
               logger.info("name:{},value:{}", name, request.getParameter(name));
           }
       }
   
       @AfterReturning(returning = "ret", pointcut = "webLog()")
       public void doAfterReturning(Object ret) throws Throwable {
           // 处理完请求，返回内容
           logger.info("RESPONSE : " + ret);
       }
   }
   ```

### SpringBoot 集成 Shiro

这里仅对Shiro功能进行基础演示使用。

目录结构：

![mulujiegou](https://www.xfc-exclave.com/upload/2020/11/mulujiegou-8e8d24f0ff224fc9829bc6a94269a6a6.png)

#### 依赖

```xml
<!--SpringBoot基础依赖-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter</artifactId>
</dependency>

<!-- shiro必要依赖 -->
<dependency>
    <groupId>org.apache.shiro</groupId>
    <artifactId>shiro-spring</artifactId>
    <version>1.4.0</version>
</dependency>

<!--SpringBoot web支持-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
<!--页面模板依赖-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-thymeleaf</artifactId>
</dependency>
<!-- lombok -->
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <optional>true</optional>
</dependency>
```

#### 配置

* application.yml

  ```yml
  shiro:
    sessionManager:
      sessionIdCookieEnabled: true
      sessionIdUrlRewritingEnabled: true
    unauthorizedUrl: /unauthorizedurl
    web:
      enabled: true
      successUrl: /index
      loginUrl: /login
  ```

* ShiroConfig.java

  ```java
  package com.xfc.testshiro.shiro.config;
  
  import com.xfc.testshiro.shiro.realm.CustomRealm;
  import org.apache.shiro.mgt.SecurityManager;
  import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
  import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
  import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
  import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
  import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
  import org.springframework.context.annotation.Bean;
  import org.springframework.context.annotation.Configuration;
  
  import java.util.HashMap;
  import java.util.Map;
  
  @Configuration
  public class ShiroConfig {
  
      @Bean
      @ConditionalOnMissingBean
      public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator() {
          DefaultAdvisorAutoProxyCreator defaultAAP = new DefaultAdvisorAutoProxyCreator();
          defaultAAP.setProxyTargetClass(true);
          return defaultAAP;
      }
  
      //将自己的验证方式加入容器
      @Bean
      public CustomRealm myShiroRealm() {
          CustomRealm customRealm = new CustomRealm();
          return customRealm;
      }
  
      //权限管理，配置主要是Realm的管理认证
      @Bean
      public SecurityManager securityManager() {
          DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
          securityManager.setRealm(myShiroRealm());
          return securityManager;
      }
  
      //Filter工厂，设置对应的过滤条件和跳转条件
      @Bean
      public ShiroFilterFactoryBean shiroFilterFactoryBean(SecurityManager securityManager) {
          ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
          shiroFilterFactoryBean.setSecurityManager(securityManager);
          Map<String, String> map = new HashMap<>();
          //登出
          map.put("/logout", "logout");
          //对所有用户认证
          map.put("/**", "authc");
          //登录
          shiroFilterFactoryBean.setLoginUrl("/login");
          //首页
          shiroFilterFactoryBean.setSuccessUrl("/index");
          //错误页面，认证不通过跳转
          shiroFilterFactoryBean.setUnauthorizedUrl("/error");
          shiroFilterFactoryBean.setFilterChainDefinitionMap(map);
          return shiroFilterFactoryBean;
      }
  
  
      @Bean
      public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
          AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
          authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
          return authorizationAttributeSourceAdvisor;
      }
  
  }
  ```

#### 代码

**实体类** 

* User.java

  ```java
  package com.xfc.testshiro.entity;
  
  import lombok.AllArgsConstructor;
  import lombok.Data;
  
  import java.util.Set;
  
  @Data
  @AllArgsConstructor
  public class User {
      private String id;
      private String userName;
      private String password;
      /**
       * 用户对应的角色集合
       */
      private Set<Role> roles;
  }
  ```

* Role.java

  ```java
  package com.xfc.testshiro.entity;
  
  import lombok.AllArgsConstructor;
  import lombok.Data;
  
  import java.util.Set;
  
  @Data
  @AllArgsConstructor
  public class Role {
      private String id;
      private String roleName;
      /**
       * 角色对应权限集合
       */
      private Set<Permissions> permissions;
  }
  ```

* Permission.java

  ```java
  package com.xfc.testshiro.entity;
  
  import lombok.AllArgsConstructor;
  import lombok.Data;
  
  @Data
  @AllArgsConstructor
  public class Permissions {
      private String id;
      private String permissionsName;
  }
  ```

**用户域** 

* CustomRealm.java

  ```java
  package com.xfc.testshiro.shiro.realm;
  
  import com.xfc.testshiro.entity.Permissions;
  import com.xfc.testshiro.entity.Role;
  import com.xfc.testshiro.entity.User;
  import com.xfc.testshiro.service.ILoginService;
  import org.apache.shiro.authc.AuthenticationException;
  import org.apache.shiro.authc.AuthenticationInfo;
  import org.apache.shiro.authc.AuthenticationToken;
  import org.apache.shiro.authc.SimpleAuthenticationInfo;
  import org.apache.shiro.authz.AuthorizationInfo;
  import org.apache.shiro.authz.SimpleAuthorizationInfo;
  import org.apache.shiro.realm.AuthorizingRealm;
  import org.apache.shiro.subject.PrincipalCollection;
  import org.springframework.beans.factory.annotation.Autowired;
  import org.springframework.util.StringUtils;
  
  public class CustomRealm extends AuthorizingRealm {
  
      @Autowired
      private ILoginService loginService;
  
      @Override
      protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
          //获取登录用户名
          String name = (String) principalCollection.getPrimaryPrincipal();
          //查询用户名称
          User user = loginService.getUserByName(name);
          //添加角色和权限
          SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
          for (Role role : user.getRoles()) {
              //添加角色
              simpleAuthorizationInfo.addRole(role.getRoleName());
              //添加权限
              for (Permissions permissions : role.getPermissions()) {
                  simpleAuthorizationInfo.addStringPermission(permissions.getPermissionsName());
              }
          }
          return simpleAuthorizationInfo;
      }
  
      // 这里需要稍微注意一下引入的shiro版本
      @Override
      protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
          if (StringUtils.isEmpty(authenticationToken.getPrincipal())) {
              return null;
          }
          //获取用户信息
          String name = authenticationToken.getPrincipal().toString();
          User user = loginService.getUserByName(name);
          if (user == null) {
              //这里返回后会报出对应异常
              return null;
          } else {
              //这里验证authenticationToken和simpleAuthenticationInfo的信息
              SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(name, user.getPassword().toString(), getName());
              return simpleAuthenticationInfo;
          }
      }
  }
  ```

**业务层** 

* ILoginService.java

  ```java
  package com.xfc.testshiro.service;
  
  import com.xfc.testshiro.entity.User;
  
  public interface ILoginService {
  
      User getUserByName(String name);
  }
  ```

* LoginServiceImpl.java

  ```java
  package com.xfc.testshiro.service.impl;
  
  import com.xfc.testshiro.entity.Permissions;
  import com.xfc.testshiro.entity.Role;
  import com.xfc.testshiro.entity.User;
  import com.xfc.testshiro.service.ILoginService;
  import org.springframework.stereotype.Service;
  
  import java.util.HashMap;
  import java.util.HashSet;
  import java.util.Map;
  import java.util.Set;
  
  @Service
  public class LoginServiceImpl implements ILoginService {
  
      @Override
      public User getUserByName(String name) {
          return getMapByName(name);
      }
  
      // 具体使用时，根据业务情况调整，一般会在此处进行数据库查询。
      private User getMapByName(String userName) {
          Permissions permissions1 = new Permissions("1", "query");
          Permissions permissions2 = new Permissions("2", "add");
  
          // xfc用户具有admin角色，具有query和add权限
          Set<Permissions> permissionsSet = new HashSet<>();
          permissionsSet.add(permissions1);
          permissionsSet.add(permissions2);
          Role role = new Role("1", "admin", permissionsSet);
          Set<Role> roleSet = new HashSet<>();
          roleSet.add(role);
          User user = new User("1", "xfc", "123456", roleSet);
          Map<String, User> map = new HashMap<>();
          map.put(user.getUserName(), user);
  
          // zhangsan用户具有user角色和query权限
          Set<Permissions> permissionsSet1 = new HashSet<>();
          permissionsSet1.add(permissions1);
          Role role1 = new Role("2", "user", permissionsSet1);
          Set<Role> roleSet1 = new HashSet<>();
          roleSet1.add(role1);
          User user1 = new User("2", "zhangsan", "123456", roleSet1);
          map.put(user1.getUserName(), user1);
          return map.get(userName);
      }
  
  }
  ```

**控制层** 

* LoginController.java

  ```java
  package com.xfc.testshiro.controller;
  
  import com.xfc.testshiro.entity.User;
  import lombok.extern.slf4j.Slf4j;
  import org.apache.shiro.SecurityUtils;
  import org.apache.shiro.authc.AuthenticationException;
  import org.apache.shiro.authc.UnknownAccountException;
  import org.apache.shiro.authc.UsernamePasswordToken;
  import org.apache.shiro.authz.AuthorizationException;
  import org.apache.shiro.authz.annotation.RequiresPermissions;
  import org.apache.shiro.authz.annotation.RequiresRoles;
  import org.apache.shiro.subject.Subject;
  import org.springframework.util.StringUtils;
  import org.springframework.web.bind.annotation.GetMapping;
  import org.springframework.web.bind.annotation.RestController;
  
  @Slf4j
  @RestController
  public class LoginController {
  
      @GetMapping("/login")
      public String login(User user) {
          if (StringUtils.isEmpty(user.getUserName()) || StringUtils.isEmpty(user.getPassword())) {
              return "请输入用户名和密码！";
          }
          //用户认证信息
          Subject subject = SecurityUtils.getSubject();
          UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(user.getUserName(), user.getPassword());
          try {
              //进行验证，这里可以捕获异常，然后返回对应信息
              subject.login(usernamePasswordToken);
              subject.checkRole("admin");
              subject.checkPermissions("query", "add");
          } catch (UnknownAccountException e) {
              log.error("用户名不存在！", e);
              return "用户名不存在！";
          } catch (AuthenticationException e) {
              log.error("账号或密码错误！", e);
              return "账号或密码错误！";
          } catch (AuthorizationException e) {
              log.error("没有权限！", e);
              return "没有权限";
          }
          return "login success";
      }
  
      @RequiresRoles("admin")
      @GetMapping("/admin")
      public String admin() {
          return "admin success!";
      }
  
      @RequiresPermissions("query")
      @GetMapping("/index")
      public String index() {
          return "index success!";
      }
  
      @RequiresPermissions("add")
      @GetMapping("/add")
      public String add() {
          return "add success!";
      }
  
  }
  ```

**异常处理** 

* MyExceptionHandler.java

  ```java
  package com.xfc.testshiro.shiro.handler;
  
  import lombok.extern.slf4j.Slf4j;
  import org.apache.shiro.authz.AuthorizationException;
  import org.springframework.web.bind.annotation.ControllerAdvice;
  import org.springframework.web.bind.annotation.ExceptionHandler;
  import org.springframework.web.bind.annotation.ResponseBody;
  
  @Slf4j
  @ControllerAdvice
  public class MyExceptionHandler {
  
      @ExceptionHandler
      @ResponseBody
      public String ErrorHandler(AuthorizationException e) {
          log.error("没有通过权限验证！", e);
          return "没有通过权限验证！";
      }
  }
  ```

#### 测试

启动项目。

浏览器访问：http://localhost:8080/login?userName=xfc&password=123456 登录xfc用户。

浏览器访问：http://localhost:8080/index 查看用户登录后是否仍被拦截。

更换用户：

浏览器访问：http://localhost:8080/login?userName=zhangsan&password=123456 登录zhangsan用户。将返回“没有权限”。

> 扩展：多用户域

### SpringBoot 集成 JSP

#### 依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>

<!--jsp必须-->
<dependency>
    <groupId>org.apache.tomcat.embed</groupId>
    <artifactId>tomcat-embed-jasper</artifactId>
</dependency>

<!-- JSTL (JSP standard Tag Library) JSP 标准标签库 -->
<dependency>
    <groupId>javax.servlet</groupId>
    <artifactId>jstl</artifactId>
</dependency>
```

#### 配置

application.yml

```yml
spring:
  mvc:
    view:
      prefix: /WEB-INF/jsp/
      suffix: .jsp
```

#### 示例代码

* UserController.java

  ```java
  package com.xfc.testjsp.controller;
  
  import org.springframework.stereotype.Controller;
  import org.springframework.ui.Model;
  import org.springframework.web.bind.annotation.RequestMapping;
  
  import java.util.Arrays;
  
  @Controller
  public class UserController {
  
      @RequestMapping("/index")
      public String query(Model model) {
          model.addAttribute("list", Arrays.asList("张三", "李四", "王五"));
          return "index";
      }
  
  }
  ```

* index.jsp

  ```jsp
  <%@ page language="java" contentType="text/html; charset=UTF-8"
           pageEncoding="UTF-8"%>
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
  <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
  <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
  "http://www.w3.org/TR/html4/loose.dtd">
  <html>
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <title>Insert title here</title>
  </head>
  <body>
  <h1>JSP页面</h1>
  <c:forEach items="${list }" var="d">
      ${d}<br>
  </c:forEach>
  
  </body>
  </html>
  ```

#### 使用

在main目录下创建 `webapp/WEB-INF/jsp` 文件夹，再该文件夹下创建 index.jsp 文件，浏览器访问即可，但 `SpringBoot` 提供了jsp的支持，但并不推荐使用jsp进行页面渲染，因此需要在 Project Structure 中进行如下配置：

![project_structure](https://www.xfc-exclave.com/upload/2020/11/project_structure-c14f66537dd94b62a60b87ab4d00b0f6.jpg)

浏览器访问： http://localhost:8080/show

### 【TODO】SpringBoot 集成 Solr

solr详细教程参见《Solr教程》，此处仅做java的简单使用方面的演示。

#### 依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-solr</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
```

#### 配置

```yml
spring:
  application:
    name: solr
  data:
    solr:
      host: http://localhost:8983/solr/book_core
```

### 【TODO】SpringBoot 集成 RabbitMQ

### 【TODO】SpringBoot 性能优化

#### 1. 优化 @SpringBootApplication

#### 2. 启动参数优化

#### 3. 启动容器优化

### 【TODO】SpringBoot 配置多数据源

### 【TODO】SpringBoot 集成 Swagger2

### SpringBoot 使用热部署（devtools）

#### 依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-devtools</artifactId>
</dependency>
```

#### 配置

```properties
spring.thymeleaf.cache=false
```

#### 使用

如果使用IDEA，则需要添加一个插件。

```xml
<plugin>
<groupId>org.springframework.boot</groupId>
<artifactId>spring-boot-maven-plugin</artifactId>
<configuration>
  <!-- 如果不设置fork,那么不会restart,devtools热部署不会起作用-->
  <fork>true</fork>
</configuration>
<executions>
  <execution>
      <goals>
          <goal>repackage</goal>
      </goals>
  </execution>
</executions>
</plugin>
```

`ctrl+shift+F9` 可以手动热部署。

### 【TODO】SpringBoot 集成 Freemarker

