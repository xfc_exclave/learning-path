---
layout: draft
title: 草稿
date: 2021-08-03 17:22:15
updated: 2021-08-03 17:22:30
---

## Hibernate教程

> 参考：http://www.hechaku.com/Hibernate/Hibernatejiaocheng.html

**Hibernate 优点：** 

- **开源和轻量级：** Hibernate框架是根据LGPL许可证和轻量级的开源工具。
- **快速性能：** Hibernate框架的性能很快，因为缓存在Hibernate框架内部使用。 hibernate框架中有两种类型的缓存：一级缓存和二级缓存。一级缓存默认是启用的。
- **数据库独立查询：** HQL(Hibernate查询语言)是面向对象的SQL版本。 它生成数据库独立查询。 所以你不需要编写数据库特定的查询语句。 在Hibernate之前，如果项目更改了数据库，我们需要更改SQL查询，从而导致维护变得非常复杂。
- **自动创建表：** Hibernate框架提供了自动创建数据库表的功能。 因此，无需手动在数据库中创建表。
- **简化复杂连接：** 在hibernate框架中可轻松获取多个表中的数据。
- **提供查询统计和数据库状态：** Hibernate支持查询缓存，并提供有关查询和数据库状态的统计信息。

---

### 体系结构

Hibernate架构包括许多对象持久对象，会话工厂，事务工厂，连接工厂，会话，事务等。

hibernate架构中有`4`层Java应用层，hibernate框架层，反手api层和数据库层。

![hibernatejiagou](http://www.hechaku.com/uploads/allimg/c171030/15093354J340-GY46.jpg)

Hibernate的高级架构，具有映射文件和配置文件。

![jiagoutu](http://www.hechaku.com/uploads/allimg/c171030/15093354J5P-HQF.jpg)