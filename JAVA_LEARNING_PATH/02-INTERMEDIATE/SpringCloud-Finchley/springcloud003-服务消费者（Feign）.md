---
layout: draft
title: 草稿
date: 2021-08-03 17:22:15
updated: 2021-08-03 17:22:30
---

## 服务消费者（Feign）

在微服务架构中，业务都会被拆分成一个独立的服务，服务与服务的通讯是基于http restful的。Spring cloud有两种服务调用方式，一种是ribbon+restTemplate，另一种是feign。

### 一、Feign简介

> Feign is a Java to HTTP client binder inspired by Retrofit, JAXRS-2.0, and WebSocket. Feign's first goal was reducing the complexity of binding Denominator uniformly to HTTP APIs regardless of ReSTfulness.
>
> ---- 摘自https://github.com/OpenFeign/feign

> Feign是一个声明式的伪Http客户端，它使得写Http客户端变得更简单。使用Feign，只需要创建一个接口并注解。它具有可插拔的注解特性，可使用Feign 注解和JAX-RS注解。Feign支持可插拔的编码器和解码器。Feign默认集成了Ribbon，并和Eureka结合，默认实现了负载均衡的效果。
>
> 简而言之：
>
> - Feign 采用的是基于接口的注解
> - Feign 整合了ribbon，具有负载均衡的能力
> - 整合了Hystrix，具有熔断的能力
>
> ---- 摘自https://blog.csdn.net/forezp/article/details/81040965



### 二、准备工作

根据springcloud001文档，同时创建Eureka Server和两个Eureka Client。

注意：除端口不同外，两个Eureka Client的代码完全相同，以不同端口来模拟负载均衡。



### 三、创建服务消费者

#### 1. 新建一个module，并添加feign及eureka client依赖。

```xml
<dependency>
	<groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-openfeign</artifactId>
</dependency>
```

#### 2. 配置文件

```yml
eureka:
  client:
    serviceUrl:
      defaultZone: http://localhost:8761/eureka/
server:
  port: 8765
spring:
  application:
    name: service-feign
```

#### 3. 配置启动类

在启动类中添加 `@EnableEurekaClient` `@EnableFeignClients`  及 `@EnableDiscoveryClient` 注解开启Feign的功能。

```java
package com.xfc.service.feign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
@EnableFeignClients
public class ServiceFeignApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceFeignApplication.class, args);
    }

}
```

#### 4. 定义一个Feign接口

**注意：** 此部分与springcloud002中的部分相同。

这里直接在两个EurekaClient的启动类中编写RESTFul风格的接口即可，即修改启动类，如下：

```java
package com.xfc.eureka.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableEurekaClient
@RestController
public class EurekaClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekaClient2Application.class, args);
    }

    @Value("${server.port}")
    String port;

    @RequestMapping("/test")
    public String test(@RequestParam(value = "name", defaultValue = "ErDong") String name) {
        return "hi " + name + " ,this test api is from port: " + port;
    }

}
```

#### 5. 在feign模块中添加测试

4.1 新建TestService.java

```java
package com.xfc.service.feign.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Auther: ErDong
 * @Email: xfc_exclave@163.com
 * @Date: 2019/12/27 0027 14:38
 * @Description:
 */
@FeignClient(value = "EUREKA-CLIENT")// 指定服务名称
public interface TestService {

    // 指定服务方法及参数
    @RequestMapping(value = "/test",method = RequestMethod.GET)
    String test(@RequestParam(value = "name") String name);

}
```

5.2 新建TestController.java

```java
package com.xfc.service.feign.controller;

import com.xfc.service.feign.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther: ErDong
 * @Email: xfc_exclave@163.com
 * @Date: 2019/12/27 0027 14:40
 * @Description:
 */
@RestController
public class TestController {

    @Autowired
    TestService testService;

    @GetMapping(value = "/test")
    public String test(@RequestParam String name) {
        return testService.test(name);
    }
}
```



### 四、启动服务及测试负载均衡

1. 分别启动EurekaServer，两个EurekaClient及ServiceFeign。

2. 访问 http://localhost:8761/ 已注册的服务实例。

   可以看到端口分别为 `8762` 和 `8763` 两个 **EUREKA-CLIENT** 实例，及一个端口为 `8765` 的 **SERVICE-FEIGN** 实例。

3. 负载均衡测试

   多次访问 http://localhost:8765/test?name=testUser

   可以看到相应结果如下：

   hi testUser ,this test api is from port: 8762

   hi testUser ,this test api is from port: 8763

   hi testUser ,this test api is from port: 8762

   hi testUser ,this test api is from port: 8763

   hi testUser ,this test api is from port: 8762

   hi testUser ,this test api is from port: 8763

   ……

4. 测试结论

   从测试结果我们可以看到，端口分别为8762何8763的两个 **EUREKA-CLIENT** 实例被轮流调用，即实现了负载均衡。