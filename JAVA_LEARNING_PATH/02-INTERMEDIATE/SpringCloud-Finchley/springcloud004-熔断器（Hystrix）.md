---
layout: draft
title: 草稿
date: 2021-08-03 17:22:15
updated: 2021-08-03 17:22:30
---

## 熔断器（Hystrix）

在微服务架构中，根据业务来拆分成一个个的服务，服务与服务之间可以相互调用（RPC），在Spring Cloud可以用RestTemplate+Ribbon和Feign来调用。为了保证其高可用，单个服务通常会集群部署。由于网络原因或者自身的原因，服务并不能保证100%可用，如果单个服务出现问题，调用这个服务就会出现线程阻塞，此时若有大量的请求涌入，Servlet容器的线程资源会被消耗完毕，导致服务瘫痪。服务与服务之间的依赖性，故障会传播，会对整个微服务系统造成灾难性的严重后果，这就是服务故障的“雪崩”效应。为了解决这个问题，业界提出了熔断器（断路器）模型。

### 一、熔断器简介

> Hystrix is a latency and fault tolerance library designed to isolate points of access to remote systems, services and 3rd party libraries, stop cascading failure and enable resilience in complex distributed systems where failure is inevitable.
>
> ---- 摘自https://github.com/Netflix/hystrix

> Hystrix 中文介绍可参考 https://www.jianshu.com/p/76dc45523807



### 二、准备工作

根据springcloud001文档，同时创建Eureka Server和两个Eureka Client。

注意：除端口不同外，两个Eureka Client的代码完全相同，以不同端口来模拟负载均衡。



### 三、在ribbon中使用断路器

**注意：** 此部分内容基于springcloud002文档。

#### 1. 向 `serice-ribbon` 模块添加依赖

```xml
<dependency>
	<groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-hystrix</artifactId>
</dependency>
```

#### 2. 配置启动类

在启动类中添加 `@EnableHystrix` 注解开启Hystrix功能。

```java
package com.xfc.service.ribbon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
@EnableHystrix
public class ServiceRibbonApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceRibbonApplication.class, args);
    }

    @Bean
    @LoadBalanced// 开启负载均衡
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

}
```

#### 3. 改写TestService

```java
package com.xfc.service.ribbon.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @Auther: ErDong
 * @Email: xfc_exclave@163.com
 * @Date: 2019/12/27 0027 12:46
 * @Description:
 */
@Service
public class TestService {

    @Autowired
    RestTemplate restTemplate;

    // 对当前方法创建熔断器的功能，并指定fallbackMethod熔断方法
    @HystrixCommand(fallbackMethod = "testError")
    public String test(String name) {
        return restTemplate.getForObject("http://EUREKA-CLIENT/test?name=" + name, String.class);
    }

    // fallbackMethod熔断方法
    public String testError(String name) {
        return "hi,"+name+",sorry,error!";
    }

}
```



#### 4. 启动服务及测试熔断器功能

1. 分别启动EurekaServer，两个EurekaClient及ServiceRibbon。

2. 访问 http://localhost:8761/ 已注册的服务实例。

   可以看到端口分别为 `8762` 和 `8763` 两个 **EUREKA-CLIENT** 实例，及一个端口为 `8764` 的 **SERVICE-FEIGN** 实例。

3. 熔断器功能测试

   多次访问 http://localhost:8764/test?name=testUser

   可以看到相应结果如下：

   hi testUser ,this test api is from port: 8762

   hi testUser ,this test api is from port: 8763

   hi testUser ,this test api is from port: 8762

   hi testUser ,this test api is from port: 8763

   hi testUser ,this test api is from port: 8762

   hi testUser ,this test api is from port: 8763

   ……

   此时断开端口为 `8763` 的 **EUREKA-CLIENT** 实例。

   再次进行多次访问，结果如下：

   hi,testUser,sorry,error!

   hi testUser ,this test api is from port: 8762

   hi,testUser,sorry,error!

   hi testUser ,this test api is from port: 8762

   hi testUser ,this test api is from port: 8762

   ……

   此时再断开端口为 `8762` 的 **EUREKA-CLIENT** 实例。

   再次进行多次访问，结果如下：

   hi,testUser,sorry,error!

   hi,testUser,sorry,error!

   hi,testUser,sorry,error!

   hi,testUser,sorry,error!

   ……

4. 测试结论

   从测试结果我们可以看到，端口分别为8762何8763的两个 **EUREKA-CLIENT** 实例在正常启用时，正常实现负载均衡，当其中某个服务实例出现故障时，客户端会进入fallbackMethod指定的熔断方法，直接返回一组字符串，而不是等待响应超时，这很好的控制了容器的线程阻塞。



### 四、在feign中使用断路器

Feign是自带断路器的，在D版本的Spring Cloud之后，默认关闭。

#### 1. 向 `serice-feign` 模块添加依赖

```xml
<dependency>
	<groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-hystrix</artifactId>
</dependency>
```

#### 2. 配置文件

添加熔断器配置：

```yml
# feign开启hystrix支持
feign:
  hystrix:
    enabled: true
```

#### 3. 改写TestService

新建TestService接口的实现类TestServiceHystric.java作为熔断器。

```java
package com.xfc.service.feign.service.hystric;

import com.xfc.service.feign.service.TestService;
import org.springframework.stereotype.Component;

/**
 * @Auther: ErDong
 * @Email: xfc_exclave@163.com
 * @Date: 2019/12/27 0027 15:39
 * @Description:
 */
@Component
public class TestServiceHystric implements TestService {

    @Override
    public String test(String name) {
        return "hi,"+name+",sorry,error!";
    }

}
```

在@FeignClient注解中指定fallback

```java
package com.xfc.service.feign.service;

import com.xfc.service.feign.service.hystric.TestServiceHystric;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Auther: ErDong
 * @Email: xfc_exclave@163.com
 * @Date: 2019/12/27 0027 14:38
 * @Description:
 */
@FeignClient(value = "EUREKA-CLIENT", fallback = TestServiceHystric.class)// 指定服务名称，指定熔断器
public interface TestService {

    // 指定服务方法及参数
    @RequestMapping(value = "/test", method = RequestMethod.GET)
    String test(@RequestParam(value = "name") String name);

}
```

#### 4. 启动服务及测试熔断器功能

与ribbon中的测试方法相同。