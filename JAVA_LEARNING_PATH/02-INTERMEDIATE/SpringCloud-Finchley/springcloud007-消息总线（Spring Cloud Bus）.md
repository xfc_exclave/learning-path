---
layout: draft
title: 草稿
date: 2021-08-03 17:22:15
updated: 2021-08-03 17:22:30
---

## 消息总线（Spring Cloud Bus）

Spring Cloud Bus 将分布式的节点用轻量的消息代理连接起来。它可以用于广播配置文件的更改或者服务之间的通讯，也可以用于监控。本文要讲述的是用Spring Cloud Bus实现通知微服务架构的配置文件的更改。

### 一、准备工作

下载安装Erlang及RabbitMQ。



### 二、改造config-client模块

#### 1. 依赖

添加bus-ampq依赖。

```xml
<dependency>
	<groupId>org.springframework.cloud</groupId>
	<artifactId>spring-cloud-starter-bus-amqp</artifactId>
</dependency>
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```

#### 2. 配置文件

修改配置文件。

```yml
server:
  port: 8881

eureka:
  client:
    serviceUrl:
      defaultZone: http://localhost:8761/eureka/

management:
  endpoints:
    web:
      exposure:
        include: bus-refresh

spring:
  application:
    name: config-client
  cloud:
    config:
      label: master
      profile: dev # dev：开发环境，test：测试环境，pro：生产环境
      discovery:
        enabled: true
        serviceId: config-server # 通过服务名指定配置服务中心（需要config-server和config-client都在服务注册中心注册）
#      uri: http://localhost:8888/ # 通过网址指定配置服务中心
    bus:
      enabled: true
      trace:
        enabled: true
  rabbitmq:
    host: localhost
    password: guest
    port: 5672
    username: guest
```

#### 3. 启动类

启动类中添加类注解 `@RefreshScope`



### 三、 启动服务及测试

复制一份 `config-client` ，端口改为8882。

依次启动 `eureka-server` ， `config-server` 及端口为8881和8882的两个 `config-client` 。

访问 http://localhost:8881/getFoo

访问 http://localhost:8882/getFoo

返回结果均如下：

> foo version 3

此时前往配置中心仓库修改foo配置为 `foo version 22`

访问getFoo，返回结果仍为 `foo version 3`

使用http工具发送POST请求

```
http://localhost:8881/actuator/bus-refresh
```

查看 `config-server` 控制台，显示已从配置中心仓库中获取到最新的配置文件。

再次访问getFoo，返回结果如下：

> foo version 22

结论：

> 当git文件更改的时候，通过pc端用post 向端口为8882的config-client发送请求/bus/refresh／；此时8882端口会发送一个消息，由消息总线向其他服务传递，从而使整个微服务集群都达到更新配置文件。