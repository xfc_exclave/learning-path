---
layout: draft
title: 草稿
date: 2021-08-03 17:22:15
updated: 2021-08-03 17:22:30
---

## 服务消费者（rest+ribbon）

在微服务架构中，业务都会被拆分成一个独立的服务，服务与服务的通讯是基于http restful的。Spring cloud有两种服务调用方式，一种是ribbon+restTemplate，另一种是feign。

### 一、ribbon简介

> Ribbon is a client side IPC library that is battle-tested in cloud. It provides the following features
>
> - Load balancing
>
> - Fault tolerance
>
> - Multiple protocol (HTTP, TCP, UDP) support in an asynchronous and reactive model
>
> - Caching and batching
>
>   ---- 摘自https://github.com/Netflix/ribbon

> Spring Cloud Ribbon是一个基于HTTP和TCP的客户端负载均衡工具，它基于Netflix Ribbon实现。通过Spring Cloud的封装，可以让我们轻松地将面向服务的REST模版请求自动转换成客户端负载均衡的服务调用。Spring Cloud Ribbon虽然只是一个工具类框架，它不像服务注册中心、配置中心、API网关那样需要独立部署，但是它几乎存在于每一个Spring Cloud构建的微服务和基础设施中。因为微服务间的调用，API网关的请求转发等内容，实际上都是通过Ribbon来实现的，包括后续我们将要介绍的Feign，它也是基于Ribbon实现的工具。
>
> ---- 摘自https://www.jianshu.com/p/1bd66db5dc46



### 二、准备工作

根据springcloud001文档，同时创建Eureka Server和两个Eureka Client。

注意：除端口不同外，两个Eureka Client的代码完全相同，以不同端口来模拟负载均衡。



### 三、创建服务消费者

#### 1. 新建一个module，并添加ribbon及eureka client依赖。

```xml
<dependency>
	<groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-ribbon</artifactId>
</dependency>
```

#### 2. 配置文件

```xml
server:
  port: 8764

eureka:
  client:
    serviceUrl:
      defaultZone: http://localhost:8761/eureka/

spring:
  application:
    name: service-ribbon
```

#### 3. 配置启动类

在启动类中添加 `@EnableEurekaClient` 及 `@EnableDiscoveryClient` 注解，并注入一个开启负载均衡的RESTFul模板。

```java
package com.xfc.service.ribbon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
public class ServiceRibbonApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceRibbonApplication.class, args);
    }

    @Bean
    @LoadBalanced// 开启负载均衡
    RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
```

#### 4. 在EurekaClient的服务中编写接口

这里直接在两个EurekaClient的启动类中编写RESTFul风格的接口即可，即修改启动类，如下：

```java
package com.xfc.eureka.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableEurekaClient
@RestController
public class EurekaClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekaClient2Application.class, args);
    }

    @Value("${server.port}")
    String port;

    @RequestMapping("/test")
    public String test(@RequestParam(value = "name", defaultValue = "ErDong") String name) {
        return "hi " + name + " ,this test api is from port: " + port;
    }

}
```

#### 5. 在ribbon模块中添加测试

5.1 新建TestService.java

```java
package com.xfc.service.ribbon.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @Auther: ErDong
 * @Email: xfc_exclave@163.com
 * @Date: 2019/12/27 0027 12:46
 * @Description:
 */
@Service
public class TestService {

    @Autowired
    RestTemplate restTemplate;

    public String test(String name) {
        return restTemplate.getForObject("http://EUREKA-CLIENT/test?name=" + name, String.class);
    }

}
```

5.1 新建TestController.java

```java
package com.xfc.service.ribbon.controller;

import com.xfc.service.ribbon.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther: ErDong
 * @Email: xfc_exclave@163.com
 * @Date: 2019/12/27 0027 12:56
 * @Description:
 */
@RestController
public class TestController {

    @Autowired
    TestService testService;

    @GetMapping(value = "/test")
    public String test(@RequestParam String name) {
        return testService.test(name);
    }
}
```



### 四、启动服务及测试负载均衡

1. 分别启动EurekaServer，两个EurekaClient及ServiceRibbon。

2. 访问 http://localhost:8761/ 已注册的服务实例。

   可以看到端口分别为 `8762` 和 `8763` 两个 **EUREKA-CLIENT** 实例，及一个端口为 `8764` 的 **EUREKA-CLIENT** 实例。

3. 负载均衡测试

   多次访问 http://localhost:8764/test?name=testUser

   可以看到相应结果如下：

   hi testUser ,this test api is from port: 8762

   hi testUser ,this test api is from port: 8763

   hi testUser ,this test api is from port: 8762

   hi testUser ,this test api is from port: 8763

   hi testUser ,this test api is from port: 8762

   hi testUser ,this test api is from port: 8763

   ……

4. 测试结论

   从测试结果我们可以看到，端口分别为8762何8763的两个 **EUREKA-CLIENT** 实例被轮流调用，即实现了负载均衡。