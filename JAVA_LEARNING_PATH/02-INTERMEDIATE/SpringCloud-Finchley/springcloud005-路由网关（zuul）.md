---
layout: draft
title: 草稿
date: 2021-08-03 17:22:15
updated: 2021-08-03 17:22:30
---

## 路由网关（zuul）

在微服务架构中，需要几个基础的服务治理组件，包括服务注册与发现、服务消费、负载均衡、断路器、智能路由、配置管理等，由这几个基础组件相互协作，共同组建了一个简单的微服务系统。

在Spring Cloud微服务系统中，一种常见的负载均衡方式是，客户端的请求首先经过负载均衡（zuul、Ngnix），再到达服务网关（zuul集群），然后再到具体的服。，服务统一注册到高可用的服务注册中心集群，服务的所有的配置文件由配置服务管理（下一篇文章讲述），配置服务的配置文件放在git仓库，方便开发人员随时改配置。

### 一、Zuul简介

> Zuul is the front door for all requests from devices and web sites to the backend of the Netflix streaming application. As an edge service application, Zuul is built to enable dynamic routing, monitoring, resiliency and security. It also has the ability to route requests to multiple Amazon Auto Scaling Groups as appropriate.
>
> ---- 摘自https://github.com/Netflix/zuul/wiki

> Zuul的主要功能是路由转发和过滤器。路由功能是微服务的一部分，比如/api/user转发到到user服务，/api/shop转发到到shop服务。zuul默认和Ribbon结合实现了负载均衡的功能。
>
> zuul有以下功能：
>
> - Authentication
> - Insights
> - Stress Testing
> - Canary Testing
> - Dynamic Routing
> - Service Migration
> - Load Shedding
> - Security
> - Static Response handling
> - Active/Active traffic management
>
> ---- 摘自https://blog.csdn.net/forezp/article/details/81041012



### 二、准备工作

接续上一文档，在原有工程上添加新的模块。



### 三、创建zuul路由网关

#### 1. 新建一个module，并添加zuul及eureka client依赖。

```xml
<dependency>
	<groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-zuul</artifactId>
</dependency>
```

#### 2. 配置文件

```yml
server:
  port: 8769

eureka:
  client:
    serviceUrl:
      defaultZone: http://localhost:8761/eureka/

spring:
  application:
    name: service-zuul

# 以/api-a/开头的请求转发至service-ribbon服务
# 以/api-b/开头的请求转发至service-feign服务
zuul:
  routes:
    api-a:
      path: /api-a/**
      serviceId: service-ribbon
    api-b:
      path: /api-b/**
      serviceId: service-feign
```

#### 3. 配置启动类

在启动类中添加 `@EnableEurekaClient` 及 `@EnableZuulProxy`开启zuul的功能。

```java
package com.xfc.service.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
@EnableEurekaClient
@EnableDiscoveryClient
public class ServiceZuulApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceZuulApplication.class, args);
    }

}
```

#### 4. 启动服务及测试路由网关

 1. 分别启动EurekaServer，两个EurekaClient及ServiceRibbon。

 2. 分别启动ServiceRibbon和ServiceFeign两个服务消费者。

 3. 启动ServiceZuul网关。

 4. 访问：

    多次访问 http://localhost:8769/api-a/test?name=testUser

    结果如下：

    hi testUser ,this test api is from port: 8762

    hi testUser ,this test api is from port: 8763

    hi testUser ,this test api is from port: 8762

    hi testUser ,this test api is from port: 8763

    ……

    多次访问 http://localhost:8769/api-b/test?name=testUser

    结果如下：

    hi testUser ,this test api is from port: 8762

    hi testUser ,this test api is from port: 8763

    hi testUser ,this test api is from port: 8762

    hi testUser ,this test api is from port: 8763

    ……

 5. 测试结论

    zuul起到路由的作用，它将不同请求分向不同的服务消费者进行处理。



### 四、服务过滤

zuul不仅只是路由，并且还能过滤，做一些安全验证。

#### 1. 添加过滤器

新建TokenFilter.java，用于过滤token。

```java
package com.xfc.service.zuul.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @Auther: ErDong
 * @Email: xfc_exclave@163.com
 * @Date: 2019/12/27 21:29
 * @Description:
 */
@Component
public class TokenFilter extends ZuulFilter {

    private static Logger log = LoggerFactory.getLogger(TokenFilter.class);


    /**
     * filterType：返回一个字符串代表过滤器的类型，在zuul中定义了四种不同生命周期的过滤器类型，如下：
     * 1. pre：前置过滤器
     * 2. routing：路由之时
     * 3. post： 路由之后
     * 4. rror：发送错误调用
     *
     * @return
     */
    @Override
    public String filterType() {
        return "pre";
    }

    /**
     * 过滤的顺序，数字越大，优先级越低
     *
     * @return
     */
    @Override
    public int filterOrder() {
        return 0;
    }

    /**
     * 是否要进行过滤
     *
     * @return
     */
    @Override
    public boolean shouldFilter() {
        return true;
    }

    /**
     * 过滤器的具体逻辑
     *
     * @return
     */
    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        log.info(String.format("%s >>> %s", request.getMethod(), request.getRequestURL().toString()));
        Object accessToken = request.getParameter("token");
        if (accessToken == null) {
            log.warn("token is empty");
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(401);
            try {
                ctx.getResponse().getWriter().write("token is empty");
            } catch (Exception e) {
            }
            return null;
        }
        log.info("ok");
        return null;
    }
}
```

#### 2. 启动服务及测试路由过滤

 1. 访问

    访问 http://localhost:8769/api-a/test?name=testUser

    结果：token is empty

    访问 http://localhost:8769/api-b/test?name=testUser

    结果：token is empty

    访问 http://localhost:8769/api-a/test?name=testUser&token=abc

    结果：hi testUser ,this test api is from port: 8762

    访问 http://localhost:8769/api-b/test?name=testUser&token=abc

    结果：hi testUser ,this test api is from port: 8763

 2. 测试结论

    zuul实现了服务过滤。