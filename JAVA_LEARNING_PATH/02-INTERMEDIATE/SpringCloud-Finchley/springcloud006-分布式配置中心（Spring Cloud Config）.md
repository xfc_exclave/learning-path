---
layout: draft
title: 草稿
date: 2021-08-03 17:22:15
updated: 2021-08-03 17:22:30
---

## 分布式配置中心（Spring Cloud Config）

使用配置服务来保存各个服务的配置文件，即Spring Cloud Config。

### 一、简介

> `Spring Cloud Config` 为分布式系统中的外部配置提供服务器和客户端支持。使用 `Config Server` ，您可以在所有环境中管理应用程序的外部属性。客户端和服务器上的概念映射与 `Spring Environment` 和 `PropertySource` 抽象相同，因此它们与Spring应用程序非常契合，但可以与任何以任何语言运行的应用程序一起使用。随着应用程序通过从开发人员到测试和生产的部署流程，您可以管理这些环境之间的配置，并确定应用程序具有迁移时需要运行的一切。服务器存储后端的默认实现使用git，因此它轻松支持标签版本的配置环境，以及可以访问用于管理内容的各种工具。可以轻松添加替代实现，并使用Spring配置将其插入。
>
> ---- 摘自https://www.springcloud.cc/spring-cloud-config.html

> 在分布式系统中，由于服务数量巨多，为了方便服务配置文件统一管理，实时更新，所以需要分布式配置中心组件。在Spring Cloud中，有分布式配置中心组件spring cloud config ，它支持配置服务放在配置服务的内存中（即本地），也支持放在远程Git仓库中。在spring cloud config 组件中，分两个角色，一是config server，二是config client。
> ---- 摘自https://blog.csdn.net/forezp/article/details/81041028



### 二、构建Config Server

#### 1. 新建一个module，并添加config server依赖。

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-config-server</artifactId>
</dependency>
```

#### 2. 配置文件

```yml
server:
  port: 8888

# 如果Git仓库为公开仓库，可以不填写用户名和密码
spring:
  application:
    name: config-server
  cloud:
    config:
      label: master # 配置仓库的分支
      server:
        git:
          uri: https://github.com/forezp/SpringcloudConfig/ # 配置git仓库地址
          searchPaths: respo # 配置仓库路径
          username: # 访问git仓库的用户名
          password: # 访问git仓库的用户密码
```

#### 3. 配置启动类

在启动类中添加 `@EnableConfigServer` 注解开启配置服务器的功能。

```java
package com.xfc.config.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class ConfigServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConfigServerApplication.class, args);
    }

}
```

#### 4. 启动服务及测试ConfigServer

启动服务后，访问 http://localhost:8888/config-client-dev.properties

结果：

> democonfigclient.message: hello spring io
>
> foo: foo version 21

结论：配置服务中心可以从远程程序获取配置信息。

http请求地址和资源文件映射如下：

> /{application}/{profiles:.\[^-\].}
>
> /{application}/{profiles}/{label:.*}
>
> /{application}-{profiles}.properties
>
> /{label}/{application}-{profiles}.properties
>
> {application}-{profiles}.json
>
> /{label}/{application}-{profiles}.json
>
> /{application}-{profiles}.yml 或 /{application}-{profiles}.yml
>
> /{label}/{application}-{profiles}.yml 或 /{label}/{application}-{profiles}.yml



### 三、构建Config Client

#### 1. 新建一个module，并添加config client依赖。

```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-web</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-config</artifactId>
</dependency>
```

#### 2. 配置文件

```yml
server:
  port: 8881

spring:
  application:
    name: config-client
  cloud:
    config:
      label: master
      profile: dev # dev：开发环境，test：测试环境，pro：生产环境
      uri: http://localhost:8888/ # 指明配置服务中心的网址
```

#### 3. 创建测试API

在启动类中添加API。

```java
package com.xfc.config.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class ConfigClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConfigClientApplication.class, args);
    }

    @Value("${foo}")
    String foo;

    @RequestMapping(value = "/getFoo")
    public String getFoo() {
        return foo;
    }

}
```

启动并访问 http://localhost:8881/getFoo

结果：foo version 21

结论：config-client从config-server获取了foo的属性，而config-server是从git仓库读取的。



### 四、高可用的分布式配置中心

#### 1. 改造config-server

 1. 添加eureka client依赖

    ```xml
    <dependency>
    	<groupId>org.springframework.cloud</groupId>
    	<artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
    </dependency>
    ```

 2. 修改配置文件

    ```yml
    server:
      port: 8888
    
    eureka:
      client:
        serviceUrl:
          defaultZone: http://localhost:8761/eureka/
    
    # 如果Git仓库为公开仓库，可以不填写用户名和密码
    spring:
      application:
        name: config-server
      cloud:
        config:
          label: master # 配置仓库的分支
          server:
            git:
              uri: https://github.com/forezp/SpringcloudConfig/ # 配置git仓库地址
              searchPaths: respo # 配置仓库路径
              username: # 访问git仓库的用户名
              password: # 访问git仓库的用户密码
    ```

	3. 主类添加 `@EnableEurekaClient` 注解

#### 2. 改造config-client

 1. 添加eureka client依赖

    ```xml
    <dependency>
    	<groupId>org.springframework.cloud</groupId>
    	<artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
    </dependency>
    ```

 2. 修改配置文件

    ```yml
    server:
      port: 8881
    
    eureka:
      client:
        serviceUrl:
          defaultZone: http://localhost:8761/eureka/
    
    spring:
      application:
        name: config-client
      cloud:
        config:
          label: master
          profile: dev # dev：开发环境，test：测试环境，pro：生产环境
          discovery:
            enabled: true
            serviceId: config-server # 通过服务名指定配置服务中心（需要config-server和config-client都在服务注册中心注册）
    #      uri: http://localhost:8888/ # 通过网址指定配置服务中心
    ```

	3. 主类添加 `@EnableEurekaClient` 注解

#### 3. 启动并测试

依次启动 `eureka-server` ， `config-server` 和 `config-client` 。

访问 http://localhost:8761/

结果：可以看到 `config-server` 和 `config-client` 均注册到服务注册中心。

访问 http://localhost:8881/getFoo

结果：foo version 21

结论：`config-server` 及 `config-client` 可以同时作为EurekaClient注册到服务注册中心，最终实现高可用。