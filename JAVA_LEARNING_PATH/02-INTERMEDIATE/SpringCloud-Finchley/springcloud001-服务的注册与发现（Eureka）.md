---
layout: draft
title: 草稿
date: 2021-08-03 17:22:15
updated: 2021-08-03 17:22:30
---

## 服务的注册与发现（Eureka）

### 一、SpringCloud简介

以下简介来自SpringCloud官网（https://spring.io/projects/spring-cloud）

> Spring Cloud provides tools for developers to quickly build some of the common patterns in distributed systems (e.g. configuration management, service discovery, circuit breakers, intelligent routing, micro-proxy, control bus, one-time tokens, global locks, leadership election, distributed sessions, cluster state). Coordination of distributed systems leads to boiler plate patterns, and using Spring Cloud developers can quickly stand up services and applications that implement those patterns. They will work well in any distributed environment, including the developer’s own laptop, bare metal data centres, and managed platforms such as Cloud Foundry.
>
> `quote from https://spring.io/projects/spring-cloud`

> Spring Cloud为开发人员提供了快速构建分布式系统中一些常见模式的工具（例如配置管理，服务发现，断路器，智能路由，微代理，控制总线）。分布式系统的协调导致了样板模式, 使用Spring Cloud开发人员可以快速地支持实现这些模式的服务和应用程序。他们将在任何分布式环境中运行良好，包括开发人员自己的笔记本电脑，裸机数据中心，以及Cloud Foundry等托管平台。
>
> `quote from https://www.springcloud.cc/spring-cloud-dalston.html`



### 二、创建Eureka服务注册中心及服务提供者

> ps. 学习完Eureka之后，可以再了解一下Consul。

#### 1. 创建maven主工程

你可以选择从https://start.spring.io/下载初始化项目。

也可以`new ` >> `project ` >> `Spring Initializr`进行初始化项目。

#### 2. 创建Eureka Server注册服务中心

1.1 创建module并选择Eureka Server依赖。

```xml
<dependency>
	<groupId>org.springframework.cloud</groupId>
	<artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
</dependency>
```

1.2 在启动类中添加注解 `@EnableEurekaServer`

```java
package com.xfc.eureka.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class EurekaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekaServerApplication.class, args);
    }

}
```

1.3 配置文件 `application.yml`

```yml
server:
  port: 8761

# 通过eureka.client.registerWithEureka：false和fetchRegistry：false来表明自己是一个eureka server.
eureka:
  instance:
    hostname: localhost
  client:
    registerWithEureka: false
    fetchRegistry: false
    serviceUrl:
      defaultZone: http://${eureka.instance.hostname}:${server.port}/eureka/

spring:
  application:
    name: eurka-server
```

1.4 启动注册服务中心

​		启动主类并访问 `http://localhost:8761/`

#### 3. 创建Eureka Client服务提供者

1.1 创建module并选择Eureka Discovery Client依赖。

```xml
<dependency>
	<groupId>org.springframework.cloud</groupId>
	<artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
</dependency>
```

1.2 在启动类中添加注解 `@EnableEurekaClient`

```java
package com.xfc.eureka.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class EurekaClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekaClientApplication.class, args);
    }

}
```

1.3 配置文件 `application.yml`

```yml
server:
  port: 8762

# 指定当前服务名称
spring:
  application:
    name: eureka-client-1

# 指定eureka-server
eureka:
  client:
    serviceUrl:
      defaultZone: http://localhost:8761/eureka/
```

1.4 启动Eureka Client

​		注意：启动 Eureka Client 时应当保证 Eureka Client也处于启动状态。

​		启动主类并访问 `http://localhost:8761/`进行查看。

​		提示信息： `EMERGENCY! EUREKA MAY BE INCORRECTLY CLAIMING INSTANCES ARE UP WHEN THEY'RE NOT. RENEWALS ARE LESSER THAN THRESHOLD AND HENCE THE INSTANCES ARE NOT BEING EXPIRED JUST TO BE SAFE.` 参考 https://blog.csdn.net/qq_26975307/article/details/86563590

在 `Instances currently registered with Eureka` 列表中查看已注册的Eureka服务实例。



#### Eureka服务注册成功

至此，一个Eureka服务即注册成功，同时注册多个Eureka服务，与Eureka Client方式相同，只须注意端口冲突即可。

