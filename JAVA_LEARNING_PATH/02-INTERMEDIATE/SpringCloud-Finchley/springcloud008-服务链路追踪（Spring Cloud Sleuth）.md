---
layout: draft
title: 草稿
date: 2021-08-03 17:22:15
updated: 2021-08-03 17:22:30
---

## 服务链路追踪（Spring Cloud Sleuth）

### 一、简介

> Add sleuth to the classpath of a Spring Boot application (see below for Maven and Gradle examples), and you will see the correlation data being collected in logs, as long as you are logging requests.
>
> ---- 摘自https://github.com/spring-cloud/spring-cloud-sleuth

Spring Cloud Sleuth 主要功能就是在分布式系统中提供追踪解决方案，并且兼容支持了 zipkin，你只需要在pom文件中引入相应的依赖即可。



### 二、构建server-zipkin

#### 1. 下载 `zipkin`

下载地址：https://dl.bintray.com/openzipkin/maven/io/zipkin/java/zipkin-server/

#### 2. 运行

> java -jar D:\jar\zipkin\zipkin-server-2.10.1-exec.jar

#### 3. 访问

zipkin默认端口为 `9411` 。

浏览器访问：http://localhost:9411



### 三、创建测试模块

创建模块 `service-zipkin-test1` 和 `service-zipkin-test2` 。

#### 1. 创建模块并添加依赖

添加zipkin-client依赖：

```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-web</artifactId>
</dependency>
<dependency>
	<groupId>org.springframework.cloud</groupId>
	<artifactId>spring-cloud-starter-zipkin</artifactId>
</dependency>
```

#### 2. 配置文件

```yml
server:
  port: 8989
spring:
  application:
    name: service-zipkin-test2
  zipkin:
    base-url: http://localhost:9411
    sleuth:
      sampler:
        probability: 1.0
```

#### 3. 启动类

模块 `service-zipkin-test1` 启动类：

```java
package com.xfc.service.zipkin.test1;

import brave.sampler.Sampler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.logging.Level;
import java.util.logging.Logger;

@SpringBootApplication
@RestController
public class ServiceZipkinTest1Application {

    private static final Logger LOG = Logger.getLogger(ServiceZipkinTest1Application.class.getName());

    public static void main(String[] args) {
        SpringApplication.run(ServiceZipkinTest1Application.class, args);
    }

    @Autowired
    private RestTemplate restTemplate;

    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @RequestMapping("/test1")
    public String callTest2() {
        LOG.log(Level.INFO, "interface of test1 application");
        return restTemplate.getForObject("http://localhost:8989/test2", String.class);
    }

    @RequestMapping("/info")
    public String info() {
        LOG.log(Level.INFO, "application info of test1");
        return "application info of test1";

    }

    @Bean
    public Sampler defaultSampler() {
        return Sampler.ALWAYS_SAMPLE;
    }

}
```

模块 `service-zipkin-test2` 启动类：

```java
package com.xfc.service.zipkin.test2;

import brave.sampler.Sampler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.logging.Level;
import java.util.logging.Logger;

@SpringBootApplication
@RestController
public class ServiceZipkinTest2Application {

    public static void main(String[] args) {
        SpringApplication.run(ServiceZipkinTest2Application.class, args);
    }

    private static final Logger LOG = Logger.getLogger(ServiceZipkinTest2Application.class.getName());

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("/test2")
    public String callTest1Info() {
        LOG.log(Level.INFO, "interface of test2 application");
        return restTemplate.getForObject("http://localhost:8988/info", String.class);
    }

    @RequestMapping("/info")
    public String home() {
        LOG.log(Level.INFO, "application info of test2");
        return "application info of test2";
    }

    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    public Sampler defaultSampler() {
        return Sampler.ALWAYS_SAMPLE;
    }

}
```

#### 4. 访问测试

- 参考上文启动server-zipkin服务。

- 启动模块 `service-zipkin-test1` 和 `service-zipkin-test2` 。

- 访问：http://localhost:8989/test2，返回结果：

  > application info of test1

- 访问：http://localhost:8988/test1，返回结果：

  > application info of test1

- 访问：http://localhost:9411/，进行追踪

  即可在 `依赖` 中查看到两个工程模块之间的依赖关系。