---
layout: draft
title: 草稿
date: 2021-08-03 17:22:15
updated: 2021-08-03 17:22:30
---

## 使用wildfly部署项目

Jboss是WildFly的前身。

在正式部署前，先查询一下Jboss与jdk是否存在版本冲突，例如：Jboss7与JDK1.8就存在冲突。

java运行环境及环境变量的配置，需要保持正常。

Jboss的环境变量配不配之都无所谓，但是，不配置环境变量的话，不能直接双击运行Jboss。

当然，不通过双击standalone.bat的方式，我们也是可以运行项目的，运行方式如下：

> D:\wildfly\wildfly-18.0.1.Final\bin>standalone.bat

其实道理和配置环境变量没有什么区别。

从官网下载的wildfly-18.0.1.Final不需要进行任何更改。

**启动项目： **

将项目 `.war` 包放入 `standalone\deployments` 目录下。

重新执行上述的启动命令。

启动成功后，和Tomcat差不多，会有打印出启动耗时，例如：

> started in 13233ms - Started 564 of 790 services (374 services are lazy, passive or on-demand)

然后，访问Jboss首页：localhost:8080，如果显示处WildFly的欢迎页，则成功了。

访问你的项目。



### Jboss与Tomcat对比

1. 不同的服务器

   Tomcat是一个的Java servlet容器和Web服务器。

   JBoss是一个基于Java EE的开源应用程序服务器。

2. 不同的处理范围

   Tomcat可以处理servlet和JSP。

   JBoss可以处理servlet，JSP和EJB，JMS。

3. 不同的规范

   Tomcat使用Sun Microsystems规范。

   JBoss使用Java EE规范。