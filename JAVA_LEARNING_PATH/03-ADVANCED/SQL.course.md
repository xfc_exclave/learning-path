---
layout: draft
title: 草稿
date: 2021-08-03 17:22:15
updated: 2021-08-03 17:22:30
---

### SQL简介

SQL 是用于访问和处理数据库的标准的计算机语言。

**SQL 是什么？** 

- SQL，指结构化查询语言，全称是 Structured Query Language。
- SQL 让您可以访问和处理数据库。
- SQL 是一种 ANSI（American National Standards Institute 美国国家标准化组织）标准的计算机语言。

**RDBMS** 

RDBMS 指关系型数据库管理系统，全称 Relational Database Management System。

RDBMS 是 SQL 的基础，同样也是所有现代数据库系统的基础，比如 MS SQL Server、IBM DB2、Oracle、MySQL 以及 Microsoft Access。

RDBMS 中的数据存储在被称为表的数据库对象中。

表是相关的数据项的集合，它由列和行组成。

### SQL语法

示例：

> 注：假定存在数据库 `spring-demo` 及数据库表 `user`

```sql
mysql> use spring-demo;
Database changed

mysql> set names utf8;
Query OK, 0 rows affected (0.00 sec)

mysql> SELECT * FROM user;
+----+--------------+----------------------------------+-----+----------+
| id | name         | password                         | sex | city     |
+----+--------------+----------------------------------+-----+----------+
| 1  | zhangsan     | 3579b2d44cdc44379fc508e3c10c8e7c | 1   | NanJing  |
| 2  | lisi         | 143bc6b44ace483c84532e10294005b1 | 2   | ShangHai |
| 3  | wangwu       | dd77a31385af4cb8b4fdc6ca99431f83 | 1   | NanJing  |
| 4  | zhaoliu      | 9265bf56365b4b3c9e82feb67a41b79b | 1   | ChengDu  |
| 5  | sunqi        | cb14cb333f0142699e9358412d552118 | 2   | ChangSha |
+----+--------------+----------------------------------+-----+----------+
5 rows in set (0.01 sec)
```

> `use spring-demo` ：用于选择数据库。
>
> `set names utf8` ：设置使用的字符集。
>
> `SELECT * FROM user` ：读取数据表的信息。

SQL 对大小写不敏感：SELECT 与 select 是相同的。

某些数据库系统要求在每条 SQL 语句的末端使用分号。分号是在数据库系统中分隔每条 SQL 语句的标准方法，这样就可以在对服务器的相同请求中执行一条以上的 SQL 语句。

**一些重要的 SQL 命令** 

- **SELECT** - 从数据库中提取数据
- **UPDATE** - 更新数据库中的数据
- **DELETE** - 从数据库中删除数据
- **INSERT INTO** - 向数据库中插入新数据
- **CREATE DATABASE** - 创建新数据库
- **ALTER DATABASE** - 修改数据库
- **CREATE TABLE** - 创建新表
- **ALTER TABLE** - 变更（改变）数据库表
- **DROP TABLE** - 删除表
- **CREATE INDEX** - 创建索引（搜索键）
- **DROP INDEX** - 删除索引

### SELECT

SELECT 语句用于从数据库中选取数据。结果被存储在一个结果表中，称为结果集。

**语法** 

```sql
SELECT column_name,column_name FROM table_name;
-- 或者
SELECT * FROM table_name;
```

> 此后的代码及SQL示例都将尽可能服用其前述示例中已有代码及数据库。

**SELECT Column 实例** 

```sql
SELECT id, name, password, sex, city FROM user;
```

执行结果：

```sql
mysql> SELECT id, name, password, sex, city FROM user;
+----+--------------+----------------------------------+-----+----------+
| id | name         | password                         | sex | city     |
+----+--------------+----------------------------------+-----+----------+
| 1  | zhangsan     | 3579b2d44cdc44379fc508e3c10c8e7c | 1   | NanJing  |
| 2  | lisi         | 143bc6b44ace483c84532e10294005b1 | 2   | ShangHai |
| 3  | wangwu       | dd77a31385af4cb8b4fdc6ca99431f83 | 1   | NanJing  |
| 4  | zhaoliu      | 9265bf56365b4b3c9e82feb67a41b79b | 1   | ChengDu  |
| 5  | sunqi        | cb14cb333f0142699e9358412d552118 | 2   | ChangSha |
+----+--------------+----------------------------------+-----+----------+
```

**SELECT * 实例** 

```sql
SELECT * FROM user;
```

执行结果：

```sql
mysql> SELECT * FROM user;
+----+--------------+----------------------------------+-----+----------+
| id | name         | password                         | sex | city     |
+----+--------------+----------------------------------+-----+----------+
| 1  | zhangsan     | 3579b2d44cdc44379fc508e3c10c8e7c | 1   | NanJing  |
| 2  | lisi         | 143bc6b44ace483c84532e10294005b1 | 2   | ShangHai |
| 3  | wangwu       | dd77a31385af4cb8b4fdc6ca99431f83 | 1   | NanJing  |
| 4  | zhaoliu      | 9265bf56365b4b3c9e82feb67a41b79b | 1   | ChengDu  |
| 5  | sunqi        | cb14cb333f0142699e9358412d552118 | 2   | ChangSha |
+----+--------------+----------------------------------+-----+----------+
```

**结果集中的导航** 

大多数数据库软件系统都允许使用编程函数在结果集中进行导航，比如：Move-To-First-Record、Get-Record-Content、Move-To-Next-Record 等等。

### SELECT DISTINCT

在表中，一个列可能会包含多个重复值，DISTINCT 关键词可用于返回唯一不同的值。

**语法** 

```sql
SELECT DISTINCT column_name,column_name FROM table_name;
```

**SELECT DISTINCT 实例** 

```sql
SELECT DISTINCT sex FROM user;
```

执行结果：

```sql
mysql> SELECT DISTINCT sex FROM user;
+-----+
| sex |
+-----+
| 1   |
| 2   |
+-----+
```

### WHERE

WHERE 子句用于提取那些满足指定条件的记录。

**语法** 

```sql
SELECT column_name,column_name
FROM table_name
WHERE column_name operator value;
```

**WHERE 子句实例** 

```sql
SELECT * FROM user WHERE name = 'NanJing';
```

执行结果：

```sql
mysql> SELECT * FROM user WHERE name = 'NanJing';
+----+--------------+----------------------------------+-----+----------+
| id | name         | password                         | sex | city     |
+----+--------------+----------------------------------+-----+----------+
| 1  | zhangsan     | 3579b2d44cdc44379fc508e3c10c8e7c | 1   | NanJing  |
| 3  | wangwu       | dd77a31385af4cb8b4fdc6ca99431f83 | 1   | NanJing  |
+----+--------------+----------------------------------+-----+----------+
```

**文本字段 vs. 数值字段** 

SQL 使用单引号来环绕文本值（大部分数据库系统也接受双引号），但如果是数值字段，请不要使用引号。

示例：

```sql
SELECT * FROM user WHERE id = 1;
```

执行结果：

```sql
mysql> SELECT * FROM user WHERE id = 1;
+----+--------------+----------------------------------+-----+----------+
| id | name         | password                         | sex | city     |
+----+--------------+----------------------------------+-----+----------+
| 1  | zhangsan     | 3579b2d44cdc44379fc508e3c10c8e7c | 1   | NanJing  |
+----+--------------+----------------------------------+-----+----------+
```

**WHERE 子句中的运算符** 

| 运算符  | 描述                                                     |
| :------ | :------------------------------------------------------- |
| =       | 等于                                                     |
| <>      | 不等于。**注：**在 SQL 的一些版本中，该操作符可被写成 != |
| >       | 大于                                                     |
| <       | 小于                                                     |
| >=      | 大于等于                                                 |
| <=      | 小于等于                                                 |
| BETWEEN | 在某个范围内                                             |
| LIKE    | 搜索某种模式                                             |
| IN      | 指定针对某个列的多个可能值                               |

### AND & OR

AND & OR 运算符用于基于一个以上的条件对记录进行过滤。

如果第一个条件和第二个条件都成立，则 AND 运算符显示一条记录。

如果第一个条件和第二个条件中只要有一个成立，则 OR 运算符显示一条记录。

**AND 运算符实例** 

```sql
SELECT * FROM user WHERE sex = 1 AND city = 'ChengDu';
```

执行结果：

```sql
mysql> SELECT * FROM user WHERE sex = 2 and city = 'ChangSha';
+----+--------------+----------------------------------+-----+----------+
| id | name         | password                         | sex | city     |
+----+--------------+----------------------------------+-----+----------+
| 5  | sunqi        | cb14cb333f0142699e9358412d552118 | 2   | ChangSha |
+----+--------------+----------------------------------+-----+----------+
```

**OR 运算符实例** 

```sql
SELECT * FROM user WHERE sex = 1 OR city = 'ChangSha';
```

执行结果：

```sql
mysql> SELECT * FROM user;
+----+--------------+----------------------------------+-----+----------+
| id | name         | password                         | sex | city     |
+----+--------------+----------------------------------+-----+----------+
| 1  | zhangsan     | 3579b2d44cdc44379fc508e3c10c8e7c | 1   | NanJing  |
| 3  | wangwu       | dd77a31385af4cb8b4fdc6ca99431f83 | 1   | NanJing  |
| 4  | zhaoliu      | 9265bf56365b4b3c9e82feb67a41b79b | 1   | ChengDu  |
| 5  | sunqi        | cb14cb333f0142699e9358412d552118 | 2   | ChangSha |
+----+--------------+----------------------------------+-----+----------+
```

**结合 AND & OR** 

```sql
SELECT * FROM user WHERE sex = 1 AND (city = 'NanJing' OR city = 'ChangSha');
```

执行结果：

```sql
mysql> SELECT * FROM user WHERE sex = 1 AND (city = 'NanJing' OR city = 'ChangSha');
+----+--------------+----------------------------------+-----+----------+
| id | name         | password                         | sex | city     |
+----+--------------+----------------------------------+-----+----------+
| 1  | zhangsan     | 3579b2d44cdc44379fc508e3c10c8e7c | 1   | NanJing  |
+----+--------------+----------------------------------+-----+----------+
```

### ORDER BY

ORDER BY 关键字用于对结果集按照一个列或者多个列进行排序。

ORDER BY 关键字默认按照升序对记录进行排序。如果需要按照降序对记录进行排序，您可以使用 DESC 关键字。

**语法** 

```sql
SELECT column_name, column_name
FROM table_name
ORDER BY column_name, column_name ASC|DESC;
```

**ORDER BY 实例** 

```sql
SELECT * FROM user ORDER BY city;
```

执行结果：

```sql
mysql> SELECT * FROM user ORDER BY city;
+----+--------------+----------------------------------+-----+----------+
| id | name         | password                         | sex | city     |
+----+--------------+----------------------------------+-----+----------+
| 5  | sunqi        | cb14cb333f0142699e9358412d552118 | 2   | ChangSha |
| 4  | zhaoliu      | 9265bf56365b4b3c9e82feb67a41b79b | 1   | ChengDu  |
| 1  | zhangsan     | 3579b2d44cdc44379fc508e3c10c8e7c | 1   | NanJing  |
| 3  | wangwu       | dd77a31385af4cb8b4fdc6ca99431f83 | 1   | NanJing  |
| 2  | lisi         | 143bc6b44ace483c84532e10294005b1 | 2   | ShangHai |
+----+--------------+----------------------------------+-----+----------+
```

**ORDER BY DESC 实例** 

```sql
SELECT * FROM user ORDER BY city DESC;
```

执行结果：

```sql
mysql> SELECT * FROM user ORDER BY city DESC;
+----+--------------+----------------------------------+-----+----------+
| id | name         | password                         | sex | city     |
+----+--------------+----------------------------------+-----+----------+
| 2  | lisi         | 143bc6b44ace483c84532e10294005b1 | 2   | ShangHai |
| 1  | zhangsan     | 3579b2d44cdc44379fc508e3c10c8e7c | 1   | NanJing  |
| 3  | wangwu       | dd77a31385af4cb8b4fdc6ca99431f83 | 1   | NanJing  |
| 4  | zhaoliu      | 9265bf56365b4b3c9e82feb67a41b79b | 1   | ChengDu  |
| 5  | sunqi        | cb14cb333f0142699e9358412d552118 | 2   | ChangSha |
+----+--------------+----------------------------------+-----+----------+
```

**ORDER BY 多列** 

```sql
SELECT * FROM user ORDER BY name, city;
```

执行结果：

```sql
mysql> SELECT * FROM user ORDER BY name, city;
+----+--------------+----------------------------------+-----+----------+
| id | name         | password                         | sex | city     |
+----+--------------+----------------------------------+-----+----------+
| 2  | lisi         | 143bc6b44ace483c84532e10294005b1 | 2   | ShangHai |
| 5  | sunqi        | cb14cb333f0142699e9358412d552118 | 2   | ChangSha |
| 3  | wangwu       | dd77a31385af4cb8b4fdc6ca99431f83 | 1   | NanJing  |
| 1  | zhangsan     | 3579b2d44cdc44379fc508e3c10c8e7c | 1   | NanJing  |
| 4  | zhaoliu      | 9265bf56365b4b3c9e82feb67a41b79b | 1   | ChengDu  |
+----+--------------+----------------------------------+-----+----------+
```

### INSERT INTO

INSERT INTO 语句用于向表中插入新记录。

**语法** 

```sql
INSERT INTO table_name VALUES (value1,value2,value3,...);
-- 或者
INSERT INTO table_name (column1,column2,column3,...) VALUES (value1,value2,value3,...);
```

**INSERT INTO 实例** 

```sql
mysql> INSERT INTO user (name, password, sex, city) VALUES ('chouba', 'faf35b34d08247aaa10ca400dce32e2d', 2, 'TianJin');
Query OK, 1 row affected (0.01 sec)
```

执行结果：

```sql
mysql> INSERT INTO user (name, password, sex, city) VALUES ('chouba', 'faf35b34d08247aaa10ca400dce32e2d', 2, 'TianJin');

mysql> SELECT * FROM user;
+----+--------------+----------------------------------+-----+----------+
| id | name         | password                         | sex | city     |
+----+--------------+----------------------------------+-----+----------+
| 1  | zhangsan     | 3579b2d44cdc44379fc508e3c10c8e7c | 1   | NanJing  |
| 2  | lisi         | 143bc6b44ace483c84532e10294005b1 | 2   | ShangHai |
| 3  | wangwu       | dd77a31385af4cb8b4fdc6ca99431f83 | 1   | NanJing  |
| 4  | zhaoliu      | 9265bf56365b4b3c9e82feb67a41b79b | 1   | ChengDu  |
| 5  | sunqi        | cb14cb333f0142699e9358412d552118 | 2   | ChangSha |
| 6  | chouba       | faf35b34d08247aaa10ca400dce32e2d | 2   | TianJin  |
+----+--------------+----------------------------------+-----+----------+
```

**在指定的列插入数据** 

```sql
INSERT INTO user (name, password, city) VALUES ('baijiu', 'fc9b101a01e244669867dffd3cb177f6', 'WuXi');
-- 在执行时未指定字段会被赋予其定义的默认值
```

执行结果：

```sql
mysql> INSERT INTO user (name, password, city) VALUES ('baijiu', 'fc9b101a01e244669867dffd3cb177f6', 'WuXi');

mysql> SELECT * FROM user;
+----+--------------+----------------------------------+-----+----------+
| id | name         | password                         | sex | city     |
+----+--------------+----------------------------------+-----+----------+
| 1  | zhangsan     | 3579b2d44cdc44379fc508e3c10c8e7c | 1   | NanJing  |
| 2  | lisi         | 143bc6b44ace483c84532e10294005b1 | 2   | ShangHai |
| 3  | wangwu       | dd77a31385af4cb8b4fdc6ca99431f83 | 1   | NanJing  |
| 4  | zhaoliu      | 9265bf56365b4b3c9e82feb67a41b79b | 1   | ChengDu  |
| 5  | sunqi        | cb14cb333f0142699e9358412d552118 | 2   | ChangSha |
| 6  | chouba       | faf35b34d08247aaa10ca400dce32e2d | 2   | TianJin  |
| 7  | baijiu       | fc9b101a01e244669867dffd3cb177f6 | 0   | WuXi     |
+----+--------------+----------------------------------+-----+----------+
```

### UPDATE

UPDATE 语句用于更新表中已存在的记录。

**语法** 

```sql
UPDATE table_name
SET column1 = value1, column2 = value2,...
WHERE some_column = some_value;
```

**SQL UPDATE 实例** 

```sql
UPDATE user SET sex = 1, city = 'LanZhou' WHERE name = 'baijiu';
```

执行结果：

```sql
mysql> UPDATE user SET sex = 1, city = 'LanZhou' WHERE name = 'baijiu';

mysql> SELECT * FROM user;
+----+--------------+----------------------------------+-----+----------+
| id | name         | password                         | sex | city     |
+----+--------------+----------------------------------+-----+----------+
| 1  | zhangsan     | 3579b2d44cdc44379fc508e3c10c8e7c | 1   | NanJing  |
| 2  | lisi         | 143bc6b44ace483c84532e10294005b1 | 2   | ShangHai |
| 3  | wangwu       | dd77a31385af4cb8b4fdc6ca99431f83 | 1   | NanJing  |
| 4  | zhaoliu      | 9265bf56365b4b3c9e82feb67a41b79b | 1   | ChengDu  |
| 5  | sunqi        | cb14cb333f0142699e9358412d552118 | 2   | ChangSha |
| 6  | chouba       | faf35b34d08247aaa10ca400dce32e2d | 2   | TianJin  |
| 7  | baijiu       | fc9b101a01e244669867dffd3cb177f6 | 1   | LanZhou  |
+----+--------------+----------------------------------+-----+----------+
```

**Update 警告！** 

在更新记录时要格外小心！如果省略了 WHERE 字句，会导致当前表所有数据更新。因此，执行没有 WHERE 子句的 UPDATE 要慎重。

### DELETE

DELETE 语句用于删除表中的行。

**语法** 

```sql
DELETE FROM table_name WHERE some_column = some_value;
```

**SQL DELETE 实例** 

```sql
DELETE FROM suser WHERE name like 'wang%' AND city = 'NanJing';
```

执行结果：

```sql
mysql> DELETE FROM suser WHERE name like 'wang%' AND city = 'NanJing';

mysql> SELECT * FROM user;
+----+--------------+----------------------------------+-----+----------+
| id | name         | password                         | sex | city     |
+----+--------------+----------------------------------+-----+----------+
| 1  | zhangsan     | 3579b2d44cdc44379fc508e3c10c8e7c | 1   | NanJing  |
| 2  | lisi         | 143bc6b44ace483c84532e10294005b1 | 2   | ShangHai |
| 4  | zhaoliu      | 9265bf56365b4b3c9e82feb67a41b79b | 1   | ChengDu  |
| 5  | sunqi        | cb14cb333f0142699e9358412d552118 | 2   | ChangSha |
| 6  | chouba       | faf35b34d08247aaa10ca400dce32e2d | 2   | TianJin  |
| 7  | baijiu       | fc9b101a01e244669867dffd3cb177f6 | 1   | LanZhou  |
+----+--------------+----------------------------------+-----+----------+
```

**删除所有数据** 

您可以在不删除表的情况下，删除表中所有的行。这意味着表结构、属性、索引将保持不变：

```sql
DELETE FROM table_name;
-- 或者
DELETE * FROM table_name;
```

> **注释：**在删除记录时要格外小心！因为您不能重来！（后续还需作为演示，这里就不执行删除操作了）

### SELECT TOP

SELECT TOP 子句用于规定要返回的记录的数目，这对于拥有数千条记录的大型表来说，是非常有用的。

> **注意：** 并非所有的数据库系统都支持 SELECT TOP 语句。 MySQL 支持 LIMIT 语句来选取指定的条数数据， Oracle 可以使用 ROWNUM 来选取。

**语法** 

1. SQL Server / MS Access 语法

   ```sql
   SELECT TOP number|percent column_name(s) FROM table_name;
   ```

2. MySQL 语法及实例

   ```sql
   SELECT column_name(s) FROM table_name LIMIT number;
   ```

   ```sql
   SELECT * FROM user LIMIT 5;
   ```

3. Oracle 语法及实例

   ```sql
   SELECT column_name(s) FROM table_name WHERE ROWNUM <= number;
   ```

   ```sql
   SELECT * FROM Persons WHERE ROWNUM <=5;
   ```

**MySQL SELECT LIMIT 实例** 

```sql
SELECT * FROM user LIMIT 5;
```

执行结果：

```sql
mysql> SELECT * FROM user LIMIT 5;
+----+--------------+----------------------------------+-----+----------+
| id | name         | password                         | sex | city     |
+----+--------------+----------------------------------+-----+----------+
| 1  | zhangsan     | 3579b2d44cdc44379fc508e3c10c8e7c | 1   | NanJing  |
| 2  | lisi         | 143bc6b44ace483c84532e10294005b1 | 2   | ShangHai |
| 4  | zhaoliu      | 9265bf56365b4b3c9e82feb67a41b79b | 1   | ChengDu  |
| 5  | sunqi        | cb14cb333f0142699e9358412d552118 | 2   | ChangSha |
| 6  | chouba       | faf35b34d08247aaa10ca400dce32e2d | 2   | TianJin  |
+----+--------------+----------------------------------+-----+----------+
```

**SQL SELECT TOP PERCENT 实例** 

在 Microsoft SQL Server 中还可以使用百分比作为参数。

```sql
SELECT TOP 50 PERCENT * FROM user;
```

### LINK

LIKE 操作符用于在 WHERE 子句中搜索列中的指定模式。

**语法** 

```sql
SELECT column_name(s) FROM table_name WHERE column_name LIKE pattern;
```

**SQL LIKE 操作符实例** 

```sql
SELECT * FROM user WHERE name LIKE 'zhao%';-- LIKE右侧通配
```

执行结果：

```sql
mysql> SELECT * FROM user WHERE name LIKE 'zhao%';
+----+--------------+----------------------------------+-----+----------+
| id | name         | password                         | sex | city     |
+----+--------------+----------------------------------+-----+----------+
| 4  | zhaoliu      | 9265bf56365b4b3c9e82feb67a41b79b | 1   | ChengDu  |
+----+--------------+----------------------------------+-----+----------+
```

除此之外，通配符也可以有如下两种匹配方式：

```sql
SELECT * FROM user WHERE name LIKE '%liu';-- LIKE左侧通配
SELECT * FROM user WHERE name LIKE '%ha%';-- LIKE左右通配
```

### 通配符

通配符可用于替代字符串中的任何其他字符。在 SQL 中，通配符与 SQL LIKE 操作符一起使用。SQL 通配符用于搜索表中的数据。在 SQL 中，可使用以下通配符：

| 通配符                     | 描述                       |
| :------------------------- | :------------------------- |
| %                          | 替代 0 个或多个字符        |
| _                          | 替代一个字符               |
| [charlist]                 | 字符列中的任何单一字符     |
| [^charlist] 或 [!charlist] | 不在字符列中的任何单一字符 |

**使用 SQL % 通配符** 

```sql
SELECT * FROM user WHERE name LIKE 'zhao%';
```

执行结果：

```sql
mysql> SELECT * FROM user WHERE name LIKE 'zhao%';
+----+--------------+----------------------------------+-----+----------+
| id | name         | password                         | sex | city     |
+----+--------------+----------------------------------+-----+----------+
| 4  | zhaoliu      | 9265bf56365b4b3c9e82feb67a41b79b | 1   | ChengDu  |
+----+--------------+----------------------------------+-----+----------+
```

**使用 SQL _ 通配符** 

```sql
SELECT * FROM user WHERE city LIKE '_h_n_ha_';
```

执行结果：

```sql
mysql> SELECT * FROM user WHERE city LIKE '_h_n_ha_';
+----+--------------+----------------------------------+-----+----------+
| id | name         | password                         | sex | city     |
+----+--------------+----------------------------------+-----+----------+
| 2  | lisi         | 143bc6b44ace483c84532e10294005b1 | 2   | ShangHai |
+----+--------------+----------------------------------+-----+----------+
```

**使用 SQL [charlist] 通配符** 

> MySQL 中使用 **REGEXP** 或 **NOT REGEXP** 运算符 (或 RLIKE 和 NOT RLIKE) 来操作正则表达式。

```sql
SELECT * FROM user WHERE name REGEXP '^[zl]';-- 查询name含有z和l字符的数据
```

执行结果：

```sql
mysql> SELECT * FROM user WHERE name REGEXP '^[zl]';
+----+--------------+----------------------------------+-----+----------+
| id | name         | password                         | sex | city     |
+----+--------------+----------------------------------+-----+----------+
| 1  | zhangsan     | 3579b2d44cdc44379fc508e3c10c8e7c | 1   | NanJing  |
| 2  | lisi         | 143bc6b44ace483c84532e10294005b1 | 2   | ShangHai |
| 4  | zhaoliu      | 9265bf56365b4b3c9e82feb67a41b79b | 1   | ChengDu  |
+----+--------------+----------------------------------+-----+----------+
```

### IN

IN 操作符允许您在 WHERE 子句中规定多个值。

**语法** 

```sql
SELECT column_name(s)
FROM table_name
WHERE column_name IN (value1, value2,...);
```

**IN 操作符实例** 

```sql
SELECT * FROM user WHERE city IN ('ChengDu', 'ShangHai');
```

执行结果：

```sql
mysql> SELECT * FROM user WHERE city IN ('ChengDu', 'ShangHai');
+----+--------------+----------------------------------+-----+----------+
| id | name         | password                         | sex | city     |
+----+--------------+----------------------------------+-----+----------+
| 2  | lisi         | 143bc6b44ace483c84532e10294005b1 | 2   | ShangHai |
| 4  | zhaoliu      | 9265bf56365b4b3c9e82feb67a41b79b | 1   | ChengDu  |
+----+--------------+----------------------------------+-----+----------+
```

### BETWEEN

BETWEEN 操作符选取介于两个值之间的数据范围内的值。这些值可以是数值、文本或者日期。

> 为了方便演示，添加 `salary` 列并补充数据，完整数据如下：

```sql
mysql> SELECT * FROM user;
+----+-----------+----------------------------------+-----+----------+--------+
| id | name      | password                         | sex | city     | salary |
+----+-----------+----------------------------------+-----+----------+--------+
| 1  | zhangsan  | 3579b2d44cdc44379fc508e3c10c8e7c | 1   | NanJing  | 8000   |
| 2  | lisi      | 143bc6b44ace483c84532e10294005b1 | 2   | ShangHai | 11000  |
| 4  | zhaoliu   | 9265bf56365b4b3c9e82feb67a41b79b | 1   | ChengDu  | 7000   |
| 5  | sunqi     | cb14cb333f0142699e9358412d552118 | 2   | ChangSha | 13000  |
| 6  | chouba    | faf35b34d08247aaa10ca400dce32e2d | 2   | TianJin  | 10000  |
| 7  | baijiu    | fc9b101a01e244669867dffd3cb177f6 | 1   | LanZhou  | 6500   |
+----+-----------+----------------------------------+-----+----------+--------+
```

**BETWEEN 操作符实例** 

```sql
SELECT * FROM user WHERE salary BETWEEN 7000 and 10000;
```

执行结果：

```sql
mysql> SELECT * FROM user WHERE salary BETWEEN 7000 and 10000;
+----+-----------+----------------------------------+-----+----------+--------+
| id | name      | password                         | sex | city     | salary |
+----+-----------+----------------------------------+-----+----------+--------+
| 1  | zhangsan  | 3579b2d44cdc44379fc508e3c10c8e7c | 1   | NanJing  | 8000   |
| 4  | zhaoliu   | 9265bf56365b4b3c9e82feb67a41b79b | 1   | ChengDu  | 7000   |
| 6  | chouba    | faf35b34d08247aaa10ca400dce32e2d | 2   | TianJin  | 10000  |
+----+-----------+----------------------------------+-----+----------+--------+
```

**NOT BETWEEN 操作符实例** 

```sql
SELECT * FROM user WHERE salary NOT BETWEEN 7000 and 10000;
```

执行结果：

```sql
mysql> SELECT * FROM user WHERE salary NOT BETWEEN 7000 and 10000;
+----+-----------+----------------------------------+-----+----------+--------+
| id | name      | password                         | sex | city     | salary |
+----+-----------+----------------------------------+-----+----------+--------+
| 2  | lisi      | 143bc6b44ace483c84532e10294005b1 | 2   | ShangHai | 11000  |
| 5  | sunqi     | cb14cb333f0142699e9358412d552118 | 2   | ChangSha | 13000  |
| 7  | baijiu    | fc9b101a01e244669867dffd3cb177f6 | 1   | LanZhou  | 6500   |
+----+-----------+----------------------------------+-----+----------+--------|
```

**带有 IN 的 BETWEEN 操作符实例** 

```sql
SELECT * FROM user WHERE (salary BETWEEN 7000 and 10000) AND city NOT IN ('BeiJing', 'NanJing');
```

执行结果：

```sql
mysql> SELECT * FROM user WHERE (salary BETWEEN 7000 and 10000) AND city NOT IN ('BeiJing', 'NanJing');
+----+-----------+----------------------------------+-----+----------+--------+
| id | name      | password                         | sex | city     | salary |
+----+-----------+----------------------------------+-----+----------+--------+
| 1  | zhangsan  | 3579b2d44cdc44379fc508e3c10c8e7c | 1   | NanJing  | 8000   |
| 4  | zhaoliu   | 9265bf56365b4b3c9e82feb67a41b79b | 1   | ChengDu  | 7000   |
| 6  | chouba    | faf35b34d08247aaa10ca400dce32e2d | 2   | TianJin  | 10000  |
+----+-----------+----------------------------------+-----+----------+--------+
```

**带有文本值的 NOT BETWEEN 操作符实例** 

### 别名

通过使用 SQL，可以为表名称或列名称指定别名。基本上，创建别名是为了让列名称的可读性更强。

**语法** 

```sql
SELECT column_name AS alias_name FROM table_name;-- 列的别名语法
SELECT column_name(s) FROM table_name AS alias_name;-- 表的别名语法
```

**列的别名实例** 

```sql
mysql> SELECT id, name AS username, password, sex, city AS usercity, salary FROM user;
+----+-----------+----------------------------------+-----+----------+--------+
| id | username  | password                         | sex | usercity | salary |
+----+-----------+----------------------------------+-----+----------+--------+
| 1  | zhangsan  | 3579b2d44cdc44379fc508e3c10c8e7c | 1   | NanJing  | 8000   |
| 2  | lisi      | 143bc6b44ace483c84532e10294005b1 | 2   | ShangHai | 11000  |
| 4  | zhaoliu   | 9265bf56365b4b3c9e82feb67a41b79b | 1   | ChengDu  | 7000   |
| 5  | sunqi     | cb14cb333f0142699e9358412d552118 | 2   | ChangSha | 13000  |
| 6  | chouba    | faf35b34d08247aaa10ca400dce32e2d | 2   | TianJin  | 10000  |
| 7  | baijiu    | fc9b101a01e244669867dffd3cb177f6 | 1   | LanZhou  | 6500   |
+----+-----------+----------------------------------+-----+----------+--------+
```

```sql
mysql> SELECT name AS username, CONCAT(sex, ',', city, ',', salary) AS user_info  FROM user;-- 合并多列并取别名
+-----------+------------------+
| username  | user_info        |
+-----------+------------------+
| zhangsan  | 1,NanJing,8000   |
| lisi      | 2,ShangHai,11000 |
| zhaoliu   | 1,ChengDu,7000   |
| sunqi     | 2,ChangSha,13000 |
| chouba    | 2,TianJin,10000  |
| baijiu    | 1,LanZhou,6500   |
+-----------+------------------+
```

**表的别名实例** 

```sql
mysql> SELECT u.name, u.city, u.salary, l.count, l.date FROM user u, access_log l WHERE u.id = l.aid
+-----------+-----------+--------+-------+---------------+
| name		+ city		+ salary + count + date			 |
+-----------+-----------+--------+-------+---------------+
| zhangsan	| NanJing	| 8000	 | 45	| 2016-05-10	 |
| lisi		| ShangHai	| 11000	 | 100	| 2016-05-13	 |
| zhaoliu	| ChengDu	| 7000	 | 10	| 2016-05-14	 |
| sunqi		| ChangSha	| 13000	 | 205	| 2016-05-14	 |
| chouba	| TianJin	| 10000	 | 13	| 2016-05-15	 |
| baijiu	| LanZhou	| 6500	 | 220	| 2016-05-15	 |
+-----------+-----------+--------+-------+---------------+
```

> 在下面的情况下，使用别名很有用：
>
> - 在查询中涉及超过一个表
> - 在查询中使用了函数
> - 列名称很长或者可读性差
> - 需要把两个列或者多个列结合在一起

### 连接（JOIN）

SQL JOIN 子句用于把来自两个或多个表的行结合起来，基于这些表之间的共同字段。

> 注：此后的 SQL 示例如非必要，将不再展示运行结果。

```SQL
SELECT
	u.name,
	u.city,
	u.salary,
	l.count,
	l.date 
FROM
	name u
	INNER JOIN access_log l ON u.id = l.aid
```

**不同的 SQL JOIN** 

- **INNER JOIN**：如果表中有至少一个匹配，则返回行
- **LEFT JOIN**：即使右表中没有匹配，也从左表返回所有的行
- **RIGHT JOIN**：即使左表中没有匹配，也从右表返回所有的行
- **FULL JOIN**：只要其中一个表中存在匹配，则返回行

### INNER JOIN

INNER JOIN 关键字在表中存在至少一个匹配时返回行。

**语法** 

```sql
SELECT column_name(s)
FROM table1
INNER JOIN table2
ON table1.column_name = table2.column_name;
-- 或者
SELECT column_name(s)
FROM table1
JOIN table2
ON table1.column_name = table2.column_name;
```

> INNER JOIN 与 JOIN 是相同的。

**示例** 

```sql
SELECT
	u.name,
	u.city,
	u.salary,
	l.count,
	l.date 
FROM
	name u
	INNER JOIN access_log l ON u.id = l.aid
ORDER BY
	count
```

> **注：** INNER JOIN 关键字在表中存在至少一个匹配时返回行。如果表中没有匹配，则不会列出这些行。

### LEFT JOIN

LEFT JOIN 关键字从左表（table1）返回所有的行，即使右表（table2）中没有匹配。如果右表中没有匹配，则结果为 NULL。

**语法** 

```sql
SELECT column_name(s)
FROM table1
LEFT JOIN table2
ON table1.column_name = table2.column_name;
-- 或者
SELECT column_name(s)
FROM table1
LEFT OUTER JOIN table2
ON table1.column_name = table2.column_name;
```

> **注：** 在某些数据库中，LEFT JOIN 称为 LEFT OUTER JOIN。

**SQL LEFT JOIN 实例** 

```sql
SELECT
	u.name,
	u.city,
	u.salary,
	l.count,
	l.date 
FROM
	name u
	LEFT JOIN access_log l ON u.id = l.aid
ORDER BY
	count
```

### RIGHT JOIN

RIGHT JOIN 关键字从右表（table2）返回所有的行，即使左表（table1）中没有匹配。如果左表中没有匹配，则结果为 NULL。

> 出 RIGHT JOIN 关键字外，语法与实例基本与 LEFT JOIN 相同。

### FULL OUTER JOIN（MySQL不支持）

FULL OUTER JOIN 关键字只要左表（table1）和右表（table2）其中一个表中存在匹配，则返回行。

FULL OUTER JOIN 关键字结合了 LEFT JOIN 和 RIGHT JOIN 的结果。

**语法** 

```sql
SELECT column_name(s)
FROM table1
FULL OUTER JOIN table2
ON table1.column_name = table2.column_name;
```

> 实例与上一实例类似。但请注意：**MySQL中不支持 FULL OUTER JOIN** 。

### UNION

UNION 操作符用于合并两个或多个 SELECT 语句的结果集。请注意，UNION 内部的每个 SELECT 语句必须拥有相同数量的列。列也必须拥有相似的数据类型。同时，每个 SELECT 语句中的列的顺序必须相同。

**语法** 

```sql
-- SQL UNION 语法
SELECT column_name(s) FROM table1
UNION
SELECT column_name(s) FROM table2;
-- SQL UNION ALL 语法
SELECT column_name(s) FROM table1
UNION ALL
SELECT column_name(s) FROM table2;
```

> UNION 结果集中的列名总是等于 UNION 中第一个 SELECT 语句中的列名。

新建一个实例数据库表 `customer` 并添加测试数据。

```sql
CREATE TABLE `customer`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `sex` int(11) NULL DEFAULT NULL COMMENT '性别',
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '城市',
  `salary` int(10) NULL DEFAULT NULL COMMENT '薪资',
  `del_flag` int(11) NULL DEFAULT NULL COMMENT '是否已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

INSERT INTO `customer` VALUES (1, 'lisiguang', '3579b2d44cdc44379fc508e3c10c8e7c', 1, 'ShangHai', 500, 0);
INSERT INTO `customer` VALUES (2, 'sunshangxiang', '3579b2d44cdc44379fc508e3c10c8e7c', 1, 'ChengDu', 1200, 0);
```

**SQL UNION 实例** 

```sql
SELECT city FROM user
UNION
SELECT city FROM customer
-- UNION会去除重复的 name 字段值
```

**SQL UNION ALL 实例** 

```sql
SELECT city FROM user
UNION ALL
SELECT city FROM customer
-- UNION ALL会显示重复的数据
```

**带有 WHERE 的 SQL UNION ALL** 

```sql
SELECT
	name, city 
FROM
	user
WHERE
	city = 'ShangHai'
UNION ALL
SELECT
	name, city 
FROM
	customer
WHERE
	city = 'ShangHai'
ORDER BY name
```

### SELECT INTO

SELECT INTO 语句从一个表复制数据，然后把数据插入到另一个新表中。

MySQL 数据库不支持 SELECT ... INTO 语句，但支持 **INSERT INTO ... SELECT** 。

**语法** 

```sql
SELECT *
INTO newtable [IN externaldb]
FROM table1;
-- 或者
SELECT column_name(s)
INTO newtable [IN externaldb]
FROM table1;
```

**SQL SELECT INTO 实例** 

```sql
-- 将user表备份到backup2020
SELECT *
INTO backup2020
FROM user u
LEFT JOIN access_log l
ON u.id = l.aid
```

### INSERT INTO SELECT

INSERT INTO SELECT 语句从一个表复制数据，然后把数据插入到一个已存在的表中。目标表中任何已存在的行都不会受影响。

**语法** 

```sql
INSERT INTO table2
SELECT * FROM table1;
-- 或者
INSERT INTO table2 (column_name(s))
SELECT column_name(s)
FROM table1;s
```

**实例** 

```sql
INSERT INTO user (name, city) SELECT name, city FROM customer;
```

### CREATE DATABASE

CREATE DATABASE 语句用于创建数据库。

**语法** 

```sql
CREATE DATABASE dbname;
```

### CREATE TABLE

CREATE TABLE 语句用于创建数据库中的表。表由行和列组成，每个表都必须有个表名。

**语法** 

```sql
CREATE TABLE table_name
(
column_name1 data_type(size),
column_name2 data_type(size),
column_name3 data_type(size),
....
);
```

**SQL CREATE TABLE 实例** 

> 参见 `UNION` 部分新建数据库表内容。

### Constraints（约束）

SQL 约束用于规定表中的数据规则。如果存在违反约束的数据行为，行为会被约束终止。约束可以在创建表时规定（通过 CREATE TABLE 语句），或者在表创建之后规定（通过 ALTER TABLE 语句）。

**CREATE TABLE + CONSTRAINT 语法** 

```sql
CREATE TABLE table_name
(
column_name1 data_type(size) constraint_name,
column_name2 data_type(size) constraint_name,
column_name3 data_type(size) constraint_name,
....
);
```

在 SQL 中，我们有如下约束：

- **NOT NULL** - 指示某列不能存储 NULL 值。
- **UNIQUE** - 保证某列的每行必须有唯一的值。
- **PRIMARY KEY** - NOT NULL 和 UNIQUE 的结合。确保某列（或两个列多个列的结合）有唯一标识，有助于更容易更快速地找到表中的一个特定的记录。
- **FOREIGN KEY** - 保证一个表中的数据匹配另一个表中的值的参照完整性。
- **CHECK** - 保证列中的值符合指定的条件。
- **DEFAULT** - 规定没有给列赋值时的默认值。

### NOT NULL

NOT NULL 约束强制列不接受 NULL 值。NOT NULL 约束强制字段始终包含值。这意味着，如果不向字段添加值，就无法插入新记录或者更新记录。

**实例** 

```sql
CREATE TABLE Persons (
    ID int NOT NULL,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255) NOT NULL,
    Age int
);
```

**添加 NOT NULL 约束** 

```sql
ALTER TABLE Persons MODIFY Age int NOT NULL;
```

**删除 NOT NULL 约束** 

```sql
ALTER TABLE Persons MODIFY Age int NULL;
```

### UNIQUE

UNIQUE 约束唯一标识数据库表中的每条记录。UNIQUE 和 PRIMARY KEY 约束均为列或列集合提供了唯一性的保证。PRIMARY KEY 约束拥有自动定义的 UNIQUE 约束。

注意，每个表可以有多个 UNIQUE 约束，但是每个表只能有一个 PRIMARY KEY 约束。

#### CREATE TABLE 时的 UNIQUE 约束

**MySQL** 

```sql
CREATE TABLE Persons
(
P_Id int NOT NULL,
LastName varchar(255) NOT NULL,
FirstName varchar(255),
Address varchar(255),
City varchar(255),
UNIQUE (P_Id)
)
```

**SQL Server / Oracle / MS Access**

```sql
CREATE TABLE Persons
(
P_Id int NOT NULL UNIQUE,
LastName varchar(255) NOT NULL,
FirstName varchar(255),
Address varchar(255),
City varchar(255)
)
```

定义多个列的 UNIQUE 约束（**MySQL / SQL Server / Oracle / MS Access**）：

```sql
CREATE TABLE Persons
(
P_Id int NOT NULL,
LastName varchar(255) NOT NULL,
FirstName varchar(255),
Address varchar(255),
City varchar(255),
UNIQUE (P_Id, LastName)
)
```

#### ALTER TABLE 时的 UNIQUE 约束

```sql
ALTER TABLE Persons ADD UNIQUE (P_Id, LastName);
```

#### 撤销 UNIQUE 约束

**MySQL** 

```sql
ALTER TABLE Persons DROP INDEX uc_PersonID;
```

**SQL Server / Oracle / MS Access**

```sql
ALTER TABLE Persons DROP CONSTRAINT uc_PersonID
```

### PRIMARY KEY

PRIMARY KEY 约束唯一标识数据库表中的每条记录，主键必须包含唯一的值，主键列不能包含 NULL 值。

每个表都应该有一个主键，并且每个表只能有一个主键。

#### CREATE TABLE 时的 PRIMARY KEY 约束

**MySQL** 

```sql
CREATE TABLE Persons
(
P_Id int NOT NULL,
LastName varchar(255) NOT NULL,
FirstName varchar(255),
Address varchar(255),
City varchar(255),
PRIMARY KEY (P_Id)
)
```

**SQL Server / Oracle / MS Access** 

```sql
CREATE TABLE Persons
(
P_Id int NOT NULL PRIMARY KEY,
LastName varchar(255) NOT NULL,
FirstName varchar(255),
Address varchar(255),
City varchar(255)
)
```

**SQL Server / Oracle / MS Access**

```sql
CREATE TABLE Persons
(
P_Id int NOT NULL,
LastName varchar(255) NOT NULL,
FirstName varchar(255),
Address varchar(255),
City varchar(255),
CONSTRAINT pk_PersonID PRIMARY KEY (P_Id,LastName)
)
```

#### ALTER TABLE 时的 PRIMARY KEY 约束

```sql
ALTER TABLE Persons ADD CONSTRAINT pk_PersonID PRIMARY KEY (P_Id, LastName);
```

#### 撤销 PRIMARY KEY 约束

**MySQL** 

```sql
ALTER TABLE Persons DROP PRIMARY KEY;
```

**SQL Server / Oracle / MS Access**

```sql
ALTER TABLE Persons DROP CONSTRAINT uc_PersonID
```

---

### FORIGN KEY

一个表中的 FOREIGN KEY 指向另一个表中的 UNIQUE KEY(唯一约束的键)。

FOREIGN KEY 约束用于预防破坏表之间连接的行为。FOREIGN KEY 约束也能防止非法数据插入外键列，因为它必须是它指向的那个表中的值之一。

```sql
-- MySQL
CREATE TABLE Orders
(
O_Id int NOT NULL,
OrderNo int NOT NULL,
P_Id int,
PRIMARY KEY (O_Id),
FOREIGN KEY (P_Id) REFERENCES Persons(P_Id)
)
```

```sql
ALTER TABLE Orders DROP FOREIGN KEY fk_PerOrders
```

> **CREATE TABLE 时 与 ALTER TABLE 时，以及不同数据库及撤销操作，语法与之前的示例相似，不再赘述。** 

### CHECK

CHECK 约束用于限制列中的值的范围。如果对单个列定义 CHECK 约束，那么该列只允许特定的值。如果对一个表定义 CHECK 约束，那么此约束会基于行中其他列的值在特定的列中对值进行限制。

```sql
-- MySQL
CREATE TABLE Persons
(
P_Id int NOT NULL,
LastName varchar(255) NOT NULL,
FirstName varchar(255),
Address varchar(255),
City varchar(255),
CHECK (P_Id>0)
)
```

> **CREATE TABLE 时 与 ALTER TABLE 时，以及不同数据库及撤销操作，语法与之前的示例相似，不再赘述。** 

### DEFAULT

DEFAULT 约束用于向列中插入默认值。如果没有规定其他的值，那么会将默认值添加到所有的新记录。

**My SQL / SQL Server / Oracle / MS Access** 

```sql
-- 插入自定义值
CREATE TABLE Persons
(
    P_Id int NOT NULL,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Address varchar(255),
    City varchar(255) DEFAULT 'Sandnes'
)
-- 插入系统值
CREATE TABLE Orders
(
    O_Id int NOT NULL,
    OrderNo int NOT NULL,
    P_Id int,
    OrderDate date DEFAULT GETDATE()
)
-- 表已创建时修改 MySQL
ALTER TABLE Persons ALTER City SET DEFAULT 'SANDNES'
-- 表已创建时修改 SQL Server / MS Access
ALTER TABLE Persons ADD CONSTRAINT ab_c DEFAULT 'SANDNES' for City
-- 表已创建时修改 Oracle
ALTER TABLE Persons MODIFY City DEFAULT 'SANDNES'
-- 撤销 DEFAULT 约束 MySQL
ALTER TABLE Persons ALTER City DROP DEFAULT
-- 撤销 DEFAULT 约束 SQL Server / Oracle / MS Access
ALTER TABLE Persons ALTER COLUMN City DROP DEFAULT
```

### CREATE INDEX

CREATE INDEX 语句用于在表中创建索引，在不读取整个表的情况下，索引使数据库应用程序可以更快地查找数据。

> **注：** 更新一个包含索引的表需要比更新一个没有索引的表花费更多的时间，这是由于索引本身也需要更新。因此，理想的做法是仅仅在常常被搜索的列（以及表）上面创建索引。

**语法** 

```sql
CREATE INDEX index_name ON table_name (column_name)
```

**CREATE INDEX 实例** 

```sql
CREATE INDEX PIndex ON Persons (LastName, FirstName)
```

### DROP

通过使用 DROP 语句，可以轻松地删除索引、表和数据库。

#### DROP INDEX 语法 

```sql
-- MS Access
DROP INDEX index_name ON table_name
-- MS SQL Server
DROP INDEX table_name.index_name
-- DB2/Oracle
DROP INDEX index_name
-- MySQL
ALTER TABLE table_name DROP INDEX index_name
```

#### DROP TABLE 语法

```sql
DROP TABLE table_name
```

#### DROP DATABASE 语法

```sql
DROP DATABASE database_name
```

#### TRUNCATE TABLE 语法

仅仅删除表内的数据，但并不删除表本身。

```sql
TRUNCATE TABLE table_name
```

### ALTER

ALTER TABLE 语句用于在已有的表中添加、删除或修改列。

**语法** 

```sql
-- 添加列
ALTER TABLE table_name ADD column_name datatype
-- 删除类
ALTER TABLE table_name DROP COLUMN column_name
-- 修改列 SQL Server / MS Access
ALTER TABLE table_name ALTER COLUMN column_name datatype
-- 修改列 My SQL / Oracle
ALTER TABLE table_name MODIFY COLUMN column_name datatype
-- Oracle 10g 之后版本
ALTER TABLE table_name COLUMN column_name datatype
```

### Auto Increment

Auto-increment 会在新记录插入表中时生成一个唯一的数字。

**MySQL 语法** 

```sql
CREATE TABLE Persons
(
ID int NOT NULL AUTO_INCREMENT,
LastName varchar(255) NOT NULL,
FirstName varchar(255),
Address varchar(255),
City varchar(255),
PRIMARY KEY (ID)
)
-- 默认 AUTO_INCREMENT 的开始值是 1，每条新记录递增 1。
```

MySQL 使用 AUTO_INCREMENT 关键字来执行 auto-increment 任务。

```sql
-- 从指定值起始
ALTER TABLE Persons AUTO_INCREMENT=100
```

**SQL Server 语法** 

```sql
CREATE TABLE Persons
(
ID int IDENTITY(1,1) PRIMARY KEY,
LastName varchar(255) NOT NULL,
FirstName varchar(255),
Address varchar(255),
City varchar(255)
)
-- IDENTITY 的开始值是 1，每条新记录递增 1。
```

**Access 语法** 

```sql
CREATE TABLE Persons
(
ID Integer PRIMARY KEY AUTOINCREMENT,
LastName varchar(255) NOT NULL,
FirstName varchar(255),
Address varchar(255),
City varchar(255)
)
-- 默认 AUTO_INCREMENT 的开始值是 1，每条新记录递增 1。
```

**Oracle 语法** 

```sql
CREATE SEQUENCE seq_person
MINVALUE 1
START WITH 1
INCREMENT BY 1
CACHE 10
```

### 视图(Views)

视图是可视化的表。

#### CREATE VIEW

在 SQL 中，视图是基于 SQL 语句的结果集的可视化的表。

视图包含行和列，就像一个真实的表。视图中的字段就是来自一个或多个数据库中的真实的表中的字段。

您可以向视图添加 SQL 函数、WHERE 以及 JOIN 语句，也可以呈现数据，就像这些数据来自于某个单一的表一样。

**语法** 

```sql
CREATE VIEW view_name AS
SELECT column_name(s)
FROM table_name
WHERE condition
```

> **注：** 视图总是显示最新的数据！每当用户查询视图时，数据库引擎通过使用视图的 SQL 语句重建数据。

**CREATE VIEW 实例** 

```sql
CREATE VIEW [Current Product List] AS
SELECT ProductID,ProductName
FROM Products
WHERE Discontinued=No
```

#### SQL 更新视图

**语法** 

```sql
CREATE OR REPLACE VIEW view_name AS
SELECT column_name(s)
FROM table_name
WHERE condition
```

#### SQL 撤销视图

**语法** 

```sql
DROP VIEW view_name
```

### 日期

#### MySQL Date 函数

| 函数          | 描述                                |
| :------------ | :---------------------------------- |
| NOW()         | 返回当前的日期和时间                |
| CURDATE()     | 返回当前的日期                      |
| CURTIME()     | 返回当前的时间                      |
| DATE()        | 提取日期或日期/时间表达式的日期部分 |
| EXTRACT()     | 返回日期/时间的单独部分             |
| DATE_ADD()    | 向日期添加指定的时间间隔            |
| DATE_SUB()    | 从日期减去指定的时间间隔            |
| DATEDIFF()    | 返回两个日期之间的天数              |
| DATE_FORMAT() | 用不同的格式显示日期/时间           |

#### MySQL Date 函数

| 函数       | 描述                             |
| :--------- | :------------------------------- |
| GETDATE()  | 返回当前的日期和时间             |
| DATEPART() | 返回日期/时间的单独部分          |
| DATEADD()  | 在日期中添加或减去指定的时间间隔 |
| DATEDIFF() | 返回两个日期之间的时间           |
| CONVERT()  | 用不同的格式显示日期/时间        |

#### SQL Date 数据类型

**MySQL** 使用下列数据类型在数据库中存储日期或日期/时间值：

- DATE - 格式：YYYY-MM-DD
- DATETIME - 格式：YYYY-MM-DD HH:MM:SS
- TIMESTAMP - 格式：YYYY-MM-DD HH:MM:SS
- YEAR - 格式：YYYY 或 YY

**SQL Server** 使用下列数据类型在数据库中存储日期或日期/时间值：

- DATE - 格式：YYYY-MM-DD
- DATETIME - 格式：YYYY-MM-DD HH:MM:SS
- SMALLDATETIME - 格式：YYYY-MM-DD HH:MM:SS
- TIMESTAMP - 格式：唯一的数字

#### SQL 日期处理

略

### NULL 值

如果表中的某个列是可选的，那么我们可以在不向该列添加值的情况下插入新记录或更新已有的记录。这意味着该字段将以 NULL 值保存。

NULL 用作未知的或不适用的值的占位符。

对于 NULL 值的处理，必须使用 IS NULL 和 IS NOT NULL 操作符。

**SQL IS NULL 实例** 

```sql
SELECT LastName,FirstName,Address FROM Persons WHERE Address IS NULL
```

**SQL IS NOT NULL 实例** 

```sql
SELECT LastName,FirstName,Address FROM Persons WHERE Address IS NOT NULL
```

### NULL 函数

**ISNULL()、NVL()、IFNULL() 和 COALESCE()** 

微软的 ISNULL() 函数用于规定如何处理 NULL 值。NVL()、IFNULL() 和 COALESCE() 函数也可以达到相同的结果。

```sql
-- SQL Server / MS Access
SELECT ProductName, UnitPrice * (UnitsInStock + ISNULL(UnitsOnOrder, 0)) FROM Products;
-- Oracle 没有 ISNULL() 函数。不过，我们可以使用 NVL() 函数达到相同的结果：
SELECT ProductName, UnitPrice * (UnitsInStock + NVL(UnitsOnOrder, 0)) FROM Products;
-- MySQL 也拥有类似 ISNULL() 的函数。不过它的工作方式与微软的 ISNULL() 函数有点不同。
-- 在 MySQL 中，我们可以使用 IFNULL() 函数
SELECT ProductName, UnitPrice * (UnitsInStock + IFNULL(UnitsOnOrder, 0)) FROM Products;
-- 或者使用 COALESCE() 函数
SELECT ProductName, UnitPrice * (UnitsInStock + IFNULL(UnitsOnOrder, 0)) FROM Products;
```

### 通用数据类型

**SQL 通用数据类型** 

| 数据类型                           | 描述                                                         |
| :--------------------------------- | :----------------------------------------------------------- |
| CHARACTER(n)                       | 字符/字符串。固定长度 n。                                    |
| VARCHAR(n) 或 CHARACTER VARYING(n) | 字符/字符串。可变长度。最大长度 n。                          |
| BINARY(n)                          | 二进制串。固定长度 n。                                       |
| BOOLEAN                            | 存储 TRUE 或 FALSE 值                                        |
| VARBINARY(n) 或 BINARY VARYING(n)  | 二进制串。可变长度。最大长度 n。                             |
| INTEGER(p)                         | 整数值（没有小数点）。精度 p。                               |
| SMALLINT                           | 整数值（没有小数点）。精度 5。                               |
| INTEGER                            | 整数值（没有小数点）。精度 10。                              |
| BIGINT                             | 整数值（没有小数点）。精度 19。                              |
| DECIMAL(p,s)                       | 精确数值，精度 p，小数点后位数 s。例如：decimal(5,2) 是一个小数点前有 3 位数，小数点后有 2 位数的数字。 |
| NUMERIC(p,s)                       | 精确数值，精度 p，小数点后位数 s。（与 DECIMAL 相同）        |
| FLOAT(p)                           | 近似数值，尾数精度 p。一个采用以 10 为基数的指数计数法的浮点数。该类型的 size 参数由一个指定最小精度的单一数字组成。 |
| REAL                               | 近似数值，尾数精度 7。                                       |
| FLOAT                              | 近似数值，尾数精度 16。                                      |
| DOUBLE PRECISION                   | 近似数值，尾数精度 16。                                      |
| DATE                               | 存储年、月、日的值。                                         |
| TIME                               | 存储小时、分、秒的值。                                       |
| TIMESTAMP                          | 存储年、月、日、小时、分、秒的值。                           |
| INTERVAL                           | 由一些整数字段组成，代表一段时间，取决于区间的类型。         |
| ARRAY                              | 元素的固定长度的有序集合                                     |
| MULTISET                           | 元素的可变长度的无序集合                                     |
| XML                                | 存储 XML 数据                                                |

**不同数据库平台上一些数据类型的通用名称** 

| 数据类型            | Access                  | SQLServer                                            | Oracle           | MySQL       | PostgreSQL       |
| :------------------ | :---------------------- | :--------------------------------------------------- | :--------------- | :---------- | :--------------- |
| *boolean*           | Yes/No                  | Bit                                                  | Byte             | N/A         | Boolean          |
| *integer*           | Number (integer)        | Int                                                  | Number           | Int Integer | Int Integer      |
| *float*             | Number (single)         | Float Real                                           | Number           | Float       | Numeric          |
| *currency*          | Currency                | Money                                                | N/A              | N/A         | Money            |
| *string (fixed)*    | N/A                     | Char                                                 | Char             | Char        | Char             |
| *string (variable)* | Text (<256) Memo (65k+) | Varchar                                              | Varchar Varchar2 | Varchar     | Varchar          |
| *binary object*     | OLE Object Memo         | Binary (fixed up to 8K) Varbinary (<8K) Image (<2GB) | Long Raw         | Blob Text   | Binary Varbinary |

> **注：** 在不同的数据库中，同一种数据类型可能有不同的名称，即使名称相同，尺寸和其他细节也可能不同！

### DB数据类型

**Microsoft Access** 

| 数据类型      | 描述                                                         | 存储     |
| :------------ | :----------------------------------------------------------- | :------- |
| Text          | 用于文本或文本与数字的组合。最多 255 个字符。                |          |
| Memo          | Memo 用于更大数量的文本。最多存储 65,536 个字符。**注：** 无法对 memo 字段进行排序。不过它们是可搜索的。 |          |
| Byte          | 允许 0 到 255 的数字。                                       | 1 字节   |
| Integer       | 允许介于 -32,768 与 32,767 之间的全部数字。                  | 2 字节   |
| Long          | 允许介于 -2,147,483,648 与 2,147,483,647 之间的全部数字。    | 4 字节   |
| Single        | 单精度浮点。处理大多数小数。                                 | 4 字节   |
| Double        | 双精度浮点。处理大多数小数。                                 | 8 字节   |
| Currency      | 用于货币。支持 15 位的元，外加 4 位小数。**提示：**您可以选择使用哪个国家的货币。 | 8 字节   |
| AutoNumber    | AutoNumber 字段自动为每条记录分配数字，通常从 1 开始。       | 4 字节   |
| Date/Time     | 用于日期和时间                                               | 8 字节   |
| Yes/No        | 逻辑字段，可以显示为 Yes/No、True/False 或 On/Off。在代码中，使用常量 True 和 False （等价于 1 和 0）。**注释：**Yes/No 字段中不允许 Null 值 | 1 比特   |
| Ole Object    | 可以存储图片、音频、视频或其他 BLOBs（Binary Large OBjects）。 | 最多 1GB |
| Hyperlink     | 包含指向其他文件的链接，包括网页。                           |          |
| Lookup Wizard | 允许您创建一个可从下拉列表中进行选择的选项列表。             | 4 字节   |

**MySQL** 

| 数据类型         | 描述                                                         |
| :--------------- | :----------------------------------------------------------- |
| CHAR(size)       | 保存固定长度的字符串（可包含字母、数字以及特殊字符）。在括号中指定字符串的长度。最多 255 个字符。 |
| VARCHAR(size)    | 保存可变长度的字符串（可包含字母、数字以及特殊字符）。在括号中指定字符串的最大长度。最多 255 个字符。**注释：**如果值的长度大于 255，则被转换为 TEXT 类型。 |
| TINYTEXT         | 存放最大长度为 255 个字符的字符串。                          |
| TEXT             | 存放最大长度为 65,535 个字符的字符串。                       |
| BLOB             | 用于 BLOBs（Binary Large OBjects）。存放最多 65,535 字节的数据。 |
| MEDIUMTEXT       | 存放最大长度为 16,777,215 个字符的字符串。                   |
| MEDIUMBLOB       | 用于 BLOBs（Binary Large OBjects）。存放最多 16,777,215 字节的数据。 |
| LONGTEXT         | 存放最大长度为 4,294,967,295 个字符的字符串。                |
| LONGBLOB         | 用于 BLOBs (Binary Large OBjects)。存放最多 4,294,967,295 字节的数据。 |
| ENUM(x,y,z,etc.) | 允许您输入可能值的列表。可以在 ENUM 列表中列出最大 65535 个值。如果列表中不存在插入的值，则插入空值。**注：** 这些值是按照您输入的顺序排序的。可以按照此格式输入可能的值： ENUM('X','Y','Z') |
| SET              | 与 ENUM 类似，不同的是，SET 最多只能包含 64 个列表项且 SET 可存储一个以上的选择。 |
| TINYINT(size)    | 带符号-128到127 ，无符号0到255。                             |
| SMALLINT(size)   | 带符号范围-32768到32767，无符号0到65535, size 默认为 6。     |
| MEDIUMINT(size)  | 带符号范围-8388608到8388607，无符号的范围是0到16777215。 size 默认为9 |
| INT(size)        | 带符号范围-2147483648到2147483647，无符号的范围是0到4294967295。 size 默认为 11 |
| BIGINT(size)     | 带符号的范围是-9223372036854775808到9223372036854775807，无符号的范围是0到18446744073709551615。size 默认为 20 |
| FLOAT(size,d)    | 带有浮动小数点的小数字。在 size 参数中规定显示最大位数。在 d 参数中规定小数点右侧的最大位数。 |
| DOUBLE(size,d)   | 带有浮动小数点的大数字。在 size 参数中规显示定最大位数。在 d 参数中规定小数点右侧的最大位数。 |
| DECIMAL(size,d)  | 作为字符串存储的 DOUBLE 类型，允许固定的小数点。在 size 参数中规定显示最大位数。在 d 参数中规定小数点右侧的最大位数。 |
| DATE()           | 日期。格式：YYYY-MM-DD **注：** 支持的范围是从 '1000-01-01' 到 '9999-12-31' |
| DATETIME()       | 日期和时间的组合。格式：YYYY-MM-DD HH:MM:SS **注：** 支持的范围是从 '1000-01-01 00:00:00' 到 '9999-12-31 23:59:59' |
| TIMESTAMP()      | 时间戳。TIMESTAMP 值使用 Unix 纪元('1970-01-01 00:00:00' UTC) 至今的秒数来存储。格式：YYYY-MM-DD HH:MM:SS **注：** 支持的范围是从 '1970-01-01 00:00:01' UTC 到 '2038-01-09 03:14:07' UTC |
| TIME()           | 时间。格式：HH:MM:SS **注：** 支持的范围是从 '-838:59:59' 到 '838:59:59' |
| YEAR()           | 2 位或 4 位格式的年。 **注：** 4 位格式所允许的值：1901 到 2155。2 位格式所允许的值：70 到 69，表示从 1970 到 2069。 |

**SQL Server** 

| 数据类型         | 描述                                                         | 存储                      |
| :--------------- | :----------------------------------------------------------- | :------------------------ |
| char(n)          | 固定长度的字符串。最多 8,000 个字符。                        | Defined width             |
| varchar(n)       | 可变长度的字符串。最多 8,000 个字符。                        | 2 bytes + number of chars |
| varchar(max)     | 可变长度的字符串。最多 1,073,741,824 个字符。                | 2 bytes + number of chars |
| text             | 可变长度的字符串。最多 2GB 文本数据。                        | 4 bytes + number of chars |
| nchar            | 固定长度的 Unicode 字符串。最多 4,000 个字符。               | Defined width x 2         |
| nvarchar         | 可变长度的 Unicode 字符串。最多 4,000 个字符。               |                           |
| nvarchar(max)    | 可变长度的 Unicode 字符串。最多 536,870,912 个字符。         |                           |
| ntext            | 可变长度的 Unicode 字符串。最多 2GB 文本数据。               |                           |
| bit              | 允许 0、1 或 NULL                                            |                           |
| binary(n)        | 固定长度的二进制字符串。最多 8,000 字节。                    |                           |
| varbinary        | 可变长度的二进制字符串。最多 8,000 字节。                    |                           |
| varbinary(max)   | 可变长度的二进制字符串。最多 2GB。                           |                           |
| image            | 可变长度的二进制字符串。最多 2GB。                           |                           |
| tinyint          | 允许从 0 到 255 的所有数字。                                 | 1 字节                    |
| smallint         | 允许介于 -32,768 与 32,767 的所有数字。                      | 2 字节                    |
| int              | 允许介于 -2,147,483,648 与 2,147,483,647 的所有数字。        | 4 字节                    |
| bigint           | 允许介于 -9,223,372,036,854,775,808 与 9,223,372,036,854,775,807 之间的所有数字。 | 8 字节                    |
| decimal(p,s)     | 固定精度和比例的数字。允许从 -10^38 +1 到 10^38 -1 之间的数字。p 参数指示可以存储的最大位数（小数点左侧和右侧）。p 必须是 1 到 38 之间的值。默认是 18。s 参数指示小数点右侧存储的最大位数。s 必须是 0 到 p 之间的值。默认是 0。 | 5-17 字节                 |
| numeric(p,s)     | 固定精度和比例的数字。允许从 -10^38 +1 到 10^38 -1 之间的数字。p 参数指示可以存储的最大位数（小数点左侧和右侧）。p 必须是 1 到 38 之间的值。默认是 18。s 参数指示小数点右侧存储的最大位数。s 必须是 0 到 p 之间的值。默认是 0。 | 5-17 字节                 |
| smallmoney       | 介于 -214,748.3648 与 214,748.3647 之间的货币数据。          | 4 字节                    |
| money            | 介于 -922,337,203,685,477.5808 与 922,337,203,685,477.5807 之间的货币数据。 | 8 字节                    |
| float(n)         | 从 -1.79E + 308 到 1.79E + 308 的浮动精度数字数据。n 参数指示该字段保存 4 字节还是 8 字节。float(24) 保存 4 字节，而 float(53) 保存 8 字节。n 的默认值是 53。 | 4 或 8 字节               |
| real             | 从 -3.40E + 38 到 3.40E + 38 的浮动精度数字数据。            | 4 字节                    |
| datetime         | 从 1753 年 1 月 1 日 到 9999 年 12 月 31 日，精度为 3.33 毫秒。 | 8 字节                    |
| datetime2        | 从 1753 年 1 月 1 日 到 9999 年 12 月 31 日，精度为 100 纳秒。 | 6-8 字节                  |
| smalldatetime    | 从 1900 年 1 月 1 日 到 2079 年 6 月 6 日，精度为 1 分钟。   | 4 字节                    |
| date             | 仅存储日期。从 0001 年 1 月 1 日 到 9999 年 12 月 31 日。    | 3 bytes                   |
| time             | 仅存储时间。精度为 100 纳秒。                                | 3-5 字节                  |
| datetimeoffset   | 与 datetime2 相同，外加时区偏移。                            | 8-10 字节                 |
| timestamp        | 存储唯一的数字，每当创建或修改某行时，该数字会更新。timestamp 值基于内部时钟，不对应真实时间。每个表只能有一个 timestamp 变量。 |                           |
| sql_variant      | 存储最多 8,000 字节不同数据类型的数据，除了 text、ntext 以及 timestamp。 |                           |
| uniqueidentifier | 存储全局唯一标识符 (GUID)。                                  |                           |
| xml              | 存储 XML 格式化数据。最多 2GB。                              |                           |
| cursor           | 存储对用于数据库操作的指针的引用。                           |                           |
| table            | 存储结果集，供稍后处理。                                     |                           |

### SQL 函数（聚合函数 & 标量函数）

**Aggregate 函数（聚合函数）** 

SQL Aggregate 函数计算从列中取得的值，返回一个单一的值。

- AVG() - 返回平均值
- COUNT() - 返回行数
- FIRST() - 返回第一个记录的值
- LAST() - 返回最后一个记录的值
- MAX() - 返回最大值
- MIN() - 返回最小值
- SUM() - 返回总和

**Scalar 函数（标量函数）** 

SQL Scalar 函数基于输入值，返回一个单一的值。

- UCASE() - 将某个字段转换为大写
- LCASE() - 将某个字段转换为小写
- MID() - 从某个文本字段提取字符，MySql 中使用
- SubString(字段，1，end) - 从某个文本字段提取字符
- LEN() - 返回某个文本字段的长度
- ROUND() - 对某个数值字段进行指定小数位数的四舍五入
- NOW() - 返回当前的系统日期和时间
- FORMAT() - 格式化某个字段的显示方式

### AVG()

AVG() 函数返回数值列的平均值。

**语法** 

```sql
SELECT AVG(column_name) FROM table_name;
```

**实例** 

```sql
SELECT site_id, count FROM access_log WHERE count > (SELECT AVG(count) FROM access_log);
```

### COUNT()

COUNT() 函数返回匹配指定条件的行数。

**语法** 

```sql
-- COUNT(column_name) 函数返回指定列的值的数目（NULL 不计入）：
SELECT COUNT(column_name) FROM table_name;
-- COUNT(*) 函数返回表中的记录数：
SELECT COUNT(*) FROM table_name;
-- COUNT(DISTINCT column_name) 函数返回指定列的不同值的数目：
SELECT COUNT(DISTINCT column_name) FROM table_name;
```

> **注：** COUNT(DISTINCT) 适用于 ORACLE 和 Microsoft SQL Server，但是无法用于 Microsoft Access。

**实例** 

略。

### FIRST()

FIRST() 函数返回指定的列中第一个记录的值。

**语法** 

```sql
SELECT FIRST(column_name) FROM table_name;
```

**SQL Server、MySQL 和 Oracle 中的 SQL FIRST() 工作区** 

```sql
-- SQL Server 语法
SELECT TOP 1 column_name FROM table_name ORDER BY column_name ASC;
-- MySQL 语法
SELECT column_name FROM table_name ORDER BY column_name ASC LIMIT 1;
-- Oracle 语法
SELECT column_name FROM table_name ORDER BY column_name ASC WHERE ROWNUM <= 1;
```

### LAST()

LAST() 函数返回指定的列中最后一个记录的值。

**语法** 

```sql
SELECT LAST(column_name) FROM table_name;
```

**SQL Server、MySQL 和 Oracle 中的 SQL LAST() 工作区** 

```sql
-- SQL Server 语法
SELECT TOP 1 column_name FROM table_name ORDER BY column_name DESC;
-- MySQL 语法
SELECT column_name FROM table_name ORDER BY column_name DESC LIMIT 1;
-- Oracle 语法
SELECT column_name FROM table_name ORDER BY column_name DESC WHERE ROWNUM <= 1;
```

### MAX() & MIN()

MAX() 函数返回指定列的最大值。

MIN() 函数返回指定列的最小值。

**语法** 

```sql
SELECT MAX(column_name) FROM table_name;
SELECT MIN(column_name) FROM table_name;
```

**实例** 

```sql
SELECT MAX(age) AS max_age FROM user;
SELECT MIN(age) AS max_age FROM user;
```

### SUM()

SUM() 函数返回数值列的总数。

**语法** 

```sql
SELECT SUM(column_name) FROM table_name;
```

**实例** 

```sql
SELECT SUM(count) AS nums FROM access_log;
```

### GROUP BY

GROUP BY 语句用于结合聚合函数，根据一个或多个列对结果集进行分组。

**语法** 

```sql
SELECT column_name, aggregate_function(column_name)
FROM table_name
WHERE column_name operator value
GROUP BY column_name;
```

**实例** 

```sql
-- 单表
SELECT site_id, SUM(access_log.count) AS nums FROM access_log GROUP BY site_id;
-- 多表
SELECT Websites.name, COUNT(access_log.aid) AS nums FROM access_log
LEFT JOIN Websites
ON access_log.site_id = Websites.id
GROUP BY Websites.name;
```

---

### HAVING

在 SQL 中增加 HAVING 子句原因是，WHERE 关键字无法与聚合函数一起使用。HAVING 子句可以让我们筛选分组后的各组数据。

**语法** 

```sql
SELECT column_name, aggregate_function(column_name)
FROM table_name
WHERE column_name operator value
GROUP BY column_name
HAVING aggregate_function(column_name) operator value;
```

**实例** 

```sql
SELECT Websites.name, SUM(access_log.count) AS nums FROM Websites
INNER JOIN access_log
ON Websites.id = access_log.site_id
WHERE Websites.alexa < 200 
GROUP BY Websites.name
HAVING SUM(access_log.count) > 200;
```

### EXISTS

EXISTS 运算符用于判断查询子句是否有记录，如果有一条或多条记录存在返回 True，否则返回 False。

**语法** 

```sql
SELECT column_name(s)
FROM table_name
WHERE EXISTS
(SELECT column_name FROM table_name WHERE condition);
```

**实例** 

```sql
-- EXISTS
SELECT Websites.name, Websites.url 
FROM Websites 
WHERE EXISTS (SELECT count FROM access_log WHERE Websites.id = access_log.site_id AND count > 200);
-- NOT EXISTS
SELECT Websites.name, Websites.url 
FROM Websites 
WHERE NOT EXISTS (SELECT count FROM access_log WHERE Websites.id = access_log.site_id AND count > 200);
```

### UCASE() & LCASE()

UCASE() 函数把字段的值转换为大写。

LCASE() 函数把字段的值转换为小写。

**语法** 

```sql
-- UCASE()
SELECT UCASE(column_name) FROM table_name;
-- LCASE()
SELECT LCASE(column_name) FROM table_name;
```

**用于 SQL Server 的语法** 

```sql
-- UCASE()
SELECT UPPER(column_name) FROM table_name;
-- LCASE()
SELECT LOWER(column_name) FROM table_name;
```

### MID()

MID() 函数用于从文本字段中提取字符。相当于截取字符串。

**语法** 

```sql
-- start：开始位置（最小为1），length：可选
SELECT MID(column_name,start[,length]) FROM table_name;
```

**实例** 

```sql
-- 获取部门名称前两个字
SELECT MID(dept_name, 1, 2) AS ShortTitle FROM sys_dept;
```

---

### LEN()

LEN() 函数返回文本字段中值的长度。

**语法** 

```sql
SELECT LEN(column_name) FROM table_name;
```

**MySQL 语法** 

```sql
SELECT LENGTH(column_name) FROM table_name;
```

### ROUND()

ROUND() 函数用于把数值字段舍入为指定的小数位数。

**语法** 

```sql
-- decimals：规定要返回的小数位数。
SELECT ROUND(column_name,decimals) FROM table_name;
```

**实例** 

```sql
-- ROUND(X)： 返回参数X的四舍五入的一个整数。
select ROUND(-1.23);-- -1
-- ROUND(X,D)： 返回参数X的四舍五入的有 D 位小数的一个数字。如果D为0，结果将没有小数点或小数部分。
select ROUND(1.298, 1);-- 1.3
```

### NOW()

NOW() 函数返回当前系统的日期和时间。

**语法** 

```sql
SELECT NOW() FROM table_name;
```

> select now() from dual; -- 此时from dual可省略

### FORMAT()

FORMAT() 函数用于对字段的显示进行格式化。

**语法** 

```sql
-- format：规定格式
SELECT FORMAT(column_name, format) FROM table_name;
```

**实例** 

```sql
SELECT name, url, DATE_FORMAT(Now(),'%Y-%m-%d') AS date FROM Websites;
```

### SQL 主机

**不同操作系统上对应运行的数据库系统** 

**MS SQL Server** 

在 Windows 和 Linux 操作系统上运行。

**MySQL** 

在 Windows, Mac OS X 和 Linux/UNIX 操作系统上运行。

**MS Access**（只建议用于小型网站）

只在 Windows OS 上运行。

### 参考

* https://www.runoob.com/sql/sql-tutorial.html