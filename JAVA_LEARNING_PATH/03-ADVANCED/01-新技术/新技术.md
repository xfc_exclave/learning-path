---
layout: draft
title: 草稿
date: 2021-08-03 17:22:15
updated: 2021-08-03 17:22:30
---

## 新技术

### Java 8

#### lambda表达式

#### Stream API

#### 时间API

---

### Java 9

#### Jigsaw

#### Jshell

#### Reactive Streams

---

### Java 10

#### 局部变量类型推断

#### G1的并行Full GC

#### ThreadLocal握手机制

---

### Java 11

#### ZGC

#### Epsilon

#### 增强var

---

### Spring 5

#### 响应式编程

---

### Spring Boot 2.0

---

### HTTP/2

---

### HTTP/3