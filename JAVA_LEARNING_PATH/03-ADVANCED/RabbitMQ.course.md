---
title: RabbitMQ 教程
categories: [消息队列]
tags: [java, RabbitMQ]
top_img: false
---

### 安装教程

#### 1. Windows下安装

* 环境

  在安装RabbitMQ之前，需要配置Erlang环境。

  Erlang 下载：

  * https://www.erlang.org/downloads （慢）
  * https://packages.erlang-solutions.com/erlang/ （快）

  Erlang 安装：

  1. 双击执行安装文件。
  2. 选择安装位置（基本上都是下一步）。
  3. 安装完成。

* 下载地址

  * https://www.rabbitmq.com/download.html （下载选择页）
  * https://www.rabbitmq.com/changelog.html （快捷下载页）

* 安装

  1. 双击执行安装文件。
  2. 选择安装位置（基本上都是下一步）。
  3. 安装完成。

  > 安装完成后， `开始` 中可以看到 `RabbitMQ Service -start` 、 `RabbitMQ Service -stop` 、 `RabbitMQ Service -remove` 三个快捷启动方式（如果没有，可以通过 `win + S` 搜索 `RabbitMQ Service` ）。

* 配置

  * 启动插件

    切换到 `[rabbitmq]\rabbitmq_server-3.8.1\sbin>` 目录。

    执行： `rabbitmq-plugins enable rabbitmq_management` （执行一次即可）。

* 启动

  执行 `RabbitMQ Service -start` （需要启动服务时执行）。

* 访问

  浏览器访问：http://localhost:15672/

* 默认配置

  * 用户名：guest
  * 密码：guset
  * 后台端口：15672
  * TCP端口：5672

### 二、AMQP知识

AMQP，即Advanced Message Queuing Protocol，一个提供统一消息服务的应用层标准高级消息队列协议。

AMQP是应用层协议的一个开放标准，为面向消息的中间件设计。基于此协议的客户端与消息中间件可传递消息，并不受客户端/中间件不同产品，不同的开发语言等条件的限制。目标是实现一种在全行业广泛使用的标准消息中间件技术，以便降低企业和系统集成的开销，并且向大众提供工业级的集成服务。主要实现有 RabbitMQ。

#### 1. 概念

* **生产者**，即消息的创建者，它将消息发送到RabbitMQ。
* **消费者**，连接到RabbitMQ，订阅到队列上，对消息进行消费，其订阅类型可分为：持续订阅（basicConsumer）和单条订阅（basicGet）。
* **消息**，它包含有效载荷和标签，有效载荷指要传输的数据，标签描述了有效载荷，并且RabbitMQ用它来决定谁获得消息，消费者只能拿到有效载荷，并不知道生产者是谁。
* **信道**，信道是生产消费者与RabbitMQ通信的渠道，生产者发布或是消费者订阅一个队列都是通过信道来通信的。信道是建立在TCP连接上的虚拟连接。RabbitMQ在一条TCP上建立成百上千个信道来达到多个线程处理，这个TCP被多个线程共享，每个线程对应一个信道，信道在rabbit都有唯一的ID ，保证了信道私有性，对应上唯一的线程使用。
* **交换器**，在RabbitMQ中，主要有四种交换器类型：Direct，Fanout，Topic，Head。
* **队列**，
* **绑定**，
* **路由键**，

队列通过路由键（routing key，某种确定的规则）绑定到交换器，生产者将消息发布到交换器，交换器根据绑定的路由键将消息路由到特定队列，然后由订阅这个队列的消费者进行接收。

![image-20201116085253769](https://www.xfc-exclave.com/upload/2020/11/image-20201116085253769-45537ab10d0f4084a68eb2913e6c8fcf.png)

#### 2. 消息的确认

消费者收到的每一条消息都必须进行确认（自动确认和自行确认）。

消费者在声明队列时，可以指定autoAck参数，当autoAck=false时，RabbitMQ会等待消费者显式发回ack信号后才从内存(和磁盘，如果是持久化消息的话)中移去消息。否则，在RabbitMQ队列中消息被消费后会立即删除它。

采用消息确认机制后，只要令autoAck=false，消费者就有足够的时间处理消息(任务)，不用担心处理消息过程中消费者进程挂掉后消息丢失的问题，因为RabbitMQ会一直持有消息直到消费者显式调用basicAck为止。

当autoAck=false时，对于RabbitMQ服务器端而言，队列中的消息分成了两部分：一部分是等待投递给消费者的消息；一部分是已经投递给消费者，但是还没有收到消费者ack信号的消息。如果服务器端一直没有收到消费者的ack信号，并且消费此消息的消费者已经断开连接，则服务器端会安排该消息重新进入队列，等待投递给下一个消费者（也可能还是原来的那个消费者）。

RabbitMQ不会为未ack的消息设置超时时间，它判断此消息是否需要重新投递给消费者的唯一依据是消费该消息的消费者连接是否已经断开。这么设计的原因是RabbitMQ允许消费者消费一条消息的时间可以很久很久。

#### 3. 交换器类型

* **Direct** 

  direct为默认的交换器类型，也非常的简单，如果路由键匹配的话，消息就投递到相应的队列。

  使用代码： `channel.basicPublish("", QueueName, null, message)` 推送direct交换器消息到对于的队列，空字符为默认的direct交换器，用队列名称当做路由键。

  ![exchange_direct](https://www.xfc-exclave.com/upload/2020/12/exchange_direck-ff13c80e071c4a4faf1a140ba36dc9c9.png)

* **Fanout** （发布/订阅模式）

  fanout有别于direct交换器，fanout是一种发布/订阅模式的交换器，当你发送一条消息的时候，交换器会把消息广播到所有附加到这个交换器的队列上。

  和direct交换器不同，我们在发送消息的时候新增 `channel.exchangeDeclare(ExchangeName, "fanout")` ，这行代码声明fanout交换器。

  ![exchange_fanout](https://www.xfc-exclave.com/upload/2020/12/exchange_fanout-62774b87b2c448ad9e7bb81c145fc66a.png)

* **Topic** （匹配订阅模式）

  topic交换器运行和fanout类似，但是可以更灵活的匹配自己想要订阅的信息，它使用路由键进行消息（规则）匹配。

  通过使用 “\*” 和 “#” ，使来自不同源头的消息到达同一个队列， ”.” 将路由键分为了几个标识符， “*” 匹配1个， “#” 匹配一个或多个。

  例如： `channel.queueBind(queueName, ExchangeName, "#.error")` 。

  ![exchange_topic](https://www.xfc-exclave.com/upload/2020/12/exchange_topic-c6b498c816a447e59104c41ee45c2dff.png)

* **headers** 

  几乎和direct一样，不实用，可以忽略。

#### 4. 虚拟主机

虚拟消息服务器，vhost，本质上就是一个迷你版的消息队列服务器，有自己的队列、交换器和绑定，最重要的是它有自己的权限机制。Vhost提供了逻辑上的分离，可以将众多客户端进行区分，又可以避免队列和交换器的命名冲突。Vhost必须在连接时指定，rabbitmq包含缺省vhost：“/”，通过缺省用户和口令guest进行访问。

虚拟主机保证了用户可以在多个不同的application中使用RabbitMQ。

在RabbitMQ中创建用户，必须要被指派给至少一个vhost，并且只能访问被指派内的队列、交换器和绑定。Vhost必须通过rabbitmq的管理控制工具创建。

### 三、原生RabbitMQ

#### 1. 准备

* 启动RabbitMQ消息服务。
* 创建maven项目及添加基础依赖。

#### 2. 依赖

```xml
<dependency>
    <groupId>com.rabbitmq</groupId>
    <artifactId>amqp-client</artifactId>
    <!--version-->
</dependency>
```

#### 3. 代码

**生产者端：** 

```java
package com.xfc.exchange.direct;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.util.Arrays;
import java.util.List;

public class DirectProducer {

    public final static String EXCHANGE_NAME = "direct_exchange";

    public static void main(String[] args) throws Exception {
        // 1.创建连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();// 默认连接到localhost
        // 2.创建连接
        Connection connection = connectionFactory.newConnection();
        // 3.创建信道
        Channel channel = connection.createChannel();
        // 4.设置交换器
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
        // 自定义路由键
        List<String> routeKey = Arrays.asList("key1", "key2", "key3");
        // 循环发送消息
        for (int i = 0; i < routeKey.size(); i++) {
            String key = routeKey.get(i % routeKey.size());
            String msg = "Hello, RabbitMq " + (i + 1);
            // 5.发送消息（交换器，路由键，属性参数，消息字节）
            channel.basicPublish(EXCHANGE_NAME, key, null, msg.getBytes());
            System.out.println("Sent " + key + ":" + msg);
        }
        channel.close();
        connection.close();
    }
}
```

**消费者端：** 

```java
package com.xfc.exchange.direct;

import com.rabbitmq.client.*;

import java.io.IOException;

public class DirectConsumer {

    public final static String QUEUE_NAME = "queue_name";

    public static void main(String[] args) throws Exception {
        // 1.创建连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();// 默认连接到localhost
        // 2.创建连接
        Connection connection = connectionFactory.newConnection();
        // 3.创建信道
        Channel channel = connection.createChannel();
        // 4.设置交换器
        channel.exchangeDeclare(DirectProducer.EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
        // 4.声明队列
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        // 5.交换器和队列绑定：将队列和交换器通过路由键进行绑定
        channel.queueBind(QUEUE_NAME, DirectProducer.EXCHANGE_NAME, "key2");

        System.out.println("waiting for message........");

        // 6.声明消费者
        final Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                System.out.println("Received[" + envelope.getRoutingKey() + "] " + message);
            }
        };
        /*消费者正式开始在指定队列上消费消息*/
        channel.basicConsume(QUEUE_NAME, true, consumer);
    }
}
```

> 注：其他交换器类型的使用方式与 direct 基本一致，可以参照 **AMQP知识** >> **交换机类型** 相关内容进行尝试。

#### 4. 运行

1. 运行消费者端代码。

2. 运行生产者端代码。

3. 观察控制台打印语句及RabbitMQ管理界面。

   生产者控制台：

   ![image-20201116102344699](https://www.xfc-exclave.com/upload/2020/11/image-20201116102344699-e5c805d499fa43bb89a899b1db3ef712.png)

   消费者控制台：

   ![image-20201116102251702](https://www.xfc-exclave.com/upload/2020/11/image-20201116102251702-f24a780cf7794311857ec823020db668.png)

   RabbitMQ管理界面：

   ![image-20201116102846194](https://www.xfc-exclave.com/upload/2020/11/image-20201116102846194-27ddad23c85548e19e9cf42dac213e74.png)

### 四、扩展

#### 1. 消息发布时的权衡

RabbitMQ在设计的时候，特意让生产者和消费者“脱钩”，也就是消息的发布和消息的消费之间是解耦的。

在 RabbitMQ 中，有不同的投递机制（生产者），但是每一种机制都对性能有一定的影响。一般来讲速度快的可靠性低，可靠性好的性能差，具体怎么使用需要根据你的应用程序来定，所以说没有最好的方式，只有最合适的方式。只有把你的项目和技术相结合，才能找到适合你的平衡。



### 五、Spring整合RabbitMQ

#### 1. 准备

* 启动RabbitMQ消息服务。

* 创建maven项目。

* 基础依赖。

  ```xml
  <!-- RabbitMQ核心依赖 -->
  <dependency>
      <groupId>org.springframework.amqp</groupId>
      <artifactId>spring-rabbit</artifactId>
      <version>2.2.12.RELEASE</version>
  </dependency>
  <!-- Spring相关依赖 -->
  <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-core</artifactId>
      <version>${spring.version}</version>
  </dependency>
  
  <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-web</artifactId>
      <version>${spring.version}</version>
  </dependency>
  <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-oxm</artifactId>
      <version>${spring.version}</version>
  </dependency>
  <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-tx</artifactId>
      <version>${spring.version}</version>
  </dependency>
  
  <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-jdbc</artifactId>
      <version>${spring.version}</version>
  </dependency>
  
  <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-webmvc</artifactId>
      <version>${spring.version}</version>
  </dependency>
  <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-aop</artifactId>
      <version>${spring.version}</version>
  </dependency>
  
  <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-context-support</artifactId>
      <version>${spring.version}</version>
  </dependency>
  
  <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-test</artifactId>
      <version>${spring.version}</version>
  </dependency>
  <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-jms</artifactId>
      <version>${spring.version}</version>
  </dependency>
  <!-- 日志相关依赖 -->
  <dependency>
      <groupId>log4j</groupId>
      <artifactId>log4j</artifactId>
      <version>1.2.17</version>
  </dependency>
  <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-api</artifactId>
      <version>1.6.1</version>
  </dependency>
  <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-log4j12</artifactId>
      <version>1.6.1</version>
  </dependency>
  ```

#### 2. 配置

**配置spring-rabbitmq.xml ：** 



### 六、Springboot整合RabbitMQ