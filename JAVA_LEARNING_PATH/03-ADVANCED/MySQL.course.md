---
layout: draft
title: 草稿
date: 2021-08-03 17:22:15
updated: 2021-08-03 17:22:30
---

## MySQL 教程

> 参考：http://www.notedeep.com/note/38/page/327

> MySQL 是一个关系型数据库管理系统，由瑞典 MySQL AB 公司开发，目前属于 Oracle 公司。MySQL 是一种关联数据库管理系统，关联数据库将数据保存在不同的表中，而不是将所有数据放在一个大仓库内，这样就增加了速度并提高了灵活性。



