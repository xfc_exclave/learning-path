---
layout: draft
title: 草稿
date: 2021-08-03 17:22:15
updated: 2021-08-03 17:22:30
---

## Oracle教程

> 参考：https://www.w3cschool.cn/oraclejc/

### 快速入门

#### Oracle 简介

**Oracle数据库是什么？** 

Oracle Database，又名Oracle RDBMS，简称Oracle。是甲骨文公司推出的一款关系数据库管理系统。

Oracle数据库系统是目前世界上流行的关系数据库管理系统，拥有可移植性好、使用方便、功能强等优点，在各类大、中、小、微机环境中都适用。

Oracle是一种高效率、可靠性好的、适应高吞吐量的数据库解决方案。

Oracle数据库服务器由一个数据库和至少一个数据库实例组成。数据库是一组存储数据的文件，而数据库实例则是管理数据库文件的内存结构。此外，数据库是由后台进程组成。

数据库和实例是紧密相连的，所以我们一般说的Oracle数据库，通常指的就是实例和数据库。

**物理存储结构** 

物理存储结构是存储数据的纯文件。当执行一个CREATE DATABASE语句来创建一个新的数据库时，将创建下列文件：

* **数据文件**：数据文件包含真实数据，例如销售订单和客户等。逻辑数据库结构(如表和索引)的数据被物理存储在数据文件中。
* **控制文件**：每个Oracle数据库都有一个包含元数据的控制文件。元数据用来描述包括数据库名称和数据文件位置的数据库物理结构。
* **联机重做日志文件**：每个Oracle数据库都有一个联机重做日志，里面包含两个或多个联机重做日志文件。联机重做日志由重做条目组成，能够记录下所有对数据所做的更改。

除这些文件外，Oracle数据库还包括如参数文件、网络文件、备份文件以及用于备份和恢复的归档重做日志文件等重要文件。

**逻辑存储结构** 

Oracle数据库使用逻辑存储结构对磁盘空间使用情况进行精细控制。以下是Oracle数据库中的逻辑存储结构：

* **数据块(Data blocks)**：Oracle将数据存储在数据块中。数据块也被称为逻辑块，Oracle块或页，对应于磁盘上的字节数。
* **范围(Extents)**：范围是用于存储特定类型信息的逻辑连续数据块的具体数量。
* **段(Segments)**：段是分配用于存储用户对象(例如表或索引)的一组范围。
* **表空间(Tablespaces)**：数据库被分成称为表空间的逻辑存储单元。 表空间是段的逻辑容器。 每个表空间至少包含一个数据文件。

Oracle实例由三个主要部分组成：系统全局区(SGA)，程序全局区(PGA)和后台进程。

#### Oracle 11g 安装

#### Oracle 12g 安装

#### Oracle 数据库创建导入

**创建新用户并授权** 

1. 使用 sqlplus 登录。

   > C:\User>Administrator>**sqlplus** 
   >
   > SQL*Plus: Release 19.0.0.0.0 - Production on 星期一 7月 27 15:59:09 2020
   > Version 19.3.0.0.0
   >
   > Copyright (c) 1982, 2019, Oracle.  All rights reserved.
   >
   > 请输入用户名:  **sys as sysdba** 
   > 输入口令:
   >
   > 连接到:
   > Oracle Database 19c Enterprise Edition Release 19.0.0.0.0 - Production
   > Version 19.3.0.0.0

2. 创建用户名及密码。

   示例：`CREATE USER EXCLAVE IDENTIFIED BY EXCLAVE123;` （用户名：EXCLAVE，密码：EXCLAVE123）

   > SQL> **CREATE USER EXCLAVE IDENTIFIED BY EXCLAVE123;** 
   >
   > 用户已创建。

3. 用户授权。

   > SQL> **GRANT CONNECT, RESOURCE, DBA TO EXCLAVE;** 
   >
   > 授权成功。

4. 登录新账号。

   注：EXCLAVE用户仅存在于ORCL数据库中，因此，必须在CONNECT命令中明确指定用户名为EXCLAVE@orcl。

   > SQL> **CONNECT EXCLAVE@orcl;** 
   > 输入口令:
   > 已连接。

**创建数据库表** 

假定已有用于创建数据库表的SQL脚本文件 `@D:\test\sql\my_test1.sql` ，执行该脚本文件，即可创建数据库表。

> SQL> **@D:\test\sql\my_test1.sql** 

查看当前用户拥有的所有表：

> SQL> **SELECT table_name FROM user_tables ORDER BY Table_name;** 

注：若当前用户不存在任何表，查询会提示 *未选定行* ，可通过以下命令查看用户拥有的表数量。

> SQL> **SELECT COUNT(table_name) FROM user_tables ORDER BY Table_name;** 

**将数据加载到表中** 

执行带有插入数据的 SQL 脚本即可。

#### Oracle数据库连接

可以通过上述示例中提到的 `sqlplus` 连接 oracle 数据库。

> C:\User>Administrator>**sqlplus** 

显示默认数据库服务名：

> SQL> **SHOW con_name;** 

切换到可插拔数据库（oracle 12c）：`SQL> ALTER SESSION SET CONTAINER = PDBORCL;` 

断开用户与Oracle数据库服务器的连接：

> SQL> **EXIT** 

**使用SQL Developer连接oracle** 



---

### 基础知识

#### Select

#### Order By

#### Distinct

#### Where

#### And

#### Or

#### Fetch

#### In

#### Between

#### Like

---

### 事务

#### Commit

#### Rollback

#### Transaction

#### Table

---

### 外键

#### 外键创建

#### 级联删除外键

#### 怎么删除外键

#### 禁用外键

#### 启用外键

---

### 字符串函数

#### Ascii() 函数

#### Asciistr() 函数

#### Chr() 函数

#### Compose() 函数

#### || 运算符

#### Convert() 函数

#### Dump() 函数

#### Inicap() 函数