---
title: 组件化高级
categories: [WEB前端, VUE入门教程]
tags: [前端, vue]
top_img: false
---

### 插槽的基本使用

> 插槽用于决定将所携带的内容，插入到指定的某个位置，从而使模板分块，具有模块化的特质和更大的重用性。通俗地理解，就是“占位”，在组件模板中占好了位置，当使用该组件标签时候，组件标签里面的内容就会自动填坑（替换组件模板中<slot>位置）。当插槽也就是坑<slot name="mySlot">有命名时，组件标签中使用属性slot=”mySlot”的元素就会替换该对应位置内容；

**代码** 

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>插槽的基本使用</title>
</head>
<body>

<div id="app">
    <!--不使用插槽-->
    <cpn></cpn>
    <!--使用插槽-->
    <cpn><span style="background: lightgreen;">哈哈哈</span></cpn>
    <cpn>
        <p style="background: #389eac; color: #fff;">多个元素一起作为插槽的替换内容时，</p>
        <p style="background: #389eac; color: #fff;">直接并列放在插槽这里就好了，它会被当作一个整体替换插槽中原有的内容</p>
    </cpn>
</div>

<template id="cpn">
    <div style="border: 1px solid lightgray; margin: 10px; padding: 10px;">
        <h2>组件标题</h2>
        <p>组件内容</p>
        <!--插槽中可以指定默认内容-->
        <p style="font-size: 13px;">下面是插槽内容展示区：</p>
        <div style="background: #ebf0f2; padding: 10px;">
            <slot><button>按钮</button></slot>
        </div>
    </div>
</template>

<script src="../js/vue.js"></script>
<script>
    const app = new Vue({
        el: '#app',
        data: {},
        components: {
            cpn: {
                template: '#cpn'
            }
        }
    })
</script>

</body>
</html>
```

---

### 具名插槽



**代码** 

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>具名插槽</title>
</head>
<body>

<div id="app">
    <cpn>
        <div slot="center" style="width: 50px; height: 50px; background: darkcyan; border-radius: 100%;">&nbsp;</div>
    </cpn>
    <cpn>
        <p>当未指定对应具名插槽，且模板中不存在未命名的插槽时，元素将不会显示</p>
        <div slot="left" style="width: 150px; height: 50px; background: aquamarine;">替换左侧插槽内容</div>
    </cpn>
</div>

<template id="cpn">
    <div style="margin: 10px; padding: 10px; border: 1px solid lightgray;">
        <p>插槽演示区：</p>
        <div style="display: inline-block; width: 30%; padding: 10px; background: lightblue;">
            <slot name="left"><span>无内容填充</span></slot>
        </div>
        <div style="display: inline-block; width: 30%; padding: 10px; background: lightyellow;">
            <slot name="center"><span>无内容填充</span></slot>
        </div>
        <div style="display: inline-block; width: 30%; padding: 10px; background: lightgreen;">
            <slot name="right"><span>无内容填充</span></slot>
        </div>
    </div>
</template>

<script src="../js/vue.js"></script>
<script>
    const app = new Vue({
        el: '#app',
        data: {},
        components: {
            cpn: {
                template: '#cpn'
            }
        }
    })
</script>

</body>
</html>
```

---

### 编译的作用域

> 组件标签中的相关属性，是来自其上层组件或Vue实例的作用域，即Vue实例或组件中定义的数据，其作用域包括其子组件的标签上的相关属性。

**代码** 

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>编译的作用域</title>
</head>
<body>

<div id="app">
    <!--此处使用的isShow的属性指向Vue实例的数据-->
    <cpn v-show="isShow"></cpn>
</div>

<template id="cpn">
    <div>
        <h2>子组件</h2>
        <!--此处使用的isShow的属性指向cpn组件中的数据-->
        <button v-show="isShow">按钮</button>
    </div>
</template>

<script src="../js/vue.js"></script>
<script>
    const app = new Vue({
        el: '#app',
        data: {
            // 此处数据的作用域Vue实例，包括其内部组件的标签中的相关属性，但不包括组件标签内部
            isShow: true
        },
        components: {
            cpn: {
                template: '#cpn',
                data() {
                    return {
                        // 此处数据作用域仅在于对应组件对应的双标签内部（不包括）
                        isShow: false
                    }
                }
            }
        }
    })
</script>

</body>
</html>
```

---

### 作用域插槽

> `作用域插槽` ，可以理解为它是一种带有数据的插槽，但其数据来源于插槽所在的作用域。

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>作用域插槽</title>
    <style>.h {color: #3c3c3c;}</style>
</head>
<body>

<div id="app">
    <p>组件中默认展示数据的方式:</p>
    <div style="background: #ebf0f2; padding: 10px;">
        <cpn></cpn>
    </div>

    <p>使用域名插槽自定义数据展示方式：</p>
    <div style="background: #ebf0f2; padding: 10px;">
        <cpn>
            <!--slot-scope="slot" 目的是获取子组件中的words-->
            <template slot-scope="anyName" slot="test-slot">
                <div style="color: #fff; letter-spacing: 2px;">
                    <p><span class="h">{{anyName.palindrome.substring(0, 7)}}</span>{{anyName.palindrome.substring(7)}}</p>
                    <p>{{anyName.palindrome.substring(0, 3)}}<span class="h">{{anyName.palindrome.substring(3, 10)}}</span>{{anyName.palindrome.substring(10)}}<span class="h"></span></p>
                    <p>{{anyName.palindrome.substring(0, 7)}}<span class="h">{{anyName.palindrome.substring(7, 14)}}</span></p>
                    <p><span class="h">{{anyName.palindrome.substring(0, 3)}}</span>{{anyName.palindrome.substring(3, 10)}}<span class="h">{{anyName.palindrome.substring(10)}}</span></p>
                </div>
            </template>
        </cpn>
    </div>
</div>

<template id="cpn">
    <div>
        <slot name="test-slot" :palindrome="words">
            <p>{{words}}</p>
        </slot>
    </div>
</template>
<script src="../js/vue.js"></script>
<script>
    const app = new Vue({
        el: '#app',
        data: {},
        components: {
            cpn: {
                template: '#cpn',
                data() {
                    return {
                        words: "赏花归去马如飞酒力微醒时已暮"
                    }
                }
            }
        }
    })
</script>

</body>
</html>
```

