---
title: vue脚手架的使用
categories: [WEB前端, VUE入门教程]
tags: [前端, vue]
top_img: false
---

### vue-cli安装使用

1. vue-cli 使用前提

   * nodejs
   * npm
   * webpack

2. 安装 vue-cli

   执行命令 `npm install -g @vue/cli` 安装全局 vue-cli 。（我这里版本是：v4.5.4）

3. 创建vue项目

   在对应文件夹目录下执行 `vue create project-name` 命令创建项目。

   * 选择预设置。（假定选择：Manually select features）

     > 选择是上下控制选项，空格控制是否选择。（假定选择：Choose Vue Version ; Babel）
     >
     > 选择完成使用回车。

   * 选择Vue版本。（假定选择：3.x）

   * 选择配置信息放置位置。（假定选择：In dedicated config files）

   * 是否保存为预设置。（假定选择：y）

   * 预设置命名。（假定：Jason）

     > 该自定义配置将被保存在 `C:\Users\用户名\.vuerc` 中。

4. 可能出现的错误。

   > 执行过程中，可能会出现报错：command failed: npm install --loglevel error
   >
   > 可以尝试清除npm缓存后重试，清除命令：npm cache clean --force

5. 目录结构

   ![011-001](http://119.3.48.114:8090/upload/2020/09/011-001-4e1cca4074c64f479825efd24c6769f4.jpg)

6. 运行

   > cd 到vue项目目录下，执行 `npm run serve` 。
   >
   > 浏览器访问：http://localhost:8080

7. vue ui

   在终端任何位置执行 `vue ui` 可运行一个vue可视化服务。

8. 如何修改默认配置

   1) 通过 vue ui 可视化界面配置。

   2) 在 `package.json` 同级目录下创建 `vue.config.js` 文件并进行配置。

> eslint - 代码规范检测