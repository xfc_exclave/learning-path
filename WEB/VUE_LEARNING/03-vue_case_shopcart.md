---
title: VUE综合案例-购物车
categories: [WEB前端, VUE入门教程]
tags: [前端, vue]
top_img: false
---

> 此处案例以淘宝购物车为参照，图片、文字、链接等信息都是即兴从淘宝商城拿下来的。

#### 截图预览

![003-001](https://img.xfc-exclave.com/2021/08/02/9ab2f949790f9259f9f20b8914dbcf7a1627890616.png)

#### style.css

> 这里的css代码并不重要，就压缩展示了。

```css
body{margin:0;padding:0;}a{color:#3c3c3c;text-decoration:none;}a:hover{color:#f60;text-decoration:underline;}.goods-table{width:100%;}.goods-table tr{border:1px solid red;margin:0;}#app{width:1200px;margin:50px auto 30px;}tbody{min-height:1000px;}.goods-table-head{background:lightblue;}.goods-table-head th{padding:10px 0;}.order-item{width:100%;margin:10px 0;padding:10px;height:120px;font-size:14px;background:#FCFCFC;}.order-active{background:#ebf0f2;}.order-item:hover{background:#ebf0f2;}.g-td-index{padding:25px;}.g-td-image{width:12%;text-align:center;}.goods-image{width:100px;height:100px;}.g-td-name{display:inline-block;color:#3c3c3c;padding:10px;height:100px;}.g-td-shop{width:15%;padding:20px;text-align:center;}.g-td-price{padding:20px;text-align:center;font-weight:bold;}.g-td-count{width:10%;padding:20px 0;text-align:center;}.g-td-count input{width:30px;text-align:center;}.g-td-amount{padding:20px;text-align:center;color:#f40;font-weight:bold;}.g-td-oper{width:15%;padding:20px;text-align:center;font-size:13px;}.balance-div{margin:20px 0;background:#e5e5e5;height:50px;line-height:50px;vertical-align:middle;width:1200px;position:fixed;bottom:0;}.balance-info-l{float:left;}.balance-info-r{float:right;width:50%;}.balance-info-r div{float:right;}.balance-btn{background:#f40;padding:0 30px;color:#fff;font-weight:bold;font-size:20px;letter-spacing:5px;}.total-price-box{width:25%;}.total-price{color:#f40;font-weight:bold;font-size:20px;}
```



#### main.js

```js
const app = new Vue({
    el: '#app',
    data: {
        goods: [
            {
                name: "单向历2020年文艺手撕日历定制送礼单向空间创意简约桌面台历摆件",
                image: "https://img.alicdn.com/bao/uploaded/i4/2938395088/O1CN01s1xpzJ1nSLjmj1N2m_!!2938395088.jpg",
                url: "https://detail.tmall.com/item.htm?id=601526627486",
                shop: "单向空间旗舰店",
                price: 98.00,
                checked: false,
                count: 5
            },
            {
                name: "varmilo阿米洛锦鲤机械键盘108cherry樱桃红轴女生游戏办公红色",
                image: "https://img.alicdn.com/bao/uploaded/i1/2861950811/O1CN01JeUw0P1HrTnEvnpgX_!!2861950811.jpg",
                url: "https://detail.tmall.com/item.htm?id=594135432115",
                shop: "varmilo旗舰店",
                price: 1059.00,
                checked: true,
                count: 1
            },
            {
                name: "文艺男女店印花宽松短袖T恤男夏季港风半袖百搭上衣韩版潮流体恤",
                image: "https://img.alicdn.com/bao/uploaded/i3/266923241/O1CN01oeVpwv1ZoQFizbRaY_!!266923241.jpg",
                url: "https://item.taobao.com/item.htm?id=620080227102",
                shop: "文艺男女店",
                price: 78.00,
                checked: false,
                count: 2
            },
            {
                name: "艾漫正版 一人之下周边王也同款茶杯喝水杯第二弹【预售】",
                image: "https://img.alicdn.com/bao/uploaded/i4/2923300187/O1CN01N4MvD51DFgfU1wpEu_!!2923300187.jpg",
                url: "https://detail.tmall.com/item.htm?id=620852178392",
                shop: "萌热动漫旗舰店",
                price: 48.00,
                checked: false,
                count: 12
            }
        ],
        activeClass: 'order-active'
    },
    methods: {
        subCount(index) {
            this.goods[index].count--;
        },
        addCount(index) {
            this.goods[index].count++;
        },
        remove(index) {
            this.goods.splice(index, 1);
        },
        checkGoods(index, e) {
            this.goods[index].checked = e.target.checked;
        }
    },
    computed: {
        totalPrice() {
            // 1.遍历this.goods数组并计算单项商品总价数组
            // 2.使用reduce函数将商品总价数组中每一项的值进行累加操作
            return this.goods.map(item => item.price * item.count * (item.checked ? 1 : 0)).reduce((preValue, currentValue) => preValue + currentValue);
        }
    },
    // 过滤器
    filters: {
        showPrice: function(price) {
            let priceStr = price.toFixed(2);
            return "￥" + priceStr;
        }
    }
});
```



#### index.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>综合案例 - 购物车</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<div id="app">
    <table class="goods-table">
        <thead class="goods-table-head">
            <th></th><th>预览图片</th><th>商品信息</th><th>店铺</th><th>单价</th><th>数量</th><th>金额</th><th>操作</th>
        </thead>
        <tbody>
            <tr v-for="(item, index) in goods" :class="['order-item', item.checked ? activeClass: '']">
                <td class="g-td-index"><input type="checkbox" :checked="item.checked" :value="index" @click="checkGoods(index, $event)"></td>
                <td class="g-td-image"><img :src="item.image" alt="" class="goods-image"></td>
                <td class="g-td-name"><a :href="item.url" target="_blank">{{item.name}}</a></td>
                <td class="g-td-shop">{{item.shop}}</td>
                <td class="g-td-price">{{item.price | showPrice}}</td>
                <td class="g-td-count">
                    <button @click="subCount(index)" :disabled="item.count <= 1">-</button>
                    <input type="text" :value="item.count" readonly>
                    <button @click="addCount(index)">+</button>
                </td>
                <td class="g-td-amount">{{item.price * item.count | showPrice}}</td>
                <td class="g-td-oper">
                    <a href="javascript:;" @click="remove(index)"><span>删除</span></a>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="balance-div">
        <div class="balance-info-l"></div>
        <div class="balance-info-r">
            <a href="javascript:;"><div class="balance-btn">结算</div></a>
            <div class="total-price-box">合计 <span class="total-price">{{totalPrice | showPrice}}</span></div>
        </div>
        <div style="clear: both;"></div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
<script src="main.js"></script>
</body>
</html>
```

