---
title: webpack
categories: [WEB前端, VUE入门教程]
tags: [前端, vue]
top_img: false
---

> 使用webpack进行打包，可以使项目能够应用多种模块化规范进行模块化开发。

### 起步

1. 创建基本目录结构，创建测试文件 `commonjs.js` 和 `es6.js` 用来测试两种模块化规范。

   * commonjs.js

     ```javascript
     // 使用commonjs的模块化规范
     function add(num1, num2) {
         return num1 + num2
     }
     
     const number1 = 100;
     const number2 = 30;
     
     module.exports = {
         add,
         number1,
         number2
     };
     ```

   * es6.js

     ```javascript
     // 使用es6的模块化规范
     export const str = 'es6.js > str 被调用';
     export const getStr = function(str) {
         console.log("es6.js > getStr() 被调用，字符串内容：" + str);
     };
     ```

2. 创建 `main.js` 作为入口文件。

   * main.js

     ```javascript
     // 1.使用commonjs的模块化规范
     const {add, number1, number2} = require('./commonjs.js')
     
     console.log(add(number1, number2));
     
     // 2.使用ES6的模块化的规范
     import {getStr, str} from "./es6";
     
     getStr(str);
     ```

3. 使用 `webpack` 进行打包。

   > 打包前的目录：

   ![008-001](http://119.3.48.114:8090/upload/2020/08/008-001-c16b3a79777042a7a2e62401320eb529.jpg)

   > 在文件路径下执行打包命令 `webpack ./src/main.js -o ./dist/bundle.js` 将 `main.js` 及相关文件打包到 `bundle.js` 中。执行命令后，我们可以看到 `dist` 文件夹下生成了新的 `bundle.js` 文件

   ![008-002](http://119.3.48.114:8090/upload/2020/08/008-002-30d264c578de48b79517ffc2491b7e37.jpg)

   > 注意：较低版本的 `webpack` 打包命令可能略有不同：`webpack ./src/main.js ./dist/bundle.js ` 。

4. 创建 `index.html` 进行引用测试。

   * 在 dist 平级目录下创建 `index.html` 并引用 `bundle.js` 。

     ```html
     <!DOCTYPE html>
     <html lang="en">
     <head>
         <meta charset="UTF-8">
         <title>Webpack起步示例</title>
     </head>
     <body>
     <div>Webpack演示页面</div>
     <script src="./dist/bundle.js"></script>
     </body>
     </html>
     ```

5. 运行并查看效果

   ![008-003](http://119.3.48.114:8090/upload/2020/08/008-003-e65eccc2d3254bda98337276255cab2b.jpg)

### 配置

1. 在 `dist` 文件夹同级目录下创建 `webpack.config.js` 。

   * webpack.config.js（固定名称）

     ```js
     const path = require('path');// 通过commonJS引入path
     
     module.exports = {
         entry:'./src/main.js',
         output: {
             path: path.resolve(__dirname, 'dist'),// 绝对路径，打包位置
             filename: 'bundle.js'
         }
     };
     ```

   测试：

   在【起步】中，我们使用 `webpack ./src/main.js -o ./dist/bundle.js` 进行打包，通过配置 `webpack.config.js` 后，直接执行 `webpack` 即可实现打包。

2. 初始化项目

   执行命令 `npm init` 或 `npm init -y` 实现项目的初始化，前者需要手动选择部分初始化信息，后者则会使用默认方案。执行完毕后，项目中会生成 `package.json` 文件。

3. 安装 `webpack` 

   这里安装 `webpack` 区别于全局安装，指的是项目内部的安装。

   在当前项目目录下执行命令 `npm install webpack --save-dev` ，其中， `--save-dev` 是指当前安装的webpack仅用于开发环境，安装完成后， `package.json` 中可以看到 `devDepencies` 引入的版本号。

   执行完毕后，当前项目中会生成 `node_modules` 包及 `package-lock.json` 文件。

4. 为项目配置执行脚本

   在 `package.json > scripts` 中，添加需要执行的命令，如 `"build": "webpack"` ，这样我们就可以通过 `npm run *` 的方式执行对应的命令，如 `npm run build` ，而通过这种方式执行命令，该命令会优先在项目中查找相应的工具进行执行，如果项目中不存在，则在全局进行查找。

5. 执行 `npm run build` 进行打包。

### loader

**使用过程** 

* 通过 npm 安装需要使用的 loader 。
* 在 `webpack.config.js` 中的 modules 关键字下进行配置。

**配置loader** 

1. 将 `commonjs.js` 和 `es6.js` 移动至 `./src/js` 目录下。

2. 在 `./src/css` 目录下创建文件 `normal.css` 文件。

   ```css
   body {
       background: #ebf0f2;
   }
   ```

3. 向 `index.html` 文件引入 `normal.css` 。

   > 无需在 `index.html` 文件中使用 link标签引入，而是通过 `main.js` 入口文件进行依赖， `main.js` 文件内容如下：

   ```js
   // 1.使用commonjs的模块化规范
   const {add, number1, number2} = require('./js/commonjs.js')
   
   console.log(add(number1, number2));
   
   // 2.使用ES6的模块化的规范
   import {getStr, str} from "./js/es6";
   
   getStr(str);
   
   // 3. 依赖css文件
   require('./css/normal.css');
   ```

4. 尝试使用 `npm run build` 进行打包。

   此时打包控制台提示报错：

   > ERROR in ./src/css/normal.css 1:5
   >
   > Module parse failed: Unexpected token (1:5)
   >
   > You may need an appropriate loader to handle this file type, currently no loaders are configured to process this file.
   >
   > > 大意：你可能需要一个合适的 loader 来处理这个文件类型，当前没有配置可处理这个文件的 loader。

5. 引入 loader

   可前往 https://www.webpackjs.com/loaders/ 查找需要的 loader （这里需要 css-loader）。

   ![008-004](http://119.3.48.114:8090/upload/2020/08/007-001-af7c1ecc7bf64ad38d9fc80bb4b45255.jpg)

   根据文档提示执行 `npm install --save-dev css-loader` 安装 `css-loader` 。

6. 补充配置文件 `webpack.config.js` 文件。

   ```js
   const path = require('path');// 通过commonJS引入path
   
   module.exports = {
       entry:'./src/main.js',
       output: {
           path: path.resolve(__dirname, 'dist'),// 绝对路径，打包位置
           filename: 'bundle.js'
       },
       module: {
           rules: [
               {
                   test: /\.css$/,
                   use: [ 'css-loader' ]
               }
           ]
       }
   };
   ```

   > 注： `css-loader` 只负责加载 css 文件，而 `style-loader` 则负责将样式加载到 DOM 中，因此 `css-loader` 需要与 `style.css` 配合进行使用。即需要再执行 `npm install style-loader --save-dev` 命令安装 `style-loader` 。
   >
   > 另外，使用多个 loader 时，是从右往左进行加载的。

7. 再次使用 `npm run dev` 进行打包，并在浏览器查看 `index.html` 样式效果，此时 `normal.css` 样式生效。

### .less 文件处理

1. 在 `./src/css` 目录下创建 `special.less` 文件。

   ```less
   @fontSize: 20px;
   @fontColor: red;
   @fontWeight: bold;
   
   body {
     font-size: @fontSize;
     color: @fontColor;
     font-weight: @fontWeight;
   }
   ```

2. 在 `main.js` 中引入 `special.less` 文件。

   ```js
   // 1.使用commonjs的模块化规范
   const {add, number1, number2} = require('./js/commonjs.js')
   
   console.log(add(number1, number2));
   
   // 2.使用ES6的模块化的规范
   import {getStr, str} from "./js/es6";
   
   getStr(str);
   
   // 3. 依赖css文件
   require('./css/normal.css');
   
   // 4. 依赖less文件
   require('./css/special.less')
   ```

3. 执行 `npm install --save-dev less-loader less` 安装 `less-loader` 。

4. `webpack.config.js` 

   ```js
   const path = require('path');// 通过commonJS引入path
   
   module.exports = {
       entry:'./src/main.js',
       output: {
           path: path.resolve(__dirname, 'dist'),// 绝对路径，打包位置
           filename: 'bundle.js',
           publicPath: "dist/"
       },
       module: {
           rules: [
               {
                   test: /\.css$/,
                   use: [ 'style-loader', 'css-loader' ]
               },
               {
                   test: /\.less$/,
                   use: ["style-loader", "css-loader", "less-loader"]
               }
           ]
       }
   };
   ```

5. `npm run build` 

### 图片文件处理

> 当直接只用在线文件时，不需要使用任何 loader 即可进行加载。而对于本地加载的图片文件，则需要使用 url-loader 进行加载。

同上述 `.less 文件处理` 相似。

1. 在 `./src/img` 目录下引入图片文件，假定为文件名为 `timg.jpg` 。

2. 执行 `npm install --save-dev url-loader` 安装 `url-loader` 并更新 `webpack.config.js` 文件。

   ```js
   const path = require('path');// 通过commonJS引入path
   
   module.exports = {
       entry:'./src/main.js',
       output: {
           path: path.resolve(__dirname, 'dist'),// 绝对路径，打包位置
           filename: 'bundle.js',
           publicPath: "dist/"
       },
       module: {
           rules: [
               {
                   test: /\.css$/,
                   use: [ 'style-loader', 'css-loader' ]
               },
               {
                   test: /\.less$/,
                   use: ["style-loader", "css-loader", "less-loader"]
               },
               {
                   test: /\.(png|jpg|gif)$/,
                   use: [
                       {
                           loader: 'url-loader',
                           options: {
                               limit: 8192
                           },
                           name: 'img/[name].[hash:8].[ext]'
                       }
                   ]
               }
           ]
       }
   };
   ```

   > 注：
   >
   > * `putput > publicPath` 指定打包后的 url 相关文件的存放位置。
   > * `url-loader` 中的 `limit` 属性指定不同大小的图片的处理方式：当需要加载的文件大小在 limit 属性指定范围内时（默认为8kb），对应图片文件将使用 base64 形式进行处理；当需要加载的文件大小超过 limit 属性的限制时，该图片文件则会被打包到指定目录下，但此时需要另外使用 `file-loader` 进行处理，且打包后该文件将以哈希方式进行重命名。

3. 执行 `npm install --save-dev file-loader` 安装 `file-loader` 。

   此时可以不用再 `webpack.config.js` 中配置 `file-loader` 。

4. `npm run build` 

   > 在 `url-loader` 中可以通过 `name` 属性定义文件被打包后的的名称。

### babel-loader

可以将 ES6 语法转换 ES5 语法进行打包。配置详情参考：https://www.webpackjs.com/loaders/babel-loader/

其他更多 loader 的使用方式类似，均可参照官网文档。