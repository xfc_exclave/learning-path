---
title: vue的前端路由
categories: [WEB前端, VUE入门教程]
tags: [前端, vue]
top_img: false
---

### 一、前端路由

> 理解性。网上查找。
>
> * 什么是前端渲染，什么是后端渲染？
> * 什么是前后端分离？
> * 什么是前端路由，什么是后端路由？

---

### 二、vue-router

#### 前置

* 使用脚手架创建项目

  通过 vue-cli 创建项目，假定项目名为 `router-demo` 。

* 通过前端改变地址

  1. URL的Hash

     URL的hash也就是锚点(#), 本质上是改变window.location的href属性，我们可以通过直接赋值location.hash来改变href, 但是页面不发生刷新。

     示例：

     ```js
     location.hash = "profile";
     ```

  2. HTML5 的 history 模式：pushState

     history接口是HTML5新增的, 它有五种模式改变URL而不刷新页面。

     示例：

     ```js
     history.pushState({}, "", "profile");// 类似栈结构，先入后出
     history.back();// 使用此方法返回上一路由页
     ```

  3. HTML5 的 history 模式：replaceState

     replaceState与pushState类似，但replaceState方法不会存储替换之前的页面地址。

     示例：

     ```js
     history.replaceState({}, "", "profile");
     ```

  4. HTML5 的 history 模式：go

     ```js
     history.go(-1);// 相当于history.back();
     ```

  5. HTML5 的 history 模式：forward

     ```js
     history.forward();// 相当于history.go(1);
     ```

#### 安装和使用vue-router

* 安装vue-router

  在使用脚手架创建项目时，会选择是否安装 vue-router ，如果安装时未选择 vue-router 插件，可以通过启动 `vue ui` 前往控制台 http://localhost:8000/project/select 选择导入当前项目，并在 http://localhost:8000/dependencies 选择 `安装依赖` ，搜索 `vue-router` 并安装。

* 导入路由对象。

  在 src 目录下创建 `index.js` 用于配置所有路由相关信息。

* 创建路由实例，并传入路由映射配置。

* 在Vue实例中挂载路由实例。

```js
// index.js
// 配置所有路由相关的信息
import VueRouter from "vue-router";
import Vue from 'vue';

// 1. 通过Vue.use(插件)安装插件
Vue.use(VueRouter);

const routes = [];
// 2. 创建VueRouter对象
const router = new VueRouter({
    // 配置路由和组件之间的引用关系。
    routes: routes
});

// 3. 将router对象传入到vue实例
export default router;
```

```js
// main.js
import { createApp } from 'vue'
import App from './App.vue'
import router from "./router";

// vue-cli 3
createApp(App).use(router).mount('#app')
```

* 配置路由映射关系

  * 创建路由组件

    在 components 目录下创建组件 `Home.vue` 、 `About.vue` 。

  * 配置路由映射，组件和路径的映射关系。

  * 使用路由： `\<router-link\>` 和 `\<router-view\>` 。

#### router-link

#### 动态路由

#### 路由的懒加载

#### 路由嵌套（子路由）

#### 路由的参数传递

#### 全局导航守卫

* 前置守卫
* 后置守卫
* 路由独享的守卫
* 组件内的守卫

#### keep-alive

actived / deactived

include / exclude

---

### 三、项目实例



