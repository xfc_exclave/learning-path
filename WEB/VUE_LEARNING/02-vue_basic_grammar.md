---
title: VUE基础语法
categories: [WEB前端, VUE入门教程]
tags: [前端, vue]
top_img: false
---

**预备** 

> 为了美观一点，以下代码示例均引入了一些 `style.css` 样式，样式代码如下：

```css
ul>li{list-style:none;}#app{width:1000px;margin:0 auto;background:#f8f9f9;margin-bottom:300px;}.part{padding:20px;}.note{color:gray;border-left:4px solid #389eac;padding-left:10px;}.test{background:#fff;padding:10px;margin-bottom:5px;}.highlight{color:darkred;border:1px solid lightgray;padding:0 5px;margin:0 5px;}.active{font-weight:bold;color:cadetblue;}.text_shadow{text-shadow:5px 5px 5px gray;}
```

---

### 插值操作

**代码** 

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>vue插值操作</title>
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>

<div id="app" v-cloak>
    <div class="part">
        <h4>v-once</h4>
        <p class="note">不使用 v-once</p>
        <div class="test">数据：{{message}}</div>
        <p class="note">使用 v-once：只会渲染一次，数据改变不会再次渲染</p>
        <div v-once class="test">数据：{{message}}</div>
    </div>
    <div class="part">
        <h4>v-html</h4>
        <p class="note">不使用 v-html 指令</p>
        <div class="test">{{element}}</div>
        <p class="note">使用 v-html 指令</p>
        <div v-html="element" class="test"></div>
    </div>
    <div class="part">
        <h4>v-text</h4>
        <p class="note">不使用 v-text，会进行拼接</p>
        <div class="test">未曾着眼足轻狂，{{text}}</div>
        <p class="note">使用 v-text，则会替换内容</p>
        <div class="test" v-text="text">[乱七八糟的内容] sdglkdjsfksjerljkdljf</div>
    </div>
    <div class="part">
        <h4>v-pre</h4>
        <p class="note">不使用 v-pre</p>
        <div class="test">{{message}}</div>
        <p class="note">使用 v-pre，则不会对Mustache语法进行解析</p>
        <div class="test" v-pre>{{message}}</div>
    </div>
    <div class="part">
        <h4>v-cloak</h4>
        <p class="note">在需要渲染的数据较多的情况下，数据未完成渲染时，用户界面Mustache语法会有短暂的不被渲染的间隙，使用v-cloak可以先隐藏页面内容，等数据渲染完成后再显示</p>
    </div>
</div>

<!--<script src="../js/vue.js"></script>-->
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
<script>
    const app = new Vue({
        el: '#app',
        data: {
            message: '在Vue实例中声明的数据。',
            element: '<a href="https://www.cnblogs.com/xfc-exclave" target="_blank">点我跳转博客首页</a>',
            text: '霜冷才出白月光'
        }
    })
</script>
</body>
</html>

<!--
v-once效果预览
app.message = '从浏览器控制台修改后的数据'
app.message = '再次改变'
-->
```

**说明** 

> 相关说明详见代码内容。

**效果预览：** 

![002-001](https://img.xfc-exclave.com/2021/08/02/ee3720bba4964373db57904f50783f181627890489.jpg)

### 动态绑定属性

**代码** 

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>动态绑定属性</title>
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>

<div id="app">
    <div class="part">
        <h4>v-bind 基本使用</h4>
        <p class="note">使用 v-bind 绑定属性，如：href、src等，<span class="highlight">v-bind:</span>语法糖写法为<span class="highlight">:</span></p>
        <div class="test">
            <a v-bind:href="href" target="_blank">
                <img :src="imgUrl" alt="" style="width: 100%;">
            </a>
        </div>
    </div>

    <div class="part">
        <h4>v-bind 动态绑定class(对象语法)</h4>
        <div class="note">
            <p>可以使用 boolean 值进行class动态绑定，示例：<span class="highlight">:class="{active: isActive}"</span></p>
            <p>也可以通过方法进行绑定，示例：<span class="highlight">:class="getActiveAttr()"</span></p>
        </div>
        <div class="test">
            <ul>
                <li v-for="menu in menus" style="float: left; margin: 0 5px; padding: 2px 5px; cursor: pointer;" :class="{active: isActive}">{{menu}}</li>
                <button @click="toggleClass">切换active状态</button>
                <div style="clear: both;"></div>
            </ul>
        </div>
        <div class="test">
            <ul>
                <li v-for="menu in menus" style="float: left; margin: 0 5px; padding: 2px 5px; cursor: pointer;" :class="getActiveAttr()">{{menu}}</li>
                <button @click="toggleClass">切换active状态</button>
                <div style="clear: both;"></div>
            </ul>
        </div>
    </div>

    <div class="part">
        <h4>v-bind 动态绑定class(数组语法)</h4>
        <div class="note">
            <p>v-bind 动态绑定class可以使用数组，但数组中携带单引号的内容会被解析为字符串，如示例中的【首页】</p>
        </div>
        <div class="test">
            <ul>
                <li :class="['active', 'shadow']" style="float: left; margin: 0 5px; padding: 2px 5px; cursor: pointer;">首页</li>
                <li :class="[active, shadow]" style="float: left; margin: 0 5px; padding: 2px 5px; cursor: pointer;">都市</li>
                <li :class="getActiveAttr2()" style="float: left; margin: 0 5px; padding: 2px 5px; cursor: pointer;">言情</li>
                <div style="clear: both;"></div>
            </ul>
        </div>
    </div>

    <div class="part">
        <h4>v-for 与 v-bind 结合使用</h4>
        <div class="note">
            <p>通过事件绑定传递参数实现切换，示例：<span class="highlight">@click="choseActive(index)"</span></p>
            <p>以下示例通过点击事件传递数组当前数据下标值，并通过对比遍历数组对比下表值动态绑定class</p>
        </div>
        <div class="test">
            <ul>
                <li v-for="(item, index) in menus" :key="index" :class="{active: index === currentIndex}" @click="choseActive(index)" style="float: left; margin: 0 5px; padding: 2px 5px; cursor: pointer;">{{item}}</li>
                <div style="clear: both;"></div>
            </ul>
        </div>
    </div>

    <div class="part">
        <h4>v-bind 动态绑定style(对象语法)</h4>
        <div class="note">
            <p><span class="highlight">:style</span>属性内带有单引号的值会被解析为字符串，如示例一</p>
            <p>不加单引号则会被解析为变量，如示例二的<span class="highlight">dimred</span></p>
            <p>同样，<span class="highlight">:style</span>的属性也可以是函数，如示例三</p>
        </div>
        <div class="test">
            <div :style="{borderRadius: '50%', width: '100px', height: '100px', background: 'lightgreen'}" style="float: left;">示例一</div>
            <div :style="{borderRadius: '50%', width: '100px', height: '100px', background: dimred}" style="float: left;">示例二</div>
            <div :style="getStyle()" style="float: left;">示例三</div>
            <div style="clear: both;"></div>
        </div>
    </div>

    <div class="part">
        <h4>v-bind 动态绑定style(数组语法)</h4>
        <div class="test">
            <div :style="[square, {border: '1px solid blue'}]" style="float: left;">数组语法示例</div>
            <div style="clear: both;"></div>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
<script>
    const app = new Vue({
        el: '#app',
        data: {
            message: '在Vue实例中声明的数据。',
            imgUrl: 'https://jianzi-1301046789.cos.ap-chengdu.myqcloud.com/public/5db57d48ee404c4680ec2135b7249dd6.jpg',
            href: 'https://www.cnblogs.com/xfc-exclave',
            isActive: false,
            menus: ['首页', '分类', '购物车', '个人中心'],
            active: 'active',
            shadow: 'text_shadow',
            currentIndex: 0,
            dimred: 'darkred',
            square: {width: '100px', height: '100px', background: 'lightblue'}
        },
        methods: {
            toggleClass() {
                this.isActive = !this.isActive;
            },
            getActiveAttr() {
                return {active: this.isActive};
            },
            getActiveAttr2() {
                return [this.active, this.shadow];
            },
            choseActive(index) {
                this.currentIndex = index;
            },
            getStyle() {
                return this.square;
            }
        }
    })
</script>
</body>
</html>
```

**说明** 

> 相关说明详见代码内容。

**效果预览：** 

![002-002](https://img.xfc-exclave.com/2021/08/02/d9b42e8038db2944f8e76669b81166c21627890505.jpg)

---

### 计算属性

**代码** 

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>计算属性</title>
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>

<div id="app">
    <div class="part">
        <h4>计算属性基本使用</h4>
        <div class="note">
            <p>计算属性<span class="highlight">computed</span>，返回一个计算后的值。</p>
            <p>以下分别是通过 methods 和 computed 实现的字符串拼接。使用语法分别是：<span class="highlight" v-pre>{{getContent()}}</span><span class="highlight" v-pre>{{getFirstHalf}}</span></p>
        </div>
        <div class="test">
            <p>{{getContent()}}</p>
            <p>{{getFirstHalf}}</p>
        </div>
    </div>

    <div class="part">
        <h4>复杂计算</h4>
        <div class="note">
            <p>此处示例使用reduce函数，并传入ES6箭头函数对象实现计算总和。</p>
        </div>
        <div class="test">
            <p>购书总价：￥{{getTotalPrice}}</p>
        </div>
    </div>

    <div class="part">
        <h4>计算属性的 getter 和 setter</h4>
        <div class="note">
            <p>每个计算属性都包含一个getter和一个setter，只是通常情况下，setter方法不常用。</p>
            <p>计算属性具有缓存，多次使用只会调用一次，只有当计算属性中的相关值被修改时，该计算属性才会再次被调用。</p>
        </div>
        <div class="test">
            <p>{{getSecondHalf}}</p>
            <p>{{getSecondHalf}}</p>
            <p>{{getSecondHalf}}</p>
            <p>{{getSecondHalf}}</p>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
<script>
    const app = new Vue({
        el: '#app',
        data: {
            str1: '江风咽，燕矶残照红蓼月。',
            str2: '红蓼月，梨云梦断，故园轻别。',
            str3: '绿烟尘外红牙节，夜残更漏人声绝。',
            str4: '人声绝，一年离索，一帷星阙。',
            books: [
                {name: '诗人十四个', price: 52.00, count: 3},
                {name: '泥淖之子', price: 48.00, count: 1},
                {name: '新诗十讲', price: 58.00, count: 2}
            ]
        },
        methods: {
            getContent() {
                return this.str1 + this.str2;
            }
        },
        computed: {
            getFirstHalf() {
                return this.str1 + this.str2;
            },
            getTotalPrice() {
                // 使用es6箭头函数
                // reduce() 方法接收一个函数作为累加器，数组中的每个值（从左到右）开始缩减，最终计算为一个值。
                // reduce(function(total, currentValue, currentIndex, arr), initialValue)
                // 这里传入一个箭头函数，total为累加值，book为数组遍历的当前对象
                return this.books.reduce((total, book) => {
                    return total + book.price * book.count;
                }, 0)
            },
            getSecondHalf: {
                // 计算属性内部有侦听器，只有当计算属性相关值发生变化，get()方法才会再次被调用，未发生变化时，调用计算属性会从缓存中获取
                get() {
                    console.log('getSecondHalf.get()方法被调用，计算属性具有缓存，多次使用只会调用一次');
                    return this.str3 + this.str4;
                },
                set(authorText) {
                    console.log('getSecondHalf.set()方法被调用');
                    this.str4 += authorText;
                }
            }
        }
    })
</script>
</body>
</html>

<!--
app.getSecondHalf = 'by 许阜晨'
-->
```

**测试** 

1. 运行代码。
2. 查看浏览器控制台，多次使用 `{{getSecondHalf}}` ，但 `getSecondHalf.get()` 方法只被调用了一次。
3. 控制台输入 `app.getSecondHalf = 'by 许阜晨'` 后， `getSecondHalf.get()` 再次被调用。

**效果预览：** 

![002-003](https://img.xfc-exclave.com/2021/08/02/51af1cf1aff6c48a71c589f1f03663751627890543.jpg)

---

### 事件监听与逻辑判断

#### 1. v-on

#### 2. v-if、v-else、v-elseif

#### 3. v-show

#### 4. v-for

> 详细用法及说明见代码内容及注释

**代码** 

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>事件监听与逻辑判断</title>
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>

<div id="app">
    <div class="part">
        <h4>v-on</h4>
        <div class="note">
            <p>vue使用<span class="highlight">v-on</span>进行事件监听</p>
        </div>
        <div class="test">
            <!-- 不需要传递参数时可以省略括号 -->
            <button @click="bindClick1">不传递参数，不带括号</button>
            <button @click="bindClick1()">不传递参数，带括号</button>
            <button @click="bindClick2(1)">传递参数</button>
            <button @click="bindClick2(1, $event)">传递参数，带事件对象</button>
        </div>
    </div>

    <div class="part">
        <h4>v-on的修饰词</h4>
        <div class="note">
            <p><span class="highlight">.stop</span>可以阻止事件传播，禁止事件冒泡。</p>
            <p><span class="highlight">.prevent</span>可以阻止事件默认行为，如form表单的button提交。</p>
            <p><span class="highlight">.enter</span>可以进行键盘事件监听，常用于input元素。</p>
        </div>
        <div class="test">
            <div @click="showParentInfo()" style="background: pink; padding: 15px;">
                <span @click="showChildInfo()" style="background: #ebf0f2; cursor: pointer; padding: 10px;">按钮A</span>
                <span>点击按钮A，父级点击事件也会被调用。</span>
            </div>
            <div @click="showParentInfo()" style="background: lightcyan; padding: 15px;">
                <span @click.stop="showChildInfo()" style="background: #f8f9f9; cursor: pointer; padding: 10px;">按钮B</span>
                <span>点击按钮B，父级点击事件不会被调用。</span>
            </div>
        </div>
        <div class="test">
            <form action="/test" method="get" style="background: lightblue; padding: 15px;">
                <!-- 这里的 v-model 作用是进行数据的双向绑定，后续详细解释-->
                <input type="text" name="username" v-model="username">
                <button @click="handleSubmit()">提交</button>
                <span>点击提交按钮，将会使用默认的表单进行提交。</span>
            </form>
            <form action="/test" method="get" style="background: lightgreen; padding: 15px;">
                <!-- 这里的 v-model 作用是进行数据的双向绑定，后续详细解释-->
                <input type="text" name="username" v-model="username">
                <button @click.prevent="handleSubmit()">提交</button>
                <span>点击提交按钮，将进入<span class="highlight">@click.prevent</span>定义的事件。</span>
            </form>
        </div>
        <div class="test">
            <div style="background: lightsalmon; padding: 15px;">
                <input type="text" @click.enter="qualifierTest" placeholder="光标进入时触发">
                <input type="text" @keyup="qualifierTest" placeholder="按下任意按键触发">
                <input type="text" @keyup.13="qualifierTest" placeholder="按下回车键触发">
                <input type="text" @keyup.enter="qualifierTest" placeholder="按下回车键触发">
            </div>
        </div>
    </div>

    <div class="part">
        <h4>v-if、v-else、v-elseif</h4>
        <div class="note">
            <p>以下示例使用 v-if，v-else-if，v-else进行综合演示</p>
        </div>
        <div class="test">
            <div v-if="isLogin">
                <p>登录成功，您当前的角色是：
                    <span v-if="selectedRole == 1">学生</span>
                    <span v-else-if="selectedRole == 2">老师</span>
                    <span v-else>管理员</span>
                </p>
                <button @click="isLogin = !isLogin">注销</button>
            </div>
            <div v-else>
                <p>您尚未登录，请选择角色一键登录</p>
                <div>
                    <select name="role" id="role" v-model="selectedRole">
                        <option v-for="role in roleList" :value="role.id">{{role.roleName}}</option>
                    </select>
                    <button @click="isLogin = !isLogin">一键登录</button>
                </div>
            </div>
        </div>
    </div>

    <div class="part">
        <h4>v-show</h4>
        <div class="note">
            <p><span class="highlight">v-show</span>和<span class="highlight">v-if</span>一样能够控制元素的显示，不同之处在于前者是通过样式来控制元素的显示与隐藏，其元素是会被渲染到页面上的，而后者当元素不显示时，元素是不会被渲染的。</p>
        </div>
        <div class="test">
            <button v-if="isHere" @click="isHere = !isHere">闭眼</button><button v-else @click="isHere = !isHere">睁眼</button>
            <p v-if="isHere">我是v-if，你闭眼我就消失</p>
            <p v-show="isHere">我是v-show，你闭眼我就藏起来</p>
        </div>
    </div>

    <div class="part">
        <h4>v-for</h4>
        <div class="note">
            <p>前面的示例已经数次使用到<span class="highlight">v-for</span>标签，这里再次进行补充。</p>
        </div>
        <div class="test">
            <p>在角色列表中各选中一个选项，点击添加角色后观察效果。</p>
            <input type="text" v-model="roleName">
            <button @click="addRole">添加角色</button>
            <ul>
                <li style="display: inline-block; padding-right: 15px;">没有 :key 属性</li>
                <li v-for="(role, index) in roleList" style="display: inline-block;">
                    <input type="checkbox"> {{role.roleName}}
                </li>
            </ul>
            <ul>
                <li style="display: inline-block; padding-right: 15px;">有 :key 属性</li>
                <li v-for="(role, index) in roleList" style="display: inline-block;" :key="index">
                    <input type="checkbox"> {{role.roleName}}
                </li>
            </ul>
            <p>我们可以观察到，没有 :key 属性的遍历列表中，选中的元素错位了。</p>
        </div>
    </div>

    <div class="part">
        <h4>vue对数组的响应</h4>
        <div class="test">
            <span v-for="(letter, index) in letters" style="padding: 0 10px;" :key="index">{{letter}}</span>
            <p><button @click="changeFirstLetterByIndex()">通过下标修改第一个字母</button><button @click="changeFirstLetterMethod()">通过方法修改第一个字母</button></p>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
<script>
    const app = new Vue({
        el: '#app',
        data: {
            message: '在Vue实例中声明的数据。',
            username: '圣·丹尼·加尔诺',
            roleName: '熊孩子',
            isLogin: false,
            isHere: true,
            roleList: [
                {id: 1, roleName: '学生'},
                {id: 2, roleName: '老师'},
                {id: 3, roleName: '管理员'}
            ],
            newRoleId: 4,
            selectedRole: 2,
            letters: ['a', 'b', 'c', 'd', 'e', 'g']
        },
        methods: {
            // 方法定义了参数，但调用时未传递且省略了小括号，未传递参数则会被当作当前事件对象
            bindClick1(myEvent) {
                console.log(myEvent);
            },
            bindClick2(num, myEvent) {
                console.log(num);
                console.log(myEvent);
            },
            showParentInfo() {
                console.log('你点击了父级元素');
            },
            showChildInfo() {
                console.log('你点击的是子元素');
            },
            handleSubmit() {
                console.log('你即将提交的数据内容是 ： ' + this.username);
            },
            qualifierTest() {
                console.log('你触发了键盘事件');
            },
            addRole() {
                this.roleList.unshift({ id: ++this.newRoleId, roleName: this.roleName })
                this.roleName = ''
            },
            changeFirstLetterByIndex() {
                this.letters[0] = 'x';
            },
            changeFirstLetterMethod() {
                //1.push()在最后添加一个元素
                //2.pop()删除最后一个元素
                //this.letters.pop()
                //3.shift()删除第一个
                //this.letters.shift()
                //4.unshift()添加在最前面,可以添加多个
                //this.letters.unshift('aaa','bbb','ccc')
                this.letters.splice(0, 1, 'w');
                //5.splice():删除元素/插入元素/替换元素
                //splice(1,1)在索引为1的地方删除一个元素,第二个元素不传，直接删除后面所有元素
                //splice(index,0,'aaa')再索引index后面删除0个元素，加上'aaa',
                //splice(1,1,'aaa')替换索引为1的后一个元素为'aaa'
                // this.letters.splice(2,0,'aaa')
                //6.sort()排序可以传入一个函数
                //this.letters.sort()
                //7.reverse()反转
                // this.letters.reverse()
            }
        }
    })
</script>
</body>
</html>
```

**效果预览：**（实际效果以运行的代码为准） 

![002-004](https://img.xfc-exclave.com/2021/08/02/f24c13a26c9dd6702a197d93db09e9e71627890559.jpg)

![002-005](https://img.xfc-exclave.com/2021/08/02/38a5e8da6ec535e1fa0e57666cc6e3dd1627890572.jpg)

> 说明：上图 v-for 示例中，同时选中“管理员”后，添加“熊孩子”角色，没有 `:key` 属性的列表被选中的元素便成了“老师”，而带有 `:key` 属性的列表中选中的值则没有发生变化。

