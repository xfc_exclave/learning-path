---
title: Vuex
categories: [WEB前端, VUE入门教程]
tags: [前端, vue]
top_img: false
---

### Vuex 简介

---

### Vuex 安装

`npm install vuex --save` 

* 导入安装插件

  ```js
  import Vuex from 'vuex'
  Vue.use(Vuex)
  ```

* 创建对象

  ```js
  const store = new Vuex.Sotre({
  	state: {
  	
  	},
      mutations: {
          
      },
      actions: {
          
      },
      getters: {
          
      },
      modules: {
          
      }
  })
  ```

  

* 导出store对象

  ```js
  export default store
  ```

  ```js
  import Vue from 'vue'
  import App from './App'
  import store from './store'
  
  vue.config.productionTip = false
  Vue
  
  new Vue({
      el: '#app',
      store,
      render: h => h(App)
  })
  ```

  

Vuex核心概念

State单一状态树

对象的解构