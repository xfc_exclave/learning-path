---
title: VUE学习前置
categories: [WEB前端, VUE入门教程]
tags: [前端, vue]
top_img: false
---

### 相关资料

vue官方网站：https://vuejs.org/

vue中文网：https://cn.vuejs.org/

vue官方文档：https://cn.vuejs.org/v2/guide/

vue学习视频：https://www.bilibili.com/video/BV15741177Eh

---

### vue特点

1. 解耦视图和数据
2. 可复用的组件
3. 前端路由技术
4. 状态管理
5. 虚拟DOM

---

### vue安装

1. CDN引入

   > <!-- 开发环境版本，包含了有帮助的命令行警告 --> 
   > <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
   > <!-- 生产环境版本，优化了尺寸和速度 -->
   >
   > <script src="https://cdn.jsdelivr.net/npm/vue"></script>

2. 下载 `js` 引入

   > 开发环境 https://vuejs.org/js/vue.js
   >
   > 生产环境 https://vuejs.org/js/vue.min.js

3. NPM安装

   > 通过 `webpack` 和 `CLI` 的进行安装使用。



---

### vue基本使用

#### 一、Hello World

**步骤：** 

1. 引入vue（以下通过CDN方式引入）
2. 创建 `Vue` 实例。
3. 创建 `div#app` 元素作为 `挂载点`。
4. 在已创建的 vue 实例中指定挂载点及初始化数据。
5. 通过 `Mustache语法` **{{message}}** 显示数据，运行并在浏览器查看效果

**代码：** 

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>vue基础语法代演示</title>
</head>
<body>

<div id="app">{{message}}</div>

<!-- 下载引入方式 -->
<!--<script src="../js/vue.js"></script>-->
<!-- CDN引入方式 -->
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
<script>
    const app = new Vue({
        el: '#app',// 挂载点
        data: {// 数据
            message: '在Vue实例中声明的数据。'
        }
    })
</script>
</body>
</html>
```

**效果预览：** 

![001-001](https://img.xfc-exclave.com/2021/08/02/6b37333de10daa31256392e5d2305ca91627890211.jpg)

> 注：我这里选择使用IDEA作为演示运行工具，你也可以选择使用 `webstorm` 、 `HBuilder` 、 `VSCode` 等，甚至是一些文本编辑器。

#### 二、vue列表显示

**步骤** 

1. 创建挂载点及 vue实例。
2. 初始化列表数据。
3. 使用 `v-for` 进行遍历展示。

**代码** 

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>vue列表展示</title>
</head>
<body>

<div id="app">
    <h2>{{title}}</h2>
    <ul>
        <li v-for="movie in movies">{{movie}}</li>
    </ul>
    <ul>
        <!-- 不要使用对象或数组之类的非基本类型值作为 v-for 的 key。 -->
        <li v-for="(actor, index) in actors" :key="index">{{index}} - {{actor}}</li>
    </ul>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
<script>
    const app = new Vue({
        el: '#app',
        data: {
            title: '电影推荐',
            movies: ['大话西游', '盗梦空间', '流浪地球', '当幸福来敲门', '楚门的世界'],
            actors: ['周星驰', '莱昂纳多·迪卡普里奥', '屈楚萧', '威尔·史密斯', '金·凯瑞']
        }
    })
</script>
</body>
</html>

<!--
app.movies.push('哈利波特')
app.actors.push('丹尼尔·雷德克里夫')
-->
```

**说明** 

> `:key` 的作用主要是为了高效的更新虚拟DOM ， `:key` 应当保持其在当前所有遍历节点中的唯一性，即在 `v-for` 中的所有 `:key` 值，应当避免重复。

**效果预览** 

![001-002](https://img.xfc-exclave.com/2021/08/02/660de8f4accddc48e45db8f78902f5cf1627890237.jpg)

#### 三、计数器案例

**代码** 

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>vue计数器案例</title>
</head>
<body>

<div id="app">
    <h2>当前计数：{{counter}}</h2>
    <!-- <button v-on:click="counter--">-</button>-->
    <!-- 语法糖 -->
    <!-- <button @click="counter++">+</button>-->
    <button @click="sub">-</button>
    <button @click="add">+</button>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
<script>
    const app = new Vue({
        el: '#app',
        data: {
            counter: 1
        },
        // 方法或函数
        methods: {
            add: function () {
                console.log('add()方法被执行');
                this.counter++;
            },
            // sub() 写法等同于 sub: function()
            sub() {
                console.log('sub()方法被执行');
                this.counter--;
            }
        }
    })
</script>
</body>
</html>
```

**效果预览** 

![001-003](https://img.xfc-exclave.com/2021/08/02/8eeff6b5840465d88c1adc0066f018b91627890254.jpg)

> 在浏览器窗口中点击相应按钮，实现计数的增减。

---

### vue中的 `MVVM` 

1. View层：

   即视图层,在我们前端开发中，通常就是DOM层。主要的作用是给用户展示各种信息。

2. Model层：

   即数据层，数据可能是我们固定的死数据，更多的是来自我们服务器，从网络上请求下来的数据。

3. VueModel层：

   即视图模型层，是View和Model沟通的桥梁。一方面它实现了Data Binding，也就是数据绑定，将Model的改变实时的反应到View中。另一方面它实现了DOM Listener，也就是DOM监听，当DOM发生一些事件(点击、滚动、touch等)时，可以监听到，并在需要的情况下改变对应的Data。

**图示** 

![001-004](https://img.xfc-exclave.com/2021/08/02/b7bff7631b34a3c76991578e2fa07a3a1627890271.jpg)

### vue的生命周期

**图示** 

![001-005](https://img.xfc-exclave.com/2021/08/02/4455d8d9785ed9dea97f35770acb91101627890281.jpg)

![001-006](https://img.xfc-exclave.com/2021/08/02/0df8ffe70b90cdfbb5853ebcd97e73e21627890308.png)