---
title: 网络模块封装-axios
categories: [WEB前端, VUE入门教程]
tags: [前端, vue]
top_img: false
---

### axios框架的基本使用

支持中请求方式：

* axios(config)
* axios.request(config)
* ......

axios使用

1. 安装

   `npm install axios --save` 

2. 使用

   ```js
   import axios from 'axios'
   
   axios({
   	url: 'http://123.207.32.32:8000/home/multidata',// 请求地址
       methods: 'get'
   }).then(res => {
       console.log(res);// 请求结果
   })
   // axios.get()
   ```

   ```js
   axios.all([axios({
       url: '/api1'
   }), axios({
       url: 'api2'
   })]).then(axios.spread((result1, result2) => {
       console.log(result1);
       console.log(result2);
   }))
   // .then(result => {})
   ```

### 全局配置

```js
axios.defaults.baseURL = 'http://yourhost/baseApi';
axios.defaults.timeout = 5000
```

### axios实例和模块封装

```js
const axiosA = axios.create({
	baseURL: 'http://hostA/api'
});
const axiosB = axios.create({
	baseURL: 'http://hostB/api'
});

axiosA({
    url: '/testA'
}).then(res => {
    console.log(res);
})
axiosB({
    url: '/testB'
}).then(res => {
    console.log(res);
})
```

### axios拦截器

```js
const instance = axios.create({
	baseURL: 'http://hostA/api'
});
// 拦截请求
instance.intercepter.request.use(config => {
    // 进行拦截处理
    return config;// 返回
}, error => {
    console.log(error);
})
// 拦截响应
instance.intercepter.response.use(res => {
    // 进行拦截处理
    return res.data;// 交由下一步处理
}, error => {
    console.log(error);
})
```

