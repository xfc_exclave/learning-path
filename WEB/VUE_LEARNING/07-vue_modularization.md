---
title: 前端模块化
categories: [WEB前端, VUE入门教程]
tags: [前端, vue]
top_img: false
---

**什么是模块：** 

* 将一个复杂的程序依据一定的规则 (规范) 封装成几个块 (文件)，并进行组合在一起；

* 块的内部数据与实现是私有的， 只是向外部暴露一些接口 (方法) 与外部其它模块通信。

**模块化的好处** 

- 避免命名冲突（减少命名空间污染）
- 更好的分离，按需加载
- 更高复用性
- 高可维护性
- 跨环境共享模块

**常见的模块化规范** 

* CommonJS
* AMD
* CMD
* ES6 Module



### ES6的模块化

**导出** 

```javascript
// 方式一
export {
  name, age
}
// 方式二
export let height = 1.88;
export let school = {name: "加利敦", grade: "大二"}
// 导出函数/类
export function getData(flag) {
  return flag ? 404 : {code: 200, data: ["数据一", "数据二"]}
}
export class Student {
  study() {
    console.log('在学习');
  }
}
// 导出默认，最多只能有一个默认导出
export default {}
export default "无引用内容";
export default function (str) {
  console.log(str);
}
```

**导入** 

```javascript
// 通过对象导入变量、函数、类
import {name, age, getData, Student} from './test1.js';
console.log(name + " - " + age);
const student = new Student();
student.study();
// 导入 export default 内容，此时命名可以不受导出js的影响
import anyName from "./test1.js";
// 导入所有
import * as test1 from './test1.js'
console.log(test1.name);
```

