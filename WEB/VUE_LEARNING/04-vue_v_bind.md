---
title: 表单绑定v-model
categories: [WEB前端, VUE入门教程]
tags: [前端, vue]
top_img: false
---

**预备** 

> 为了美观一点，以下代码示例引入了一些 `style.css` 样式，样式代码如下：

```css
ul>li{list-style:none;}#app{width:1000px;margin:0 auto;background:#f8f9f9;margin-bottom:300px;}.part{padding:20px;}.note{color:gray;border-left:4px solid #389eac;padding-left:10px;}.test{background:#fff;padding:10px;margin-bottom:5px;}.highlight{color:darkred;border:1px solid lightgray;padding:0 5px;margin:0 5px;}.active{font-weight:bold;color:cadetblue;}.text_shadow{text-shadow:5px 5px 5px gray;}.corner-mark{font-size: 12px;color: gray;}.corner-mark .tag{margin:0 3px 0 5px;color:red;}
```

---

**代码** 

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>表单绑定</title>
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>

<div id="app">
    <div class="part">
        <h4>v-model的基本使用</h4>
        <div class="note">
            <p>Vue中使用v-model指令来实现表单元素和数据的双向绑定。当表单中的数据发生改变，Vue对象中的数据也会随之改变，反之亦然。</p>
            <p>以下的示例也可通过浏览器控制台<span class="highlight">app._data.message</span>查看双向绑定效果。</p>
        </div>
        <div class="test">
            <input type="text" v-model="message">{{message}}
        </div>
    </div>
    <div class="part">
        <h4>v-model原理</h4>
        <div class="note">
            <p>v-model其实是一个语法糖，它的背后本质上是包含两个操作：</p>
            <p>1. v-bind绑定一个value属性</p>
            <p>2. v-on指令给当前元素绑定input事件</p>
            <p>具体实现原理如下述示例。</p>
        </div>
        <div class="test">
            <!-- 如果需要同时为事件传递事件对象和其他参数，那么事件对象应当使用 $event 作为参数进行传递，如 handleChange($event, userId) -->
            <input type="text" :value="message" @input="handleChange">{{message}}
        </div>
    </div>
    <div class="part">
        <h4>v-model & input[radio]</h4>
        <div class="note">
            <p>radio单选框的name属性是互斥的，如果使用v-model，可以不使用name就可以互斥。</p>
        </div>
        <div class="test">
            <p>你所在的城市：{{city}}</p>
            <label for="dongjing"></label><input id="dongjing" type="radio" value="东京" v-model="city">东京
            <label for="nanjing"></label><input id="nanjing" type="radio" value="南京" v-model="city">南京
            <label for="beijing"></label><input id="beijing" type="radio" value="北京" v-model="city">北京
        </div>
    </div>
    <div class="part">
        <h4>v-model & input[checkbox]</h4>
        <div class="note">
            <p>下述示例分别演示复选框的单选项和多选项，仅为演示checkbox的双向绑定功能。</p>
        </div>
        <div class="test">
            <p><label for="agreement"></label><input id="agreement" type="checkbox" v-model="isAgree">同意绑定下载垃圾软件</p>
            <div v-for="(item, index) in ads" :key="item">
                <label :for="item"></label><input id="item" type="checkbox" :value="item" v-model="ads">{{item}}
            </div>
            <div>
                <p>你将要额外下载如下软件：{{ads}}</p>
            </div>
        </div>
    </div>
    <div class="part">
        <h4>v-model & select</h4>
        <div class="note">
            <p>下述示例分别演示 v-model 与 select单选和多选进行复合使用</p>
        </div>
        <div class="test">
            <div>
                <select name="books" v-model="selectBook">
                    <option v-for="(book, index) in books" :value="book.bookId" :key="book.bookId">{{book.bookId + " - " + book.bookName}}</option>
                </select>
                <p>当前选中的序号为：{{selectBook}}</p>
            </div>
            <div>
                <select name="books" v-model="selectBooks" multiple>
                    <option v-for="(book, index) in books" :value="book.bookId" :key="book.bookId">{{book.bookId + " - " + book.bookName}}</option>
                </select>
                <p>当前选中的序号为：{{selectBooks}}</p>
            </div>
        </div>
    </div>
    <div class="part">
        <h4>v-model修饰符</h4>
        <div class="note">
            <p><span class="highlight">lazy</span>修饰符：默认情况下，v-model默认是在input事件中同步输入框的数据的，lazy修饰符可以让数据在失去焦点或者回车时才会更新。</p>
            <p><span class="highlight">number</span>修饰符可以让在输入框中输入的内容自动转成数字类型</p>
            <p><span class="highlight">trim</span>修饰符可以过滤内容左右两边的空格</p>
        </div>
        <div class="test">
            <div>
                <p><input type="text" v-model.lazy="lazyStr"><span class="corner-mark"><span class="tag">*</span>离焦后更新下面的内容</span></p>
                <p>内容：{{lazyStr}}</p>
            </div>
            <hr>
            <div>
                <p><input type="number" v-model.number="number"><span class="corner-mark"><span class="tag">*</span>更改输入框中的值，数据类型也会更新</span></p>
                <p>当前数据 {{number}} 的数据类型为：{{typeof number}}</p>
            </div>
            <hr>
            <div>
                <p><input type="text" v-model.trim="trimStr"><span class="corner-mark"><span class="tag">*</span>在首位处添加空格，字符串不会变化，可通过字符长度进行验证</span></p>
                <p>当前数据长度为：{{trimStr.length}}</p>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
<script>
    const app = new Vue({
        el: '#app',
        data: {
            message: '在Vue实例中声明的数据。',
            city: "南京",
            isAgree: true,
            ads: ["贪玩蓝月", "传奇盛世", "快手极速版", "360安全浏览器", "热血传说"],
            books: [{bookId: 1, bookName: "中國文學作品選注 - 袁行霈"}, {bookId: 2, bookName: "中國歷代文學作品選 - 朱東潤"}, {bookId: 3, bookName: "中國古代文學作品選 - 郁賢皓"}, {bookId: 4, bookName: "中国古代文学作品选 - 袁世硕"}],
            selectBooks: [1, 4],
            selectBook: 3,
            fruits: [],
            lazyStr: "文心雕龙札记 - 黄侃",
            number: "5631",
            trimStr: "诗韵词韵速查手册"
        },
        methods: {
            // 函数声明参数，但调用时未传递，则默认为当前当前原生事件对象
            handleChange(e) {
                this.message = e.target.value;
            }
        }
    })
</script>
</body>
</html>
```

**说明** 

> 相关说明详见代码内容。

**效果预览** 

![004-001](https://img.xfc-exclave.com/2021/08/02/22badaefafc3d6be049ea9dcecb391dd1627890719.jpg)

![004-002](https://img.xfc-exclave.com/2021/08/02/48bfa19e377926e64af38172be821bf51627890727.jpg)