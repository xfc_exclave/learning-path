---
title: Promise
categories: [WEB前端, VUE入门教程]
tags: [前端, vue]
top_img: false
---

### 使用场景

一般情况下，当需要进行异步操作时，可以使用 Promise 对这个异步操作进行封装。

使用示例：

```js
// 方式一
new Promise((resolve, reject) => {
    $.ajax({
        url: '/api/getUser',
        type: 'POST',
        data: {'userId': 1},
        success: function() {
			resolve("test msg");
        },
        error: function() {
            reject("err msg");
        }
    });
}).then((msg) => {
    console.log(msg);
}).catch(err => {
    console.log(err);
});
```

```js
// 方式二
new Promise((resolve, reject) => {
	$.ajax({
        url: '/api/getUser',
        type: 'POST',
        data: {'userId': 1},
        success: function() {
			resolve("test msg");
        },
        error: function() {
            reject("err msg");
        }
    });
}).then(data => {
	console.log(data);
}, err => {
	console.log(err);
});
```

---

### Promise 三种状态

* pending ：等待状态。
* fulfill ：满足状态，它会回调 `.then()` 方法。
* reject ：拒绝状态，它会回调 `.catch()` 方法。

---

### Promise 的链式调用

**示例代码** 

```js
new Promise((resolve, reject) => {
    // 逻辑代码
    resolve(msg);
}).then(msg => {
    console.log(msg);
    return new Promise((resolve) => {
        // 逻辑代码
    	resolve(msg2);
        // throw "exceptions ~~~~";
    })
}).then(msg2 => {
    console.log(msg2);
    return new Promise((resolve) => {
        // 逻辑代码
    	resolve(msg3);
    })
}).then(msg3 => {
    console.log(msg3);
    // ...
})。catch(err => {
    console.log(err);
})
```

---

### Promise 中 all 方法的使用

```js
new Promise.all([
    new Promise((resolve, reject) => {
        $.ajax({
            url: '/api/getUser',
            type: 'POST',
            data: {'userId': 1},
            success: function() {
                resolve("test msg");
            }
        });
    }),
    new Promise((resolve, reject) => {
        $.ajax({
            url: '/api/getRole',
            type: 'POST',
            data: {'userId': 1},
            success: function() {
                resolve("test msg");
            }
        });
    })
]).then(results => {
    console.log("user info = " + results[0]);
    console.log("role info = " + results[1]);
})
```

