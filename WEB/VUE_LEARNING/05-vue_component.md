---
title: 组件化开发
categories: [WEB前端, VUE入门教程]
tags: [前端, vue]
top_img: false
---

### 组件化的基本使用

**步骤：** 

1. 创建组件构造器 `Vue.extend()` 
2. 注册组件 `Vue.component()` 
3. 使用组件（在Vue实例作用范围内使用）

**代码** 

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>组件化的基本使用</title>
</head>
<body>

<div id="app">
    <!--3.使用组件-->
    <cpn-a></cpn-a>
    <cpn-a></cpn-a>
    <cpn-a></cpn-a>
    <cpn-a></cpn-a>

    <!-- 组件也可以被嵌套进其他元素内 -->
    <div>
        <cpn-a></cpn-a>
    </div>
</div>

<!-- 无法在Vue实例对象范围之外使用改对象注册的组件 -->
<cpn-a></cpn-a>

<!--本地js-->
<!--<script src="../js/vue.js"></script>-->
<!--在线CDN（引入的内容无法通过 ctrl + click 的方式进入源代码）-->
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
<script>
    // 1.创建组件构造器对象 Vue.extend()
    const constructorA = Vue.extend({
        template: `
        <div style="width: 100px; height: 100px; margin: 10px; display: inline-block;">
            <div style="width: 50px; height: 50px; float: left; border-radius: 100% 0 0 0; background: lightskyblue;"></div>
            <div style="width: 50px; height: 50px; float: left; border-radius: 0 100% 0 0; background: lightcyan;"></div>
            <div style="width: 50px; height: 50px; float: left; border-radius: 0 0 0 100%; background: lightsalmon;"></div>
            <div style="width: 50px; height: 50px; float: left; border-radius: 0 0 100% 0; background: lightgreen;"></div>
        </div>`
    });

    // 2.注册组件 Vue.component()
    Vue.component('cpn-a', constructorA);

    const app = new Vue({
        el: '#app',
        data: {}
    })
</script>

</body>
</html>
```

**效果预览：** 

![005-001](https://img.xfc-exclave.com/2021/08/02/cbe97bdd99069a92a1b70988d68f9cfa1627890858.jpg)

---

### 全局组件和局部组件

**代码** 

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>组件化的基本使用</title>
</head>
<body>

<p>Vue实例一</p>
<div id="app">
    <cpn-a></cpn-a>
    <cpn-a></cpn-a>
    <!--以驼峰命名注册的组件，使用时标签需要替换为全小写并使用短横线连接-->
    <cpn-b></cpn-b>
    <cpn-a></cpn-a>
</div>
<hr>
<p>Vue实例二</p>
<div id="app2">
    <cpn-a></cpn-a>
    <cpn-b></cpn-b>
</div>

<!--本地js-->
<!--<script src="../js/vue.js"></script>-->
<!--在线CDN（引入的内容无法通过 ctrl + click 的方式进入源代码）-->
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
<script>
    const constructorA = Vue.extend({
        template: `
        <div style="width: 100px; height: 100px; margin: 10px; display: inline-block;">
            <div style="width: 50px; height: 50px; float: left; border-radius: 100% 0 0 0; background: lightskyblue;"></div>
            <div style="width: 50px; height: 50px; float: left; border-radius: 0 100% 0 0; background: lightcyan;"></div>
            <div style="width: 50px; height: 50px; float: left; border-radius: 0 0 0 100%; background: lightsalmon;"></div>
            <div style="width: 50px; height: 50px; float: left; border-radius: 0 0 100% 0; background: lightgreen;"></div>
        </div>`
    });

    const constructorB = Vue.extend({
        template: `<div style="width: 100px; height: 100px; margin: 10px; display: inline-block; background: linear-gradient(#389eac, #ebf0f2); border-radius: 50%;"></div>`
    });

    // 在此处注册的 cpn-a 是全局组件，所有Vue示例均可使用改组件
    Vue.component('cpn-a', constructorA);

    const app = new Vue({
        el: '#app',
        data: {},
        components: {
            cpnB: constructorB
        }
    });

    const app2 = new Vue({
        el: '#app2',
        data: {}
    });
</script>

</body>
</html>
```

**效果预览：** 

![005-002](https://img.xfc-exclave.com/2021/08/02/e886c28db9a110469b555f0aa5f3a22c1627890870.jpg)

---

### 父组件和子组件

**代码** 

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>父组件与子组件</title>
</head>
<body>

<div id="app">
    <cpn-parent></cpn-parent>
    <cpn-child></cpn-child><!--cpnChild既未在全局注册，也未在 #app 挂载的实例中注册，因此这里无法直接使用<cpn-child></cpn-child>-->
</div>

<script src="../js/vue.js"></script>
<script>
    // 1.创建第一个组件构造器(子组件)
    const cpnChild = Vue.extend({
        template: `<div style="background: #ebf0f2; padding: 5px; margin-top: 5px;">
            <h4>文章标题</h4>
            <p>文章简述内容</p>
        </div>`
    });

    // 2.创建第二个组件构造器(父组件)
    const cpnParent = Vue.extend({
        template: `
        <div style="width: 400px; border: 1px solid lightgray; margin: 20px auto;">
            <input type="text" placeholder="输入关键词搜索文章" style="padding: 5px 10px; margin: 10px; width: 80%;">
            <cpn-child></cpn-child>
            <cpn-child></cpn-child>
            <cpn-child></cpn-child>
            <cpn-child></cpn-child>
        </div>`,
        components: {
            cpnChild// ES6写法，相当于 cpnChild: cpnChild
        }
    });

    // root组件，也相当于是cpnParent的父组件
    const app = new Vue({
        el: '#app',
        data: {},
        components: {
            cpnParent
        }
    })
</script>

</body>
</html>
```

**效果预览：** 

![005-003](https://img.xfc-exclave.com/2021/08/02/611c77ca502a95aa22df8111f95e3cd31627890882.jpg)

---

### 组件注册的语法糖

> 组件的注册使用需要经历3个步骤，如下：
>
> 1. 创建组件构造器 `Vue.extend()` 
> 2. 注册组件 `Vue.component()` 
> 3. 使用组件（在Vue实例作用范围内使用）
>
> 但我们也可以将 `Vue.extend()` 与 `Vue.component()` 进行合并书写，直接使用对象的方式替代，最终书写方式如下：

```html
<div id="app">
    <regional-cpn></regional-cpn>
    <global-cpn></global-cpn>
</div>

<script src="../js/vue.js"></script>
<script>
    // 1.全局注册
    Vue.component('globalCpn', {
        template: `
        <div>
            <p>全局组件相关内容</p>
        </div>`
    });

    // 2.注册局部组件的语法糖
    const app = new Vue({
        el: '#app',
        data: {
            message: '你好啊'
        },
        components: {
            'regionalCpn': {
                template: `
                <div>
                    <p>局部组件相关内容</p>
                </div>`
            }
        }
    })
</script>
```

---

### 组件模板的分离写法

> 在以上的示例中，模板内容被书写在 js 相关代码中，但我们也可以通过下面的两种方式分离出来。

**代码** 

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>组件模板的分离写法</title>
</head>
<body>

<div id="app">
    <cpn1></cpn1>
    <cpn2></cpn2>
</div>

<!--方式一：template标签-->
<template id="cpn-template">
    <div>
        <p>方式一：组件模板内容</p>
    </div>
</template>

<!--方式二：script标签，注意:类型必须是text/x-template-->
<script type="text/x-template" id="cpn-template2">
    <div>
        <p>方式二：组件模板内容</p>
    </div>
</script>

<script src="../js/vue.js"></script>
<script>
    // 全局组件
    Vue.component('cpn1', {
        template: '#cpn-template'
    });

    const app = new Vue({
        el: '#app',
        data: {},
        components: {
            cpn2: {
                template: '#cpn-template2'
            }
        }
    })
</script>

</body>
</html>
```

---

### 组件的数据存放

> 组件内部无法直接使用 `Vue` 实例对象中声明的数据，那么当需要为组件传入数据时，则需要在组件中声明 `data()` 函数用于存放数据，注意，是 `data()` 函数而非 `data` 属性。

**代码** 

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>组件的数据存放</title>
</head>
<body>

<div id="app">
    <cpn></cpn>
    <cpn></cpn>
</div>

<!--2.template标签-->
<template id="cpn">
    <div>
        <h2>{{title}}<span>{{author}}</span></h2><!--这里是无法直接使用Vue对象中的{{author}}的，会提示author未定义-->
        <p>{{content}}</p>
    </div>
</template>

<script src="../js/vue.js"></script>
<script>

    // 1.注册一个全局组件
    Vue.component('cpn', {
        template: '#cpn',
        data() {
            return {
                title: '纳兰词全编笺注',
                content: '纳兰词最全注释本，重新遇见纯净的容若。全新考订，白话注释，简体横排'
            }
        }
    });

    const app = new Vue({
        el: '#app',
        data: {
            author: '苏缨'
        }
    })
</script>

</body>
</html>
```

**组件中的数据为什么需要 `data()` 函数方式进行存储？** 

> 使用函数返回的数据在每一次被调用时，都会为调用这创建响应的实例，因此在函数被多次调用的情况下，每次调用的对象是不相互共享的。
>
> 而使用属性的方式存储数据，每次调用该数据时都将会是同一个数据对象。
>
> 如果你了解一定的java知识，或者其他后端语言（想必有不少语言在这方面都是有相似点的），你或许可以理解为以下代码：

```java
public static String str = "abcdef";

public static void main(String[] args) {
    // 函数调用方式
	System.out.println(System.identityHashCode(getStr()));
	System.out.println(System.identityHashCode(getStr()));
    // 对象调用方式
	System.out.println(System.identityHashCode(str));
	System.out.println(System.identityHashCode(str));
}

public static String getStr() {
	return new String("abcdef");
}
```

> 下面使用代码演示多个实例共享同一个数据的问题。

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>组件为什么用函数存储数据</title>
</head>
<body>

<!--组件实例对象-->
<div id="app">
    <cpn></cpn>
    <cpn></cpn>
    <cpn></cpn>
</div>

<template id="cpn">
    <div>
        <button @click="decrement">-</button>
        <span>{{counter}}</span>
        <button @click="increment">+</button>
    </div>
</template>
<script src="../js/vue.js"></script>
<script>
    // 使用变量接收数据时，该变量会生成唯一不变的内存地址
    const obj = {
        counter: 0
    };
    // 1.注册组件
    Vue.component('cpn', {
        template: '#cpn',
        data() {
            /*return {
                counter: 0
            }*/
            // 如果使用上面的return方式，每个实例对象会有各自对应的counter数据。
            // 如果使用下面的return方式，则会出现多个实例共享同一个counter数据。
            return obj;
        },
        methods: {
            increment() {
                this.counter++
            },
            decrement() {
                this.counter--
            }
        }
    })

    const app = new Vue({
        el: '#app',
        data: {}
    })
</script>

</body>
</html>
```

---

### 父子组件之间的通讯

#### 1. 父传子 `props` 

> 在父子组件之间，往往需要进行数据的相互传递，父级组件向子级组件传递数据，主要是通过 `props` 完成的。代码及图示如下：

**代码** 

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>组件之间的通讯 - 父传子</title>
</head>
<body>

<div id="app">
    <cpn :books="books" :press="publisher"></cpn>
</div>

<template id="cpn">
    <div>
        <h2>{{press}}</h2>
        <ul>
            <li v-for="item in books">{{item}}</li>
        </ul>
    </div>
</template>

<script src="../js/vue.js"></script>
<script>
    // 父传子: props
    const cpn = {
        template: '#cpn',
        props: ['press', 'books'],
        data() {
            return {}
        }
    };

    const app = new Vue({
        el: '#app',
        data: {
            publisher: '高等教育出版社',
            books: ['中国文学史', '文学批评导引', '文学理论教程']
        },
        components: {
            cpn
        }
    })
</script>

</body>
</html>
```

**截图：** 

![005-004](https://img.xfc-exclave.com/2021/08/02/9cd71b78f580172973521866deba68541627890912.jpg)

> 值得注意的是，上述代码中各处的命名规范。
>
> 当然，除了上述 `props: ['press', 'books']` 使用数组接收父组件传递的数据之外， `props` 也可以通过对象的方式为接收的数据定义更加丰富的属性。

1. 类型限制

```javascript
props: {
    press: ['String', 'Number'],
    books: Array
}
```

2. 多条件限制

```javascript
props: {
    press: {
        type: String,
        default: '未知出版社',
        required: true
    },
    books: {
        type: Array,
        default() {
            return []
        }
    }
}
```

> 注意：在对 `props` 进行限制时，当类型是对象或者数组时，默认值必须是一个函数。

另外，也可以使用 `vaildator` 进行自定义验证，例如：

```javascript
books: {
    validator: function(value) {
        return value.length > 0;
    }
}
```

#### 2. 子传父 `$emit` 

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>组件之间的通讯 - 子传父</title>
</head>
<body>

<div id="app" style="width: 800px; height: 200px; margin: 0 auto; padding: 10px; background: #ebf0f2;">
    <cpn @handle-calendar="calendarClick"></cpn>
    <div style="float: left; padding: 10px;">
        <p style="font-weight: bold;">{{title}}</p>
        <p>{{message}}</p>
    </div>
    <div style="clear: both;"></div>
</div>

<template id="cpn">
    <div style="float: left;">
        <div v-for="item in calendar" :key="item.date" style="margin: 10px;">
            <button @click="showCalendar(item)">3月{{item.date}}日</button>
        </div>
    </div>
</template>

<script src="../js/vue.js"></script>
<script>
    const cpn = {
        template: '#cpn',
        data() {
            return {
                calendar: [
                    {date: '24', avoid: '宜快快长大', description: '很多母亲正在楚门，正在回家，正怀抱着世界，甜蜜而小心。'},
                    {date: '25', avoid: '宜老味道', description: '所谓传统，就是保持在民间大众口头手头心头上的那些可以信手拈来的东西。'},
                    {date: '26', avoid: '忌恶心循环', description: '因为没有真正的证明，找不到工作；因为找不到工作，所以没有真正的有关证明。'},
                    {date: '27', avoid: '宜合群', description: '谁说英雄寂寞？我们的英雄就是欢乐的。'},
                ]
            }
        },
        methods: {
            showCalendar(info) {
                // 发射事件: 自定义事件
                this.$emit('handle-calendar', info)
            }
        }
    };

    // 2.父组件
    const app = new Vue({
        el: '#app',
        data: {
            title: '',
            message: '点击对应日期展示推送信息'
        },
        components: {
            cpn
        },
        methods: {
            calendarClick(obj) {
                this.title = obj.avoid;
                this.message = obj.description;
            }
        }
    })
</script>

</body>
</html>
```

**截图：** 

![005-005](https://img.xfc-exclave.com/2021/08/02/cf5a25e8667c99fa27738ae86bb09ecf1627890903.jpg)

> 值得注意的是，上述代码在传递过程中的函数命名方式。

---

### 父子组件的访问方式

#### 1. 父组件访问子组件 `$refs` / `$children` 

**代码** 

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>父子组件的访问方式</title>
</head>
<body>

<div id="app">
    <cpn></cpn>
    <cpn ref="child_cpn"></cpn>
    <button @click="btnClick">按钮</button>
</div>

<template id="cpn">
    <div>子组件</div>
</template>
<script src="../js/vue.js"></script>
<script>
    const app = new Vue({
        el: '#app',
        data: {},
        methods: {
            btnClick() {
                // 调用子组件中的数据
                // $children用于获取到所有子组件，获取指定组件时需要通过下标判断
                // $refs.*用于获取指定的子组件
                console.log(this.$children[0].message);
                console.log(this.$refs.child_cpn.message);
                // 调用子组件中的方法
                this.$refs.child_cpn.showMessage();
            }
        },
        components: {
            cpn: {
                template: '#cpn',
                data() {
                    return {
                        message: '子组件中的数据'
                    }
                },
                methods: {
                    showMessage() {
                        alert("子组件中的方法被调用");
                    }
                }
            }
        }
    })
</script>

</body>
</html>
```

#### 2. 子组件访问父组件 `$parent` 

**代码** 

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>父子组件的访问方式</title>
</head>
<body>

<div id="app">
    <cpn @show-msg="showMessage"></cpn>
</div>

<template id="cpn">
    <div>
        <div>子组件</div>
        <button @click="btnClick">按钮</button>
    </div>
</template>

<script src="../js/vue.js"></script>
<script>
    const app = new Vue({
        el: '#app',
        data: {
            message: '父组件中的数据'
        },
        methods: {
            showMessage(str) {
                console.log("父组件中的方法，参数内容：" + str);
            }
        },
        components: {
            cpn: {
                template: '#cpn',
                methods: {
                    btnClick() {
                        // 访问根组件
                        console.log(this.$root.message);
                        this.$root.showMessage('hello - $root');
                        // 除了通过 $emit ，也可以通过 $parent 调用父组件中的方法
                        this.$emit('show-msg', 'hello - $emit');
                        this.$parent.showMessage('hello - $parent');
                    }
                }
            }
        }
    })
</script>

</body>
</html>
```

