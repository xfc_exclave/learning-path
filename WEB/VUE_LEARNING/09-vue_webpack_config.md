---
title: webpack配置vue及plugin
categories: [WEB前端, VUE入门教程]
tags: [前端, vue]
top_img: false
---

### 准备

> 创建新的项目文件夹。

**目录结构** 

![009-001](http://119.3.48.114:8090/upload/2020/09/009-001-f4574e75d3ff4362ab07efb6ac681fcf.jpg)

**步骤** 

1. 初始化目录结构及相关文件。

   * 创建项目文件夹。
   * 创建 `dist` 、 `src` 文件目录。
   * 创建 `index.html` 、 `webpack.config.js` 、 `main.js` 文件。
   
2. 编辑配置文件 `webpack.config.js` 

   ```js
   const path = require('path');// 通过commonJS引入path
   
   module.exports = {
       entry:'./src/main.js',
       output: {
           path: path.resolve(__dirname, 'dist'),// 绝对路径，打包位置
           filename: 'bundle.js',
           publicPath: "dist/"
       },
       module: {
           rules: [
               {
                   test: /\.css$/,
                   use: [ 'style-loader', 'css-loader' ]
               }
           ]
       },
       resolve: {
           alias: {
               // 设置别名，指定在import Vue的来源，以便区别不同的版本
               'vue$': 'vue/dist/vue.esm.js'
           }
       }
   };
   ```

   注：vue 存在两种版本，即 `runtime-only` 和 `runtime-compiler` 。在 `webpack.config.js` 中设置别名可以指定 vue 引用的版本。

3. 初始化 `npm` 

   > npm init

4. 安装所需 loader 及 vue。

   > 执行 `npm install --save-dev css-loader` 安装 `css-loader` 
   >
   > 执行 `npm install style-loader --save-dev` 安装 `style-loader` 

   > 执行 `npm install vue --save` 安装 vue

5. 编辑 `package.json` 文件。

   > 在 scrpit 项中添加 `"build": "webpack"` 属性。

6. 编辑 `main.js` 文件。

   ```js
   import Vue from 'vue';
   
   const app = new Vue({
       el: '#app',
       data: {
           message: '用于演示的数据'
       }
   })
   ```

7. 编辑 `index.html` 文件

   ```html
   <!DOCTYPE html>
   <html lang="en">
   <head>
       <meta charset="UTF-8">
       <title>Webpack配置Vue</title>
   </head>
   <body>
   <div id="app">
       <h3>{{message}}</h3>
   </div>
   <script src="./dist/bundle.js"></script>
   </body>
   </html>
   ```

8. `npm run build` 

---

### vue演示

#### 1. 抽离vue实例的数据及模板

修改 index.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Webpack配置Vue</title>
</head>
<body>
<div id="app"></div>
<script src="./dist/bundle.js"></script>
</body>
</html>
```

修改 main.js

```js
import Vue from 'vue';

const App = {
    template: '<div>在 main.js 中使用template引入的数据。<span>{{message}}</span><button @click="btnClick">按钮</button></div>',
    data() {
        return {
            message: '用于演示的数据'
        }
    }
}

new Vue({
    el: '#app',
    template: '<App/>',
    comments: {
        App
    },
    methods: {
        btnClick: function() {
            alert("你触发了点击事件。");
        }
    }
})
```

运行 `npm run build` 并查看效果。

#### 2. 抽离组件代码

在 src/vue 目录下创建 app.js 文件

```js
export default {
    template: '<div>在 main.js 中使用template引入的数据。<span>{{message}}</span><button @click="btnClick">按钮</button></div>',
    data() {
        return {
            message: '用于演示的数据'
        }
    },
    methods: {
        btnClick: function() {
            alert("你触发了点击事件。");
        }
    }
}
```

修改 main.js

```js
import Vue from 'vue';
import App from './vue/app'

new Vue({
    el: '#app',
    template: '<App/>',
    components: {
        App
    }
})
```

运行 `npm run build` 并查看效果。

#### 3. 分离 template 和 js 代码

在 src/vue 目录下创建 App.vue 文件

```js
export default {
    template: '<div>在 main.js 中使用template引入的数据。<span>{{message}}</span><button @click="btnClick">按钮</button></div>',
    data() {
        return {
            message: '用于演示的数据'
        }
    },
    methods: {
        btnClick: function() {
            alert("你触发了点击事件。");
        }
    }
}
```

> app.js 文件可删除了。

修改 main.js 文件

```js
import Vue from 'vue';
import App from './vue/App.vue'

new Vue({
    el: '#app',
    template: '<App/>',
    components: {
        App
    }
})
```

但由于没有安装 vue-loader ，此时仍无法通过编译打包。

> 执行 `npm install --save-dev vue-loader vue-template-compiler` 安装 vue-loader 。

在 webpack.config.js 中配置 vue-loader

```js
const path = require('path');// 通过commonJS引入path
const VueLoaderPlugin = require('vue-loader/lib/plugin')// v15.*.*

module.exports = {
    entry:'./src/main.js',
    output: {
        path: path.resolve(__dirname, 'dist'),// 绝对路径，打包位置
        filename: 'bundle.js',
        publicPath: "dist/"
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [ 'style-loader', 'css-loader' ]
            },
            {
                test: /\.vue$/,
                use: [ 'vue-loader']
            }
        ]
    },
    resolve: {
        alias: {
            // 设置别名，指定在import Vue的来源，以便区别不同的版本
            'vue$': 'vue/dist/vue.esm.js'
        }
    },
    // v15.*.*
    plugins: [
        new VueLoaderPlugin()
    ]
};
```

> 注：vue-loader v15.* 之后，均需要配置 `VueLoaderPlugin` 插件，否则无法通过编译。
>
> 更多详情参考：https://vue-loader.vuejs.org/migrating.html#a-plugin-is-now-required

运行 `npm run build` 并查看效果。

#### 4. 引入子组件

在 src/vue 目录下创建 Cpn.vue 文件

```js
<template>
  <div>子组件内容</div>
</template>

<script>
export default {
  name: "Cpn"
}
</script>

<style scoped>

</style>
```

修改 App.vue 文件：

```js
<template>
  <div>在 main.js 中使用template引入的数据。<span>{{ message }}</span>
    <button @click="btnClick">按钮</button>
    <cpn/>
  </div>
</template>

<script>
import Cpn from './Cpn.vue';

export default {
  name: "App",
  components: {
    Cpn
  },
  data() {
    return {
      message: '用于演示的数据'
    }
  },
  methods: {
    btnClick: function () {
      alert("你触发了点击事件。");
    }
  }
}
</script>

<style scoped>

</style>
```

#### 5. 配置导入文件简写后缀

在 `webpack.config.js` 中配置 resolve.extensions 为 ['.js', ".css", '.vue'] ，即可省略导入文件后缀名。

即可以通过 `import Cpn from './Cpn';` 引入 Cpn.vue 文件

> 注：如果存在同名（不区分大小写）js 或其他后缀文件，需要加上后缀以便区分。

### Plugin

上面已经提到了修改 webpack.config.js 文件中的 Plugin，即 Plugin 的使用。

1. 添加版权声明的插件

   ```js
   // webpack.config.js
   const webpack = require('webpack');
   // ...
   plugins: [
       new webpack.BannerPlugin("CropWrite by XXX")
   ]
   ```

2. 打包 html 的插件

   执行命令 `npm install html-webpack-plugin --save-dev` 安装插件。

   ```js
   // webpack.config.js
   const htmlWebpackPlugin = require('html-webpack-plugin');
   // ...
   plugins: [
       // 注：打包后的index.html与bundle.js统计目录，不需要再配置publicPath
       // index.html中不需要编写引入bundle.js，插件会帮组引入
       new HtmlWebpackPlugin({
           template: 'index.html'
       })
   ]
   ```

3. 压缩 js 的插件

   执行命令 `npm install uglifyjs-webpack-plugin --save-dev` 安装插件。

   ```js
   // webpack.config.js
   const Uglifyjs = require('uglifyjs-webpack-plugin');
   // ...
   plugins: [
       new Uglifyjs()
   ]
   ```

   > 此处可能会报错， `Unexpected token: punc «(»` ，参考：https://www.cnblogs.com/xueshanshan/p/6610940.html

