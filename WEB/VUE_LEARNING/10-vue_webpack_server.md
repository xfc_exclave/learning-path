---
title: 本地服务器及配置分离
categories: [WEB前端, VUE入门教程]
tags: [前端, vue]
top_img: false
---

### 本地服务器

执行 `npm install webpack-dev-server --save-dev` 安装 `webpack-server` 

配置 `webpack.config.js` ：

```js
module.exports = {
    // ...
    devServer: {
        contentBase: './dist',
        inline: true
    }
}
```

配置 `package.json` > scripts 脚本：

```json
"scripts": {
  "test": "echo \"Error: no test specified\" && exit 1",
  "build": "webpack",
  "dev": "webpack-dev-server"
}
```

执行命令 `npm run dev` 运行本地服务。

访问本地服务，默认：http://localhost:8080

> 注：webstorm及idea中，Ctrl+C 可终止当前批处理操作。

配置 `"dev": "webpack-dev-server --open"` 在启动后可自动打开浏览器。

---

### webpack的配置分离

1. 新建 `build` 文件夹，并在该文件夹下创建文件 `base.config.js` 、 `prod.config.js` 、 `dev.config.js` 。

2. 复制 `webpack.config.js` 文件中的内容到这三个文件夹（完成后可删除 `webpack.config.js` ）。

3. 执行命令 `npm install webpack-merge --save-dev ` 安装 `webpack-merge` 。

4. 配置文件：

   * `base.config.js` ：通用配置。

     示例：

     ```js
     const path = require('path');// 通过commonJS引入path
     const VueLoaderPlugin = require('vue-loader/lib/plugin')// v15.*.*
     const webpack = require('webpack');
     const HtmlWebpackPlugin = require('html-webpack-plugin');
     
     module.exports = {
         entry:'./src/main.js',
         output: {
             path: path.resolve(__dirname, '../dist'),// 绝对路径，打包位置
             filename: 'bundle.js',
             // publicPath: "dist/"
         },
         module: {
             rules: [
                 {
                     test: /\.css$/,
                     use: [ 'style-loader', 'css-loader' ]
                 },
                 {
                     test: /\.vue$/,
                     use: [ 'vue-loader']
                 }
             ]
         },
         resolve: {
             extensions: ['.js', '.json', '.vue', '.scss', '.css'],
             alias: {
                 // 设置别名，指定在import Vue的来源，以便区别不同的版本
                 'vue$': 'vue/dist/vue.esm.js'
             }
         },
         // v15.*.*
         plugins: [
             new VueLoaderPlugin(),
             new webpack.BannerPlugin("CropWrite by XXX"),
             new HtmlWebpackPlugin({
                 template: 'index.html'
             })
         ]
     };
     ```

   * `prod.config.js` ：生产环境配置。

     ```js
     const WebpackMerge = require('webpack-merge');
     const baseConfig = require('./base.config');
     module.export = WebpackMerge(baseConfig, {
         plugins: [
             new UglifyjsWebpackPlugin()
         ]
     })
     ```

   * `dev.config.js` ：开发环境配置。

     ```js
     const WebpackMerge = require('webpack-merge');
     const baseConfig = require('./base.config');
     module.export = WebpackMerge(baseConfig, {
         devServer: {
             contentBase: './dist',
             inline: true
         }
     })
     ```

5. 修改 `webpack.json` 中的 `script` 脚本。

   ```json
   "scripts": {
       "test": "echo \"Error: no test specified\" && exit 1",
       "build": "webpack --config ./build/prod.config.js",
       "dev": "webpack-dev-server --open --config ./build/dev.config.js"
   }
   ```

   > 修改脚本命令使其执行经过分离后的配置文件。

6. 注意修改 `base.config.js` 文件中的打包目录。

   ```js
   path: path.resolve(__dirname, '../dist')
   ```

7. 编译运行并查看效果

   > npm run build
   >
   > npm run dev